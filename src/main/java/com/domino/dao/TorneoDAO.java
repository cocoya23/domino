package com.domino.dao;

import com.domino.bens.*;
import com.domino.util.HibernateUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class TorneoDAO {
    
    public List<Torneo> getTorneo(Integer idjugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createQuery("from Torneo").list();
        session.getTransaction().commit();
        session.close();
        return result;
    }
    
    public Integer getTorneoxJug(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(JugadorTorneo.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorn.intValue()),
                Restrictions.eq("idjugador", idJug.intValue()))).list();
        Integer tam = result.size();
        session.getTransaction().commit();
        session.close();
        return tam;
    }
    
    public List<Torneo> getTorneoxJugD(Integer idTorn) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(Torneo.class).
                add(Restrictions.eq("idTorneo", idTorn.intValue())).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }
    
    public List<JugadorTorneo> getTorneoxJugCheck(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(JugadorTorneo.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorn.intValue()),
                Restrictions.eq("idjugador", idJug.intValue()))).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }
    
    public List<Torneo> getTorneoxPareja(Integer idjugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createQuery("from Torneo"
                + " where idTorneo in ("
                + " select idTorneo from  RegistroTorneoModPareja"
                + " where idjugador =" + idjugador.intValue() + ")").list();
        session.getTransaction().commit();
        session.close();
        return result;
    }
    
    public List<Torneo> getTorneoxEquipo(Integer idjugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createQuery("from Torneo"
                + " where idTorneo in ("
                + " select idTorneo from  RegistroTorneoModEquipo"
                + " where idjugador =" + idjugador.intValue() + ")").list();
        session.getTransaction().commit();
        session.close();
        return result;
    }
    
    public void updateJugadorxTorneo(JugadorTorneo jugadortorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.update(jugadortorneo);
        session.getTransaction().commit();
        session.close();
    }
    
    public void delRegistroTorneoModPareja(Integer idTorn, Integer idJug) throws Exception {
        try {
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            RegistroTorneoModPareja registrotorneomodpareja = (RegistroTorneoModPareja) session.createCriteria(RegistroTorneoModPareja.class).
                    add(Restrictions.and(
                    Restrictions.eq("idTorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador", idJug.intValue()))).uniqueResult();
            session.delete(registrotorneomodpareja);
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
    
    public void delFormacionPareja(Integer idTorn, Integer idJug) throws Exception {
        try {
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            FormacionPareja formacionpareja = (FormacionPareja) session.createCriteria(FormacionPareja.class).
                    add(Restrictions.or(
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador1", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador2", idJug.intValue())))).uniqueResult();
            session.delete(formacionpareja);
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
    
    public Integer rowsFormacionPareja(Integer idTorn, Integer idJug) throws Exception {
        Integer rows = 0;
        try {
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            List result = session.createCriteria(FormacionPareja.class).
                    add(Restrictions.or(
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador1", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador2", idJug.intValue())))).list();
            rows = result.size();
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            System.out.println(ex);
            rows = 0;
        }
        return rows;
    }
    
    public FormacionPareja namePareja(Integer idTorn, Integer idJug) throws Exception {
        try {
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            FormacionPareja formacionpareja = (FormacionPareja) session.createCriteria(FormacionPareja.class).
                    add(Restrictions.or(
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador1", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador2", idJug.intValue())))).uniqueResult();
            session.getTransaction().commit();
            session.close();
            return formacionpareja;
        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }
    }
    
    public Integer equipoRows(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(FormacionEquipo.class).
                add(Restrictions.or(
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador1", idJug.intValue())),
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador2", idJug.intValue())),
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador3", idJug.intValue())),
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador4", idJug.intValue())))).list();
        Integer rows = result.size();
        session.getTransaction().commit();
        session.close();
        return rows;
    }
    
    public FormacionEquipo formEquipo(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        FormacionEquipo formacionEquipo = (FormacionEquipo) session.createCriteria(FormacionEquipo.class).
                add(Restrictions.or(
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador1", idJug.intValue())),
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador2", idJug.intValue())),
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador3", idJug.intValue())),
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador4", idJug.intValue())))).uniqueResult();
        session.getTransaction().commit();
        session.close();
        
        return formacionEquipo;
    }
    
    public List<RegistroTorneoModEquipo> getRegistroTorneoModEquipo(Integer id, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List<RegistroTorneoModEquipo> result = session.createCriteria(RegistroTorneoModEquipo.class).
                add(Restrictions.and(
                Restrictions.ne("idjugador", id.intValue()),
                Restrictions.eq("idTorneo", idtorneo.intValue()),
                Restrictions.eq("activo", 0))).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }
    
    public Integer insertEquipo1(FormacionEquipo formacionequipo, Integer idtorn, Integer idjug1) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.save(formacionequipo);
        formacionequipo = (FormacionEquipo) session.createCriteria(FormacionEquipo.class).
                add(Restrictions.and(
                Restrictions.eq("idtorneo", idtorn.intValue()),
                Restrictions.eq("idjugador1", idjug1.intValue()))).uniqueResult();
        Integer idform = formacionequipo.getIdformacionequipo();
        session.getTransaction().commit();
        session.close();
        return idform;
    }
    
    public void insertEquipo2(FormacionEquipo formacionequipo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.saveOrUpdate(formacionequipo);
        session.getTransaction().commit();
        session.close();
    }
    
    public void updateRegistroTorneoModEquipo(Integer idtorn, Integer idjug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        RegistroTorneoModEquipo registrotorneomodequipo = (RegistroTorneoModEquipo) session.createCriteria(RegistroTorneoModEquipo.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idtorn.intValue()),
                Restrictions.eq("idjugador", idjug.intValue()))).uniqueResult();
        registrotorneomodequipo.setActivo(1);
        session.saveOrUpdate(registrotorneomodequipo);
        session.getTransaction().commit();
        session.close();
        
    }
    
    public void updateRegistroTorneoModEquipoIn(Integer idtorn, Integer idjug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        RegistroTorneoModEquipo registrotorneomodequipo = (RegistroTorneoModEquipo) session.createCriteria(RegistroTorneoModEquipo.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idtorn.intValue()),
                Restrictions.eq("idjugador", idjug.intValue()))).uniqueResult();
        registrotorneomodequipo.setActivo(0);
        session.saveOrUpdate(registrotorneomodequipo);
        session.getTransaction().commit();
        session.close();
        
    }
    
    public FormacionEquipo getFormacionEquipo(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        FormacionEquipo formacionequipo = (FormacionEquipo) session.createCriteria(FormacionEquipo.class).
                add(Restrictions.or(
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador1", idJug.intValue())),
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador2", idJug.intValue())),
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador3", idJug.intValue())),
                Restrictions.and(
                Restrictions.eq("idtorneo", idTorn.intValue()),
                Restrictions.eq("idjugador4", idJug.intValue())))).uniqueResult();
        session.getTransaction().commit();
        session.close();
        return formacionequipo;
    }
    
    public String getNombreEquipo(Integer idjugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Jugador jugador = (Jugador) session.createCriteria(Jugador.class).
                add(Restrictions.eq("idjugador", idjugador.intValue())).uniqueResult();
        String name = jugador.getNombre();
        session.getTransaction().commit();
        session.close();
        return name;
    }
    
    public Integer getTorneoxJugInd(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        JugadorTorneo jugadortorneo = (JugadorTorneo) session.createCriteria(JugadorTorneo.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorn.intValue()),
                Restrictions.eq("idjugador", idJug.intValue()))).uniqueResult();
        int ind = jugadortorneo.getIndividual();
        session.getTransaction().commit();
        session.close();
        return ind;
    }
    
    public Integer getTorneoxJugPar(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        JugadorTorneo jugadortorneo = (JugadorTorneo) session.createCriteria(JugadorTorneo.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorn.intValue()),
                Restrictions.eq("idjugador", idJug.intValue()))).uniqueResult();
        int par = jugadortorneo.getPareja();
        session.getTransaction().commit();
        session.close();
        return par;
    }
    
    public Integer getTorneoxJugEqui(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        JugadorTorneo jugadortorneo = (JugadorTorneo) session.createCriteria(JugadorTorneo.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorn.intValue()),
                Restrictions.eq("idjugador", idJug.intValue()))).uniqueResult();
        int equ = jugadortorneo.getEquipo();
        session.getTransaction().commit();
        session.close();
        return equ;
    }
    
    public void saveTorneoxJugInd(Integer idTorn, Integer idJug, Integer i) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        JugadorTorneo jugadortorneo = (JugadorTorneo) session.createCriteria(JugadorTorneo.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorn.intValue()),
                Restrictions.eq("idjugador", idJug.intValue()))).uniqueResult();
        jugadortorneo.setIndividual(i);
        session.saveOrUpdate(jugadortorneo);
        session.getTransaction().commit();
        session.close();
        
    }
    
    public void saveTorneoxJugPar(Integer idTorn, Integer idJug, Integer i) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        JugadorTorneo jugadortorneo = (JugadorTorneo) session.createCriteria(JugadorTorneo.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorn.intValue()),
                Restrictions.eq("idjugador", idJug.intValue()))).uniqueResult();
        jugadortorneo.setPareja(i);
        session.saveOrUpdate(jugadortorneo);
        session.getTransaction().commit();
        session.close();
        
    }
    
    public void saveTorneoxJugEqui(Integer idTorn, Integer idJug, Integer i) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        JugadorTorneo jugadortorneo = (JugadorTorneo) session.createCriteria(JugadorTorneo.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorn.intValue()),
                Restrictions.eq("idjugador", idJug.intValue()))).uniqueResult();
        jugadortorneo.setEquipo(i);
        session.saveOrUpdate(jugadortorneo);
        session.getTransaction().commit();
        session.close();
        
    }
    
    public Integer getIdTorneoxJugPar(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        JugadorTorneo jugadortorneo = (JugadorTorneo) session.createCriteria(JugadorTorneo.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorn.intValue()),
                Restrictions.eq("idjugador", idJug.intValue()))).uniqueResult();
        int id = jugadortorneo.getIdjugador_x_torneo();
        session.getTransaction().commit();
        session.close();
        return id;
    }
    
    public void insertRegistroTorModPareja(RegistroTorneoModPareja registrotorneomodpareja) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.saveOrUpdate(registrotorneomodpareja);
        session.getTransaction().commit();
        session.close();
    }
    
    public void insertRegistroTorModEquipo(RegistroTorneoModEquipo registrotorneomodequipo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.saveOrUpdate(registrotorneomodequipo);
        session.getTransaction().commit();
        session.close();
    }
    
    public void delFormacionEquipo(Integer idTorn, Integer idJug) throws Exception {
        try {
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            FormacionEquipo formacionequipo = (FormacionEquipo) session.createCriteria(FormacionEquipo.class).
                    add(Restrictions.or(
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador1", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador2", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador3", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador4", idJug.intValue())))).uniqueResult();
            Integer id1 = formacionequipo.getIdjugador1();
            Integer id2 = formacionequipo.getIdjugador2();
            Integer id3 = formacionequipo.getIdjugador3();
            Integer id4 = formacionequipo.getIdjugador4();
            if (id1 == null) {
                id1 = 0;
            }
            if (id2 == null) {
                id2 = 0;
            }
            if (id3 == null) {
                id3 = 0;
            }
            if (id4 == null) {
                id4 = 0;
            }
            if (id1.intValue() == idJug.intValue()) {
                if (id1.intValue() > 0 && id2.intValue() == 0 && id3.intValue() == 0 && id4.intValue() == 0) {
                    session.delete(formacionequipo);
                } else {
                    formacionequipo.setIdjugador1(null);
                    session.saveOrUpdate(formacionequipo);
                }
            }
            if (id2.intValue() == idJug.intValue()) {
                if (id1.intValue() == 0 && id2.intValue() > 0 && id3.intValue() == 0 && id4.intValue() == 0) {
                    session.delete(formacionequipo);
                } else {
                    formacionequipo.setIdjugador2(null);
                    session.saveOrUpdate(formacionequipo);
                }
            }
            if (id3.intValue() == idJug.intValue()) {
                if (id1.intValue() == 0 && id2.intValue() == 0 && id3.intValue() > 0 && id4.intValue() == 0) {
                    session.delete(formacionequipo);
                } else {
                    formacionequipo.setIdjugador3(null);
                    session.saveOrUpdate(formacionequipo);
                }
            }
            if (id4.intValue() == idJug.intValue()) {
                if (id1.intValue() == 0 && id2.intValue() == 0 && id3.intValue() == 0 && id4.intValue() > 0) {
                    
                    session.delete(formacionequipo);
                    
                } else {
                    formacionequipo.setIdjugador4(null);
                    session.saveOrUpdate(formacionequipo);
                }
            }
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
    
    public void delRegistroTorneoModEquipo(Integer idTorn, Integer idJug) throws Exception {
        try {
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            RegistroTorneoModEquipo registrotorneomodequipo = (RegistroTorneoModEquipo) session.createCriteria(RegistroTorneoModEquipo.class).
                    add(Restrictions.and(
                    Restrictions.eq("idTorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador", idJug.intValue()))).uniqueResult();
            session.delete(registrotorneomodequipo);
            session.getTransaction().commit();
            session.close();
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
    
    public FormacionEquipo delFormacionEquipo2(Integer idTorn, Integer idJug) throws Exception {
        FormacionEquipo formacionequipo = null;
        try {
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            formacionequipo = (FormacionEquipo) session.createCriteria(FormacionEquipo.class).
                    add(Restrictions.or(
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador1", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador2", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn.intValue()),
                    Restrictions.eq("idjugador3", idJug.intValue())),
                    Restrictions.and(
                    Restrictions.eq("idtorneo", idTorn),
                    Restrictions.eq("idjugador4", idJug)))).uniqueResult();
            session.delete(formacionequipo);
            session.getTransaction().commit();
            session.close();
            
        } catch (Exception ex) {
            System.out.println(ex);
        }
        
        return formacionequipo;
    }
    
    public void saveTorneoxRegistroTorneoModEquipo(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        RegistroTorneoModEquipo registroTorneoModEquipo = (RegistroTorneoModEquipo) session.createCriteria(RegistroTorneoModEquipo.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idTorn.intValue()),
                Restrictions.eq("idjugador", idJug.intValue()))).uniqueResult();
        registroTorneoModEquipo.setActivo(0);
        session.saveOrUpdate(registroTorneoModEquipo);
        session.getTransaction().commit();
        session.close();
        
    }
    
    public void deleteformacionequipo(FormacionEquipo formacionequipo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.delete(formacionequipo);
        session.getTransaction().commit();
        session.close();
    }
    
    public List<RegistroTorneoModPareja> getRegistroTorneoModPareja(Integer id, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(RegistroTorneoModPareja.class).
                add(Restrictions.and(
                Restrictions.ne("idjugador", id.intValue()),
                Restrictions.eq("idTorneo", idtorneo.intValue()),
                Restrictions.eq("activo", 0))).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }
    
    public void insertModPareja(FormacionPareja formacionPareja) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.save(formacionPareja);
        session.getTransaction().commit();
        session.close();
        
    }
    
    public void updatePareja(Integer id, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        RegistroTorneoModPareja registroTorneoModPareja = (RegistroTorneoModPareja) session.createCriteria(RegistroTorneoModPareja.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idtorneo.intValue()),
                Restrictions.eq("idjugador", id.intValue()))).uniqueResult();
        registroTorneoModPareja.setActivo(1);
        session.saveOrUpdate(registroTorneoModPareja);
        session.getTransaction().commit();
        session.close();
        
    }
    
    public void updatePareja2(Integer id, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        RegistroTorneoModPareja registroTorneoModPareja = (RegistroTorneoModPareja) session.createCriteria(RegistroTorneoModPareja.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idtorneo.intValue()),
                Restrictions.eq("idjugador", id.intValue()))).uniqueResult();
        registroTorneoModPareja.setActivo(0);
        session.saveOrUpdate(registroTorneoModPareja);
        session.getTransaction().commit();
        session.close();
        
    }
    
    public FormacionPareja getFormacionPareja(Integer id, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        FormacionPareja formacionPareja = (FormacionPareja) session.createCriteria(FormacionPareja.class).
                add(Restrictions.or(
                Restrictions.and(
                Restrictions.eq("idtorneo", idtorneo.intValue()),
                Restrictions.eq("idjugador1", id.intValue())
                ),
                Restrictions.and(
                Restrictions.eq("idtorneo", idtorneo.intValue()),
                Restrictions.eq("idjugador2", id.intValue())
                ))).uniqueResult();
        session.getTransaction().commit();
        session.close();
        return formacionPareja;
    
    }
    
    public void updateParejaActivo(Integer id, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        RegistroTorneoModPareja registroTorneoModPareja = (RegistroTorneoModPareja) session.createCriteria(RegistroTorneoModPareja.class).
                add(Restrictions.and(
                Restrictions.eq("idTorneo", idtorneo.intValue()),
                Restrictions.eq("idjugador", id.intValue()))).uniqueResult();
        registroTorneoModPareja.setActivo(0);
        session.saveOrUpdate(registroTorneoModPareja);
        session.getTransaction().commit();
        session.close();
        
    }
    
     public void deleteFormacionPareja(FormacionPareja formacionPareja) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.delete(formacionPareja);
        session.getTransaction().commit();
        session.close();
        
    }
}
