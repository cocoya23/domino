package com.domino.dao;

import com.domino.bens.FormacionPareja;
import com.domino.bens.Jugador;
import com.domino.bens.JugadorTorneo;
import com.domino.util.HibernateUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class JugadorDAO {

    public Integer getLogin(String correo, String pass) throws Exception {
        Integer rows;
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(Jugador.class).
                add(Restrictions.and(
                Restrictions.eq("email", correo),
                Restrictions.eq("psw", pass))).list();
        rows = result.size();
        session.getTransaction().commit();
        session.close();
        return rows;
    }
    
    
    public Jugador getNombre(String correo, String pass) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Jugador jugador = (Jugador)session.createCriteria(Jugador.class).
                add(Restrictions.and(
                Restrictions.eq("email", correo),
                Restrictions.eq("psw", pass))).uniqueResult();
        session.getTransaction().commit();
        session.close();
        return jugador;
    }

    public Integer getCorreo(String correo) throws Exception {
        Integer rows;
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(Jugador.class).add(Restrictions.eq("email", correo)).list();
        rows = result.size();
        session.getTransaction().commit();
        session.close();
        return rows;
    }

    public void insertJugador(Jugador jugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.save(jugador);
        session.getTransaction().commit();
        session.close();

    }

    public List<Jugador> getIdJug(String correo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(Jugador.class).add(Restrictions.eq("email", correo)).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }

    public void updateJugador(Jugador jugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.update(jugador);
        session.getTransaction().commit();
        session.close();
    }

    public Integer getIdJugador(String correo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        Jugador jugador = (Jugador) session.createCriteria(Jugador.class).
                add(Restrictions.eq("email", correo)).uniqueResult();
        Integer id = jugador.getIdjugador();
        session.getTransaction().commit();
        session.close();
        return id;
    }

    public void insertJugadorxTorneo(JugadorTorneo jugadortorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        session.save(jugadortorneo);
        session.getTransaction().commit();
        session.close();

    }
    
   
//optimizar esta consulta
    public List<Jugador> Pareja(Integer idjugador, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        String hql = " from Jugador "
                + "  where idjugador in ("
                + "  select idjugador from RegistroTorneoModPareja "
                + "  where idTorneo = " + idtorneo + " and idjugador!=" + idjugador + " and activo = 0)";
        List result = session.createQuery(hql).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }
    
     public List<Jugador> Equipo(Integer idjugador, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        String hql = " from Jugador "
                + "  where idjugador in ("
                + "  select idjugador from RegistroTorneoModEquipo "
                + "  where idTorneo = " + idtorneo + " and idjugador!=" + idjugador + " and activo = 0)";
        System.out.println(hql);
        List result = session.createQuery(hql).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }
  
}
