package com.domino.dao;

import com.domino.bens.Metropoli;
import com.domino.bens.Torneo;
import com.domino.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class MetropoliDAO {

    public List<Metropoli> getMetropoli() throws Exception {

        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createQuery("from Metropoli where idPais=1").list();
        session.getTransaction().commit();
        session.close();

        return result;
    }

    public List<Metropoli> getMetropoliId(Integer id) throws Exception {
        System.out.println("Entro DAO");
        System.out.println(id);
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(Metropoli.class).         
            add(Restrictions.eq("idPais", id)).list();
        session.getTransaction().commit();
        session.close();

        return result;
    }

    public List<Metropoli> getMetropoliName(Integer id) throws Exception {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        List result = session.createCriteria(Metropoli.class).         
            add(Restrictions.eq("idMetropoli", id)).list();
        session.getTransaction().commit();
        session.close();
        return result;
    }
}
