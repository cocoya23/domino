
package com.domino.dao;

import com.domino.bens.Pais;
import com.domino.util.HibernateUtil;
import java.util.List;
import org.hibernate.Session;

public class PaisDAO {
    
    public  List<Pais> getPaises() throws Exception{
            Session session = HibernateUtil.getSession();
            session.beginTransaction();
            List result = session.createQuery( "from Pais" ).list();
            session.getTransaction().commit();
            session.close();
    return result;
    }
}
