package com.domino.bens;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table( name = "paises" )
public class Pais implements Serializable {
    
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "idPais")
    private Integer id;

@Column(name = "pais")
    private String nombre;


public Pais(){
}
      public Pais(Integer id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    
    public Integer getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

  

    public void setId(Integer id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    
}
