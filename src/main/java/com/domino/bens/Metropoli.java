package com.domino.bens;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "metropoli")
public class Metropoli implements Serializable {
    
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name = "idMetropoli") 
    private Integer idMetropoli;
    
@Column(name = "metropoli")
    private String metropoli;

@Column (name = "idPais")
    private Integer idPais;


    public Metropoli () {
    }

    public Metropoli(Integer idMetropoli, String metropoli, Integer idPais) {
        this.idMetropoli = idMetropoli;
        this.metropoli = metropoli;
        this.idPais = idPais;
    }


    public Integer getIdMetropoli() {
        return idMetropoli;
    }

    public void setIdMetropoli(Integer idMetropoli) {
        this.idMetropoli = idMetropoli;
    }

    public String getMetropoli() {
        return metropoli;
    }

    public void setMetropoli(String metropoli) {
        this.metropoli = metropoli;
    }

    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

   
}