package com.domino.bens;

import java.io.Serializable;
import javax.persistence.*;

@Entity
@Table(name = "torneos")
public class Torneo implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idTorneo")
    private Integer idTorneo;
    @Column(name = "torneo")
    private String torneo;
    @Column(name = "ambito")
    private Integer ambito;
    @Column(name = "idPais")
    private Integer idPais;
    @Column(name = "idMetropoli")
    private Integer idMetropoli;
    @Column(name = "sede")
    private String sede;
    @Column(name = "genero")
    private Integer genero;
    @Column(name = "fechaHoraio")
    private String fechaHoraio;
    @Column(name = "formato")
    private Integer formato;
    @Column(name = "numeroMult")
    private Integer numeroMult;
    @Column(name = "modInd")
    private Integer modInd;
    @Column(name = "modIndB1")
    private Float modIndB1;
    @Column(name = "modIndB2")
    private Float modIndB2;
    @Column(name = "modIndB3")
    private Float modIndB3;
    @Column(name = "modPareja")
    private Integer modPareja;
    @Column(name = "modParejaB1")
    private Float modParejaB1;
    @Column(name = "modParejaB2")
    private Float modParejaB2;
    @Column(name = "modParejaB3")
    private Float modParejaB3;
    @Column(name = "modEquipo")
    private Integer modEquipo;
    @Column(name = "modEquipoB1")
    private Float modEquipoB1;
    @Column(name = "modEquipoB2")
    private Float modEquipoB2;
    @Column(name = "modEquipoB3")
    private Float modEquipoB3;
    @Column(name = "metaind")
    private Integer metaind;
    @Column(name = "tiempoPartidaind")
    private Integer tiempoPartidaind;
    @Column(name = "horarioIntDesde")
    private String horarioIntDesde;
    @Column(name = "horarioIntHasta")
    private String horarioIntHasta;
    @Column(name = "horarioSede")
    private String horarioSede;
    @Column(name = "activoInternet")
    private Integer activoInternet;
    @Column(name = "activoTorneo")
    private Integer activoTorneo;
    @Column(name = "partidaind")
    private Integer partidaind;
    @Column(name = "insind")
    private Float insind;
    @Column(name = "partidapar")
    private Integer partidapar;
    @Column(name = "inspar")
    private Float inspar;
    @Column(name = "partidaequ")
    private Integer partidaequ;
    @Column(name = "insequ")
    private Float insequ;
    @Column(name = "horarioSedeHasta")
    private String horarioSedeHasta;
    @Column(name = "metapar")
    private Integer metapar;
    @Column(name = "tiempoPartidapar")
    private Integer tiempoPartidapar;
    @Column(name = "metaequ")
    private Integer metaequ;
    @Column(name = "tiempoPartidaequ")
    private Integer tiempoPartidaequ;
    @Column(name = "fechahorariofin")
    private String fechahorariofin;
    @Column(name = "formatopar")
    private Integer formatopar;
    @Column(name = "numeroMultpar")
    private Integer numeroMultpar;
    @Column(name = "fotmatoequ")
    private Integer fotmatoequ;

    public Torneo() {
    }

    public Torneo(Integer idTorneo, String torneo, Integer ambito, Integer idPais, Integer idMetropoli, String sede, Integer genero, String fechaHoraio, Integer formato, Integer numeroMult, Integer modInd, Float modIndB1, Float modIndB2, Float modIndB3, Integer modPareja, Float modParejaB1, Float modParejaB2, Float modParejaB3, Integer modEquipo, Float modEquipoB1, Float modEquipoB2, Float modEquipoB3, Integer metaind, Integer tiempoPartidaind, String horarioIntDesde, String horarioIntHasta, String horarioSede, Integer activoInternet, Integer activoTorneo, Integer partidaind, Float insind, Integer partidapar, Float inspar, Integer partidaequ, Float insequ, String horarioSedeHasta, Integer metapar, Integer tiempoPartidapar, Integer metaequ, Integer tiempoPartidaequ, String fechahorariofin, Integer formatopar, Integer numeroMultpar, Integer fotmatoequ) {
        this.idTorneo = idTorneo;
        this.torneo = torneo;
        this.ambito = ambito;
        this.idPais = idPais;
        this.idMetropoli = idMetropoli;
        this.sede = sede;
        this.genero = genero;
        this.fechaHoraio = fechaHoraio;
        this.formato = formato;
        this.numeroMult = numeroMult;
        this.modInd = modInd;
        this.modIndB1 = modIndB1;
        this.modIndB2 = modIndB2;
        this.modIndB3 = modIndB3;
        this.modPareja = modPareja;
        this.modParejaB1 = modParejaB1;
        this.modParejaB2 = modParejaB2;
        this.modParejaB3 = modParejaB3;
        this.modEquipo = modEquipo;
        this.modEquipoB1 = modEquipoB1;
        this.modEquipoB2 = modEquipoB2;
        this.modEquipoB3 = modEquipoB3;
        this.metaind = metaind;
        this.tiempoPartidaind = tiempoPartidaind;
        this.horarioIntDesde = horarioIntDesde;
        this.horarioIntHasta = horarioIntHasta;
        this.horarioSede = horarioSede;
        this.activoInternet = activoInternet;
        this.activoTorneo = activoTorneo;
        this.partidaind = partidaind;
        this.insind = insind;
        this.partidapar = partidapar;
        this.inspar = inspar;
        this.partidaequ = partidaequ;
        this.insequ = insequ;
        this.horarioSedeHasta = horarioSedeHasta;
        this.metapar = metapar;
        this.tiempoPartidapar = tiempoPartidapar;
        this.metaequ = metaequ;
        this.tiempoPartidaequ = tiempoPartidaequ;
        this.fechahorariofin = fechahorariofin;
        this.formatopar = formatopar;
        this.numeroMultpar = numeroMultpar;
        this.fotmatoequ=fotmatoequ;
    }

    public Integer getIdTorneo() {
        return idTorneo;
    }

    public void setIdTorneo(Integer idTorneo) {
        this.idTorneo = idTorneo;
    }

    public String getTorneo() {
        return torneo;
    }

    public void setTorneo(String torneo) {
        this.torneo = torneo;
    }

    public Integer getAmbito() {
        return ambito;
    }

    public void setAmbito(Integer ambito) {
        this.ambito = ambito;
    }

    public Integer getIdPais() {
        return idPais;
    }

    public void setIdPais(Integer idPais) {
        this.idPais = idPais;
    }

    public Integer getIdMetropoli() {
        return idMetropoli;
    }

    public void setIdMetropoli(Integer idMetropoli) {
        this.idMetropoli = idMetropoli;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public Integer getGenero() {
        return genero;
    }

    public void setGenero(Integer genero) {
        this.genero = genero;
    }

    public String getFechaHoraio() {
        return fechaHoraio;
    }

    public void setFechaHoraio(String fechaHoraio) {
        this.fechaHoraio = fechaHoraio;
    }

    public Integer getFormato() {
        return formato;
    }

    public void setFormato(Integer formato) {
        this.formato = formato;
    }

    public Integer getNumeroMult() {
        return numeroMult;
    }

    public void setNumeroMult(Integer numeroMult) {
        this.numeroMult = numeroMult;
    }

    public Float getModIndB1() {
        return modIndB1;
    }

    public void setModIndB1(Float modIndB1) {
        this.modIndB1 = modIndB1;
    }

    public Float getModIndB2() {
        return modIndB2;
    }

    public void setModIndB2(Float modIndB2) {
        this.modIndB2 = modIndB2;
    }

    public Float getModIndB3() {
        return modIndB3;
    }

    public void setModIndB3(Float modIndB3) {
        this.modIndB3 = modIndB3;
    }

    public Float getModParejaB1() {
        return modParejaB1;
    }

    public void setModParejaB1(Float modParejaB1) {
        this.modParejaB1 = modParejaB1;
    }

    public Float getModParejaB2() {
        return modParejaB2;
    }

    public void setModParejaB2(Float modParejaB2) {
        this.modParejaB2 = modParejaB2;
    }

    public Float getModParejaB3() {
        return modParejaB3;
    }

    public void setModParejaB3(Float modParejaB3) {
        this.modParejaB3 = modParejaB3;
    }

    public Integer getModEquipo() {
        return modEquipo;
    }

    public void setModEquipo(Integer modEquipo) {
        this.modEquipo = modEquipo;
    }

    public Integer getModInd() {
        return modInd;
    }

    public void setModInd(Integer modInd) {
        this.modInd = modInd;
    }

    public Integer getModPareja() {
        return modPareja;
    }

    public void setModPareja(Integer modPareja) {
        this.modPareja = modPareja;
    }

    public Float getModEquipoB1() {
        return modEquipoB1;
    }

    public void setModEquipoB1(Float modEquipoB1) {
        this.modEquipoB1 = modEquipoB1;
    }

    public Float getModEquipoB2() {
        return modEquipoB2;
    }

    public void setModEquipoB2(Float modEquipoB2) {
        this.modEquipoB2 = modEquipoB2;
    }

    public Float getModEquipoB3() {
        return modEquipoB3;
    }

    public void setModEquipoB3(Float modEquipoB3) {
        this.modEquipoB3 = modEquipoB3;
    }

    public Integer getMetaind() {
        return metaind;
    }

    public void setMetaind(Integer metaind) {
        this.metaind = metaind;
    }

    public Integer getTiempoPartidaind() {
        return tiempoPartidaind;
    }

    public void setTiempoPartidaind(Integer tiempoPartidaind) {
        this.tiempoPartidaind = tiempoPartidaind;
    }

    public String getHorarioIntDesde() {
        return horarioIntDesde;
    }

    public void setHorarioIntDesde(String horarioIntDesde) {
        this.horarioIntDesde = horarioIntDesde;
    }

    public String getHorarioIntHasta() {
        return horarioIntHasta;
    }

    public void setHorarioIntHasta(String horarioIntHasta) {
        this.horarioIntHasta = horarioIntHasta;
    }

    public String getHorarioSede() {
        return horarioSede;
    }

    public void setHorarioSede(String horarioSede) {
        this.horarioSede = horarioSede;
    }

    public Integer getActivoInternet() {
        return activoInternet;
    }

    public void setActivoInternet(Integer activoInternet) {
        this.activoInternet = activoInternet;
    }

    public Integer getActivoTorneo() {
        return activoTorneo;
    }

    public void setActivoTorneo(Integer activoTorneo) {
        this.activoTorneo = activoTorneo;
    }

    public Integer getPartidaind() {
        return partidaind;
    }

    public void setPartidaind(Integer partidaind) {
        this.partidaind = partidaind;
    }

    public Float getInsind() {
        return insind;
    }

    public void setInsind(Float insind) {
        this.insind = insind;
    }

    public Integer getPartidapar() {
        return partidapar;
    }

    public void setPartidapar(Integer partidapar) {
        this.partidapar = partidapar;
    }

    public Float getInspar() {
        return inspar;
    }

    public void setInspar(Float inspar) {
        this.inspar = inspar;
    }

    public Integer getPartidaequ() {
        return partidaequ;
    }

    public void setPartidaequ(Integer partidaequ) {
        this.partidaequ = partidaequ;
    }

    public Float getInsequ() {
        return insequ;
    }

    public void setInsequ(Float insequ) {
        this.insequ = insequ;
    }

    public String getHorarioSedeHasta() {
        return horarioSedeHasta;
    }

    public void setHorarioSedeHasta(String horarioSedeHasta) {
        this.horarioSedeHasta = horarioSedeHasta;
    }

    public Integer getMetapar() {
        return metapar;
    }

    public void setMetapar(Integer metapar) {
        this.metapar = metapar;
    }

    public Integer getTiempoPartidapar() {
        return tiempoPartidapar;
    }

    public void setTiempoPartidapar(Integer tiempoPartidapar) {
        this.tiempoPartidapar = tiempoPartidapar;
    }

    public Integer getMetaequ() {
        return metaequ;
    }

    public void setMetaequ(Integer metaequ) {
        this.metaequ = metaequ;
    }

    public Integer getTiempoPartidaequ() {
        return tiempoPartidaequ;
    }

    public void setTiempoPartidaequ(Integer tiempoPartidaequ) {
        this.tiempoPartidaequ = tiempoPartidaequ;
    }

    public String getFechahorariofin() {
        return fechahorariofin;
    }

    public void setFechahorariofin(String fechahorariofin) {
        this.fechahorariofin = fechahorariofin;
    }

    public Integer getFormatopar() {
        return formatopar;
    }

    public void setFormatopar(Integer formatopar) {
        this.formatopar = formatopar;
    }

    public Integer getNumeroMultpar() {
        return numeroMultpar;
    }

    public void setNumeroMultpar(Integer numeroMultpar) {
        this.numeroMultpar = numeroMultpar;
    }

    public Integer getFotmatoequ() {
        return fotmatoequ;
    }

    public void setFotmatoequ(Integer fotmatoequ) {
        this.fotmatoequ = fotmatoequ;
    }
    
     
}
