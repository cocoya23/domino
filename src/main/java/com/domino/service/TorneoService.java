package com.domino.service;

import com.domino.bens.*;
import com.domino.dao.TorneoDAO;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.hibernate.Session;

@ManagedBean(name = "torneoService", eager = true)
@ApplicationScoped
public class TorneoService implements Serializable {

    TorneoDAO torneoDAO = new TorneoDAO();

    public List<Torneo> getTorneo(Integer idjugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Torneo> torneo = this.torneoDAO.getTorneo(idjugador);
        return torneo;
    }

    public Integer getTorneoxJug(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Integer rows = this.torneoDAO.getTorneoxJug(idTorn, idJug);
        return rows;
    }

    public List<Torneo> getTorneoxJugD(Integer idTorn) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List result = this.torneoDAO.getTorneoxJugD(idTorn);
        return result;
    }

    public List<JugadorTorneo> getTorneoxJugCheck(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List result = this.torneoDAO.getTorneoxJugCheck(idTorn, idJug);
        return result;
    }

    public List<Torneo> getTorneoxPareja(Integer idjugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Torneo> torneo = this.torneoDAO.getTorneoxPareja(idjugador);
        return torneo;
    }

    public List<Torneo> getTorneoxEquipo(Integer idjugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Torneo> torneo = this.torneoDAO.getTorneoxEquipo(idjugador);
        return torneo;
    }

    public void updateJugadorxTorneo(JugadorTorneo jugadortorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDAO.updateJugadorxTorneo(jugadortorneo);
    }

    public void delRegistroTorneoModPareja(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDAO.delRegistroTorneoModPareja(idTorn, idJug);
    }

    public void delRegistroTorneoModEquipo(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDAO.delRegistroTorneoModEquipo(idTorn, idJug);
    }

    public void delFormacionPareja(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDAO.delFormacionPareja(idTorn, idJug);
    }

    public Integer equipoRows(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Integer rows = this.torneoDAO.equipoRows(idTorn, idJug);
        return rows;
    }

    /*
     * public List<RegistroTorneoModEquipo> getRegistroTorneoModEquipo(Integer
     * id) throws Exception { Session session = HibernateUtil.getSession();
     * session.isConnected(); List<RegistroTorneoModEquipo> torneo =
     * this.torneoDAO.getRegistroTorneoModEquipo(id); return torneo;
     *
     * }
     */
    public Integer insertEquipo1(FormacionEquipo formacionequipo, Integer idtorn, Integer idjug1) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Integer idforme = this.torneoDAO.insertEquipo1(formacionequipo, idtorn, idjug1);
        return idforme;

    }

    public void insertEquipo2(FormacionEquipo formacionequipo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDAO.insertEquipo2(formacionequipo);

    }

    public void updateRegistroTorneoModEquipo(Integer idtorn, Integer idjug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDAO.updateRegistroTorneoModEquipo(idtorn, idjug);

    }

    public void insertRegistroTorModPareja(RegistroTorneoModPareja registrotorneomodpareja) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDAO.insertRegistroTorModPareja(registrotorneomodpareja);
    }

    public void insertRegistroTorModEquipo(RegistroTorneoModEquipo registrotorneomodequipo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDAO.insertRegistroTorModEquipo(registrotorneomodequipo);
    }

    public void updateRegistroTorneoModEquipoIn(Integer idtorn, Integer idjug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDAO.updateRegistroTorneoModEquipoIn(idtorn, idjug);


    }

    public FormacionEquipo getFormacionEquipo(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        FormacionEquipo formacionequipo = this.torneoDAO.getFormacionEquipo(idTorn, idJug);
        return formacionequipo;
    }

    public String getNombreEquipo(Integer idjugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        String name = this.torneoDAO.getNombreEquipo(idjugador);
        return name;

    }

    public Integer getTorneoxJugInd(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        int ind = this.torneoDAO.getTorneoxJugInd(idTorn, idJug);
        return ind;
    }

    public void saveTorneoxJugInd(Integer idTorn, Integer idJug, Integer i) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDAO.saveTorneoxJugInd(idTorn, idJug, i);

    }

    public Integer getTorneoxJugPar(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        int par = this.torneoDAO.getTorneoxJugPar(idTorn, idJug);
        return par;
    }

    public Integer getIdTorneoxJugPar(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        int id = this.torneoDAO.getIdTorneoxJugPar(idTorn, idJug);
        return id;
    }

    public void saveTorneoxJugPar(Integer idTorn, Integer idJug, Integer i) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDAO.saveTorneoxJugPar(idTorn, idJug, i);

    }

    public void saveTorneoxJugEqui(Integer idTorn, Integer idJug, Integer i) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDAO.saveTorneoxJugEqui(idTorn, idJug, i);

    }

    public void actualizaEquipo(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        FormacionEquipo formacionequipo = getFormacionEquipo(idTorn, idJug);
        Integer idequipoinsert = formacionequipo.getIdformacionequipo();
        Integer idtorneoinsert = formacionequipo.getIdtorneo();
        Integer idjugador1 = formacionequipo.getIdjugador1();
        Integer idjugador2 = formacionequipo.getIdjugador2();
        Integer idjugador3 = formacionequipo.getIdjugador3();
        Integer idjugador4 = formacionequipo.getIdjugador4();
        if (idjugador1 == null) {
        } else {
            updateRegistroTorneoModEquipoIn(idtorneoinsert, idjugador1);
        }
        if (idjugador2 == null) {
        } else {
            updateRegistroTorneoModEquipoIn(idtorneoinsert, idjugador2);
        }
        if (idjugador3 == null) {
        } else {
            updateRegistroTorneoModEquipoIn(idtorneoinsert, idjugador3);
        }
        if (idjugador4 == null) {
        } else {
            updateRegistroTorneoModEquipoIn(idtorneoinsert, idjugador4);
        }
    }

    public Integer getTorneoxJugEqui(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        int equ = this.torneoDAO.getTorneoxJugEqui(idTorn, idJug);
        return equ;
    }

    public void delFormacionEquipo(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDAO.delFormacionEquipo(idTorn, idJug);
    }

    public void delFormacionEquipo2(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        FormacionEquipo formacionequipo = this.torneoDAO.delFormacionEquipo2(idTorn, idJug);
        Integer id1 = formacionequipo.getIdjugador1();
        Integer id2 = formacionequipo.getIdjugador2();
        Integer id3 = formacionequipo.getIdjugador3();
        Integer id4 = formacionequipo.getIdjugador4();
        if (id1 == null) {
            id1 = 0;
        }
        if (id2 == null) {
            id2 = 0;
        }
        if (id3 == null) {
            id3 = 0;
        }
        if (id4 == null) {
            id4 = 0;
        }
        if (id1.intValue() == idJug.intValue()) {
            if (id1.intValue() > 0 && id2.intValue() == 0 && id3.intValue() == 0 && id4.intValue() == 0) {
                this.torneoDAO.deleteformacionequipo(formacionequipo);
                this.torneoDAO.saveTorneoxRegistroTorneoModEquipo(idTorn, idJug);
            }
        }
        if (id2.intValue() == idJug.intValue()) {
            if (id1.intValue() == 0 && id2.intValue() > 0 && id3.intValue() == 0 && id4.intValue() == 0) {
                this.torneoDAO.deleteformacionequipo(formacionequipo);
                this.torneoDAO.saveTorneoxRegistroTorneoModEquipo(idTorn, idJug);
            }
        }
        if (id3.intValue() == idJug.intValue()) {
            if (id1.intValue() == 0 && id2.intValue() == 0 && id3.intValue() > 0 && id4.intValue() == 0) {
                this.torneoDAO.deleteformacionequipo(formacionequipo);
                this.torneoDAO.saveTorneoxRegistroTorneoModEquipo(idTorn, idJug);
            }
        }
        if (id4.intValue() == idJug.intValue()) {
            if (id1.intValue() == 0 && id2.intValue() == 0 && id3.intValue() == 0 && id4.intValue() > 0) {
                this.torneoDAO.deleteformacionequipo(formacionequipo);
                this.torneoDAO.saveTorneoxRegistroTorneoModEquipo(idTorn, idJug);
            }
        }
    }
}
