package com.domino.service;

import com.domino.bens.*;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.model.SelectItem;

/**
 *
 * @author Sergio Cano
 */
@ManagedBean(name = "cargadatostorneoService", eager = true)
@ApplicationScoped
public class CargaDatosTorneoService implements Serializable {

    @ManagedProperty("#{torneoService}")
    private TorneoService torneoService;
    @ManagedProperty("#{metropoliService}")
    private MetropoliService metropoliService;
    private List<SelectItem> torneos;
    private String ambito;
    private String nomtor;
    private Integer formato;
    private Integer formatopareja;
    private Integer formatoequipo;
    private Integer multibolsa;
    private Integer multibolsapareja;
    private Integer individual;
    private Integer pareja;
    private Integer equipo;
    //variables individual
    private Float modIndB1;
    private Float modIndB2;
    private Float modIndB3;
    private Integer metaind;
    private Integer tiempoPartidaind;
    private Integer partidaind;
    private Float insind;
    //variables pareja
    private Float modparB1;
    private Float modparB2;
    private Float modparB3;
    private Integer metapar;
    private Integer tiempoPartidapar;
    private Integer partidapar;
    private Float inspar;
    //variables equipo
    private Float modequB1;
    private Float modequB2;
    private Float modequB3;
    private Integer metaequ;
    private Integer tiempoPartidaequ;
    private Integer partidaequ;
    private Float insequ;
    private String amb;
    private String lugar;
    private String sede;
    private String genero;
    private String fechadesde;
    private String fechahasta;
    private String desInter;
    private String hasInter;
    private String desdsede;
    private Integer idjugador1;
    private Integer idjugador2;
    private Integer idjugador3;
    private Integer idjugador4;
    private String nombre1;
    private String nombre2;
    private String nombre3;
    private Integer variableequipo1;
    private Integer variableequipo2;
    private Integer variableequipo3;
    private Integer idequipoinsert;
    private Integer idtorneoinsert;
    private String formind;
    private String formpar;
    private String formequ;
    private String bo1i;
    private String bo2i;
    private String bo3i;
    private String bo1p;
    private String bo2p;
    private String bo3p;
    private String bo1e;
    private String bo2e;
    private String bo3e;
    private String insi;
    private String insp;
    private String inse;
    Date fechadesde1;
    Date fechahasta1;
    Date desInter1;
    Date hasInter1;
    Date desdsede1;
    private boolean in;
    private boolean pa;
    private boolean eq;

    public int cargaTorneoRegistro(Integer torneoId, Integer id) {
        torneos = new ArrayList(0);
        int idmet = 0;
        SimpleDateFormat formateador = new SimpleDateFormat(
                "dd 'de' MMMM 'de' yyyy 'a las' hh:mm:ss a");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        int varcontrol = 0;
        try {
            int rows = torneoService.getTorneoxJug(torneoId, id);
            if (rows == 0) {
                for (Torneo torneo : torneoService.getTorneoxJugD(torneoId)) {
                    torneos.add(new SelectItem(torneo.getIdTorneo(), torneo.getTorneo()));
                    formato = torneo.getFormato();
                    formind = format(formato);
                    formatopareja = torneo.getFormatopar();
                    formpar = format(formatopareja);
                    formatoequipo = torneo.getFotmatoequ();
                    formequ = format(formatoequipo);
                    String mul = String.valueOf(torneo.getNumeroMult());
                    multibolsa = multibolsa1(mul);
                    multibolsapareja = torneo.getNumeroMultpar();
                    //Variables individual
                    individual = torneo.getModInd();
                    modIndB1 = torneo.getModIndB1();
                    bo1i = DecimalFormat(modIndB1);
                    modIndB2 = torneo.getModIndB2();
                    bo2i = DecimalFormat(modIndB2);
                    modIndB3 = torneo.getModIndB3();
                    bo3i = DecimalFormat(modIndB3);
                    metaind = torneo.getMetaind();
                    tiempoPartidaind = torneo.getTiempoPartidaind();
                    partidaind = torneo.getPartidaind();
                    insind = torneo.getInsind();
                    insi = DecimalFormat(insind);
                    //Variable pareja
                    pareja = torneo.getModPareja();
                    modparB1 = torneo.getModParejaB1();
                    bo1p = DecimalFormat(modparB1);
                    modparB2 = torneo.getModParejaB2();
                    bo2p = DecimalFormat(modparB2);
                    modparB3 = torneo.getModParejaB3();
                    bo3p = DecimalFormat(modparB3);
                    metapar = torneo.getMetapar();
                    tiempoPartidapar = torneo.getTiempoPartidapar();
                    partidapar = torneo.getPartidapar();
                    inspar = torneo.getInspar();
                    insp = DecimalFormat(inspar);
                    //variable equipo
                    equipo = torneo.getModEquipo();
                    modequB1 = torneo.getModEquipoB1();
                    bo1e = DecimalFormat(modequB1);
                    modequB2 = torneo.getModEquipoB2();
                    modequB3 = torneo.getModEquipoB3();
                    metaequ = torneo.getMetaequ();
                    tiempoPartidaequ = torneo.getTiempoPartidaequ();
                    partidaequ = torneo.getPartidaequ();
                    insequ = torneo.getInsequ();
                    inse = DecimalFormat(insequ);
                    //variables 
                    nomtor = torneo.getTorneo();
                    amb = ambito(torneo.getAmbito());
                    lugar = torneo.getSede();
                    idmet = torneo.getIdMetropoli();
                    genero = genero1(torneo.getGenero());
                    fechadesde1 = sdf.parse(torneo.getFechaHoraio());
                    fechadesde = formateador.format(fechadesde1);
                    fechahasta1 = sdf.parse(torneo.getFechahorariofin());
                    fechahasta = formateador.format(fechahasta1);
                    desInter1 = sdf.parse(torneo.getHorarioIntDesde());
                    desInter = formateador.format(desInter1);
                    hasInter1 = sdf.parse(torneo.getHorarioIntHasta());
                    hasInter = formateador.format(hasInter1);
                    desdsede1 = sdf.parse(torneo.getHorarioSede());
                    desdsede = formateador.format(desdsede1);
                    if (formato == 4) {
                        in = true;
                    } else {
                        in = false;
                    }
                    if (formatopareja == 4) {
                        pa = true;
                    } else {
                        pa = false;
                    }
                    if (formatoequipo == 4) {
                        eq = true;
                    } else {
                        eq = false;
                    }
                }
                varcontrol = 0;
            } else {
                for (Torneo torneo : torneoService.getTorneoxJugD(torneoId)) {
                    torneos.add(new SelectItem(torneo.getIdTorneo(), torneo.getTorneo()));
                    formato = torneo.getFormato();
                    formind = format(formato);
                    formatopareja = torneo.getFormatopar();
                    formpar = format(formatopareja);
                    formatoequipo = torneo.getFotmatoequ();
                    formequ = format(formatoequipo);
                    String mul = String.valueOf(torneo.getNumeroMult());
                    multibolsa = multibolsa1(mul);
                    multibolsapareja = torneo.getNumeroMultpar();
                    //Variables individual
                    individual = torneo.getModInd();
                    modIndB1 = torneo.getModIndB1();
                    bo1i = DecimalFormat(modIndB1);
                    modIndB2 = torneo.getModIndB2();
                    bo2i = DecimalFormat(modIndB2);
                    modIndB3 = torneo.getModIndB3();
                    bo3i = DecimalFormat(modIndB3);
                    metaind = torneo.getMetaind();
                    tiempoPartidaind = torneo.getTiempoPartidaind();
                    partidaind = torneo.getPartidaind();
                    insind = torneo.getInsind();
                    insi = DecimalFormat(insind);
                    //Variable pareja
                    pareja = torneo.getModPareja();
                    modparB1 = torneo.getModParejaB1();
                    bo1p = DecimalFormat(modparB1);
                    modparB2 = torneo.getModParejaB2();
                    bo2p = DecimalFormat(modparB2);
                    modparB3 = torneo.getModParejaB3();
                    bo3p = DecimalFormat(modparB3);
                    metapar = torneo.getMetapar();
                    tiempoPartidapar = torneo.getTiempoPartidapar();
                    partidapar = torneo.getPartidapar();
                    inspar = torneo.getInspar();
                    insp = DecimalFormat(inspar);
                    //variable equipo
                    equipo = torneo.getModEquipo();
                    modequB1 = torneo.getModEquipoB1();
                    bo1e = DecimalFormat(modequB1);
                    modequB2 = torneo.getModEquipoB2();
                    modequB3 = torneo.getModEquipoB3();
                    metaequ = torneo.getMetaequ();
                    tiempoPartidaequ = torneo.getTiempoPartidaequ();
                    partidaequ = torneo.getPartidaequ();
                    insequ = torneo.getInsequ();
                    inse = DecimalFormat(insequ);
                    //variables 
                    nomtor = torneo.getTorneo();
                    amb = ambito(torneo.getAmbito());
                    lugar = torneo.getSede();
                    idmet = torneo.getIdMetropoli();
                    genero = genero1(torneo.getGenero());
                    fechadesde1 = sdf.parse(torneo.getFechaHoraio());
                    fechadesde = formateador.format(fechadesde1);
                    fechahasta1 = sdf.parse(torneo.getFechahorariofin());
                    fechahasta = formateador.format(fechahasta1);
                    desInter1 = sdf.parse(torneo.getHorarioIntDesde());
                    desInter = formateador.format(desInter1);
                    hasInter1 = sdf.parse(torneo.getHorarioIntHasta());
                    hasInter = formateador.format(hasInter1);
                    desdsede1 = sdf.parse(torneo.getHorarioSede());
                    desdsede = formateador.format(desdsede1);
                    if (formato == 4) {
                        in = true;
                    } else {
                        in = false;
                    }
                    if (formatopareja == 4) {
                        pa = true;
                    } else {
                        pa = false;
                    }
                    if (formatoequipo == 4) {
                        eq = true;
                    } else {
                        eq = false;
                    }
                }
                varcontrol = 1;
            }
            for (Metropoli metropoli : metropoliService.getMetropoliName(idmet)) {
                sede = metropoli.getMetropoli();
            }
        } catch (Exception ex) {
            Logger.getLogger(CargaDatosTorneoService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return varcontrol;
    }

    public int modalidad(boolean mod) {
        if (mod == true) {
            return 1;
        } else {
            return 0;
        }
    }

    public String DecimalFormat(float cant) {
        DecimalFormat formateador = new DecimalFormat("###,###.##");
        String x = formateador.format(cant);
        return x;

    }

    public boolean modalidadInt(String mod) {
        if (mod.equals("1")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean modalidadInt2(int mod) {
        if (mod == 1) {
            return true;
        } else {
            return false;
        }
    }

    public String ambito(Integer amb) {

        if (amb == 1) {
            return "Nacional";
        } else {
            return "Internacional";
        }

    }

    public String ambito1(int amb) {

        if (amb == 1) {
            return "Nacional";
        } else {
            return "Internacional";
        }

    }

    public String format(Integer mul) {
        if (mul == 1) {
            return "Sencillo";

        }
        if (mul == 2) {
            return "Multibolsa";
        }
        if (mul == 3) {
            return "Multibolsa";
        }
        if (mul == 4) {
            return "Maya";
        } else {
            return "Formato incorrecto";
        }

    }

    public String genero1(Integer amb) {

        if (amb == 1) {
            return "Masculino";
        }
        if (amb == 2) {
            return "Femenino";
        } else {
            return "Mixto";
        }

    }

    public Integer multibolsa1(String mult) {
        if (mult.equals("2")) {
            System.out.println("Entro a if");
            return 2;
        } else {
            System.out.println("Entro a else");
            return 1;
        }
    }

    public String getAmb() {
        return amb;
    }

    public void setAmb(String amb) {
        this.amb = amb;
    }

    public String getAmbito() {
        return ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }

    public String getBo1e() {
        return bo1e;
    }

    public void setBo1e(String bo1e) {
        this.bo1e = bo1e;
    }

    public String getBo1i() {
        return bo1i;
    }

    public void setBo1i(String bo1i) {
        this.bo1i = bo1i;
    }

    public String getBo1p() {
        return bo1p;
    }

    public void setBo1p(String bo1p) {
        this.bo1p = bo1p;
    }

    public String getBo2e() {
        return bo2e;
    }

    public void setBo2e(String bo2e) {
        this.bo2e = bo2e;
    }

    public String getBo2i() {
        return bo2i;
    }

    public void setBo2i(String bo2i) {
        this.bo2i = bo2i;
    }

    public String getBo2p() {
        return bo2p;
    }

    public void setBo2p(String bo2p) {
        this.bo2p = bo2p;
    }

    public String getBo3e() {
        return bo3e;
    }

    public void setBo3e(String bo3e) {
        this.bo3e = bo3e;
    }

    public String getBo3i() {
        return bo3i;
    }

    public void setBo3i(String bo3i) {
        this.bo3i = bo3i;
    }

    public String getBo3p() {
        return bo3p;
    }

    public void setBo3p(String bo3p) {
        this.bo3p = bo3p;
    }

    public String getDesInter() {
        return desInter;
    }

    public void setDesInter(String desInter) {
        this.desInter = desInter;
    }

    public Date getDesInter1() {
        return desInter1;
    }

    public void setDesInter1(Date desInter1) {
        this.desInter1 = desInter1;
    }

    public String getDesdsede() {
        return desdsede;
    }

    public void setDesdsede(String desdsede) {
        this.desdsede = desdsede;
    }

    public Date getDesdsede1() {
        return desdsede1;
    }

    public void setDesdsede1(Date desdsede1) {
        this.desdsede1 = desdsede1;
    }

    public Integer getEquipo() {
        return equipo;
    }

    public void setEquipo(Integer equipo) {
        this.equipo = equipo;
    }

    public String getFechadesde() {
        return fechadesde;
    }

    public void setFechadesde(String fechadesde) {
        this.fechadesde = fechadesde;
    }

    public Date getFechadesde1() {
        return fechadesde1;
    }

    public void setFechadesde1(Date fechadesde1) {
        this.fechadesde1 = fechadesde1;
    }

    public String getFechahasta() {
        return fechahasta;
    }

    public void setFechahasta(String fechahasta) {
        this.fechahasta = fechahasta;
    }

    public Date getFechahasta1() {
        return fechahasta1;
    }

    public void setFechahasta1(Date fechahasta1) {
        this.fechahasta1 = fechahasta1;
    }

    public Integer getFormato() {
        return formato;
    }

    public void setFormato(Integer formato) {
        this.formato = formato;
    }

    public Integer getFormatopareja() {
        return formatopareja;
    }

    public void setFormatopareja(Integer formatopareja) {
        this.formatopareja = formatopareja;
    }

    public String getFormind() {
        return formind;
    }

    public void setFormind(String formind) {
        this.formind = formind;
    }

    public String getFormpar() {
        return formpar;
    }

    public void setFormpar(String formpar) {
        this.formpar = formpar;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getHasInter() {
        return hasInter;
    }

    public void setHasInter(String hasInter) {
        this.hasInter = hasInter;
    }

    public Date getHasInter1() {
        return hasInter1;
    }

    public void setHasInter1(Date hasInter1) {
        this.hasInter1 = hasInter1;
    }

    public Integer getIdequipoinsert() {
        return idequipoinsert;
    }

    public void setIdequipoinsert(Integer idequipoinsert) {
        this.idequipoinsert = idequipoinsert;
    }

    public Integer getIdjugador1() {
        return idjugador1;
    }

    public void setIdjugador1(Integer idjugador1) {
        this.idjugador1 = idjugador1;
    }

    public Integer getIdjugador2() {
        return idjugador2;
    }

    public void setIdjugador2(Integer idjugador2) {
        this.idjugador2 = idjugador2;
    }

    public Integer getIdjugador3() {
        return idjugador3;
    }

    public void setIdjugador3(Integer idjugador3) {
        this.idjugador3 = idjugador3;
    }

    public Integer getIdjugador4() {
        return idjugador4;
    }

    public void setIdjugador4(Integer idjugador4) {
        this.idjugador4 = idjugador4;
    }

    public Integer getIdtorneoinsert() {
        return idtorneoinsert;
    }

    public void setIdtorneoinsert(Integer idtorneoinsert) {
        this.idtorneoinsert = idtorneoinsert;
    }

    public Integer getIndividual() {
        return individual;
    }

    public void setIndividual(Integer individual) {
        this.individual = individual;
    }

    public String getInse() {
        return inse;
    }

    public void setInse(String inse) {
        this.inse = inse;
    }

    public Float getInsequ() {
        return insequ;
    }

    public void setInsequ(Float insequ) {
        this.insequ = insequ;
    }

    public String getInsi() {
        return insi;
    }

    public void setInsi(String insi) {
        this.insi = insi;
    }

    public Float getInsind() {
        return insind;
    }

    public void setInsind(Float insind) {
        this.insind = insind;
    }

    public String getInsp() {
        return insp;
    }

    public void setInsp(String insp) {
        this.insp = insp;
    }

    public Float getInspar() {
        return inspar;
    }

    public void setInspar(Float inspar) {
        this.inspar = inspar;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public Integer getMetaequ() {
        return metaequ;
    }

    public void setMetaequ(Integer metaequ) {
        this.metaequ = metaequ;
    }

    public Integer getMetaind() {
        return metaind;
    }

    public void setMetaind(Integer metaind) {
        this.metaind = metaind;
    }

    public Integer getMetapar() {
        return metapar;
    }

    public void setMetapar(Integer metapar) {
        this.metapar = metapar;
    }

    public MetropoliService getMetropoliService() {
        return metropoliService;
    }

    public void setMetropoliService(MetropoliService metropoliService) {
        this.metropoliService = metropoliService;
    }

    public Float getModIndB1() {
        return modIndB1;
    }

    public void setModIndB1(Float modIndB1) {
        this.modIndB1 = modIndB1;
    }

    public Float getModIndB2() {
        return modIndB2;
    }

    public void setModIndB2(Float modIndB2) {
        this.modIndB2 = modIndB2;
    }

    public Float getModIndB3() {
        return modIndB3;
    }

    public void setModIndB3(Float modIndB3) {
        this.modIndB3 = modIndB3;
    }

    public Float getModequB1() {
        return modequB1;
    }

    public void setModequB1(Float modequB1) {
        this.modequB1 = modequB1;
    }

    public Float getModequB2() {
        return modequB2;
    }

    public void setModequB2(Float modequB2) {
        this.modequB2 = modequB2;
    }

    public Float getModequB3() {
        return modequB3;
    }

    public void setModequB3(Float modequB3) {
        this.modequB3 = modequB3;
    }

    public Float getModparB1() {
        return modparB1;
    }

    public void setModparB1(Float modparB1) {
        this.modparB1 = modparB1;
    }

    public Float getModparB2() {
        return modparB2;
    }

    public void setModparB2(Float modparB2) {
        this.modparB2 = modparB2;
    }

    public Float getModparB3() {
        return modparB3;
    }

    public void setModparB3(Float modparB3) {
        this.modparB3 = modparB3;
    }

    public Integer getMultibolsa() {
        return multibolsa;
    }

    public void setMultibolsa(Integer multibolsa) {
        this.multibolsa = multibolsa;
    }

    public Integer getMultibolsapareja() {
        return multibolsapareja;
    }

    public void setMultibolsapareja(Integer multibolsapareja) {
        this.multibolsapareja = multibolsapareja;
    }

    public String getNombre1() {
        return nombre1;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public String getNombre2() {
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    public String getNombre3() {
        return nombre3;
    }

    public void setNombre3(String nombre3) {
        this.nombre3 = nombre3;
    }

    public String getNomtor() {
        return nomtor;
    }

    public void setNomtor(String nomtor) {
        this.nomtor = nomtor;
    }

    public Integer getPareja() {
        return pareja;
    }

    public void setPareja(Integer pareja) {
        this.pareja = pareja;
    }

    public Integer getPartidaequ() {
        return partidaequ;
    }

    public void setPartidaequ(Integer partidaequ) {
        this.partidaequ = partidaequ;
    }

    public Integer getPartidaind() {
        return partidaind;
    }

    public void setPartidaind(Integer partidaind) {
        this.partidaind = partidaind;
    }

    public Integer getPartidapar() {
        return partidapar;
    }

    public void setPartidapar(Integer partidapar) {
        this.partidapar = partidapar;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public Integer getTiempoPartidaequ() {
        return tiempoPartidaequ;
    }

    public void setTiempoPartidaequ(Integer tiempoPartidaequ) {
        this.tiempoPartidaequ = tiempoPartidaequ;
    }

    public Integer getTiempoPartidaind() {
        return tiempoPartidaind;
    }

    public void setTiempoPartidaind(Integer tiempoPartidaind) {
        this.tiempoPartidaind = tiempoPartidaind;
    }

    public Integer getTiempoPartidapar() {
        return tiempoPartidapar;
    }

    public void setTiempoPartidapar(Integer tiempoPartidapar) {
        this.tiempoPartidapar = tiempoPartidapar;
    }

    public TorneoService getTorneoService() {
        return torneoService;
    }

    public void setTorneoService(TorneoService torneoService) {
        this.torneoService = torneoService;
    }

    public List<SelectItem> getTorneos() {
        return torneos;
    }

    public void setTorneos(List<SelectItem> torneos) {
        this.torneos = torneos;
    }

    public Integer getVariableequipo1() {
        return variableequipo1;
    }

    public void setVariableequipo1(Integer variableequipo1) {
        this.variableequipo1 = variableequipo1;
    }

    public Integer getVariableequipo2() {
        return variableequipo2;
    }

    public void setVariableequipo2(Integer variableequipo2) {
        this.variableequipo2 = variableequipo2;
    }

    public Integer getVariableequipo3() {
        return variableequipo3;
    }

    public void setVariableequipo3(Integer variableequipo3) {
        this.variableequipo3 = variableequipo3;
    }

    public Integer getFormatoequipo() {
        return formatoequipo;
    }

    public void setFormatoequipo(Integer formatoequipo) {
        this.formatoequipo = formatoequipo;
    }

    public String getFormequ() {
        return formequ;
    }

    public void setFormequ(String formequ) {
        this.formequ = formequ;
    }

    public boolean getEq() {
        return eq;
    }

    public void setEq(boolean eq) {
        this.eq = eq;
    }

    public boolean getIn() {
        return in;
    }

    public void setIn(boolean in) {
        this.in = in;
    }

    public boolean getPa() {
        return pa;
    }

    public void setPa(boolean pa) {
        this.pa = pa;
    }
}
