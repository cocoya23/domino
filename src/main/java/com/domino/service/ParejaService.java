package com.domino.service;

import com.domino.bens.FormacionPareja;
import com.domino.bens.RegistroTorneoModPareja;
import com.domino.dao.TorneoDAO;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Sergio Cano
 */
@ManagedBean(name = "parejaService", eager = true)
@ApplicationScoped
public class ParejaService implements Serializable {

    TorneoDAO torneoDao = new TorneoDAO();
    FormacionPareja formacionPareja;

    public Integer getrowsParejas(Integer torneoId, Integer id) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Integer rows = this.torneoDao.rowsFormacionPareja(torneoId, id);
        return rows;
    }

    public String namePareja(Integer idTorn, Integer idJug) throws Exception {
        String nombre = "";
        Session session = HibernateUtil.getSession();
        session.isConnected();
        formacionPareja = this.torneoDao.namePareja(idTorn, idJug);
        if (idJug.intValue() == formacionPareja.getIdjugador1().intValue()) {
            nombre = this.torneoDao.getNombreEquipo(formacionPareja.getIdjugador2());
        }
        if (idJug.intValue() == formacionPareja.getIdjugador2().intValue()) {
            nombre = this.torneoDao.getNombreEquipo(formacionPareja.getIdjugador1());
        }
        return nombre;
    }

    public List<RegistroTorneoModPareja> getRegistroTorneoModPareja(Integer id, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List result = this.torneoDao.getRegistroTorneoModPareja(id, idtorneo);
        return result;
    }

    public void insertModPareja(FormacionPareja formacionPareja) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDao.insertModPareja(formacionPareja);
    }

    public void updatePareja(Integer id, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDao.updatePareja(id, idtorneo);
    }

    public void deleteFormacionPareja(Integer id, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        FormacionPareja formacionPareja = this.torneoDao.getFormacionPareja(id, idtorneo);
        int id1 = formacionPareja.getIdjugador1();
        int id2 = formacionPareja.getIdjugador2();
        this.torneoDao.updateParejaActivo(id1, idtorneo);
        this.torneoDao.updateParejaActivo(id2, idtorneo);
        this.torneoDao.deleteFormacionPareja(formacionPareja);

    }
    
    
    public Integer idPareja(Integer idTorn, Integer idJug) throws Exception {
        Integer id2=0;
        Session session = HibernateUtil.getSession();
        session.isConnected();
        formacionPareja = this.torneoDao.namePareja(idTorn, idJug);
        if (idJug.intValue() == formacionPareja.getIdjugador1().intValue()) {
            id2=formacionPareja.getIdjugador2();
        }
        if (idJug.intValue() == formacionPareja.getIdjugador2().intValue()) {
            id2=formacionPareja.getIdjugador1();
        }
        return id2;
    }
    
    public void updatePareja2(Integer id, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.torneoDao.updatePareja2(id, idtorneo);
    }
}
