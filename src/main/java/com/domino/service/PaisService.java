package com.domino.service;


import com.domino.bens.Pais;
import com.domino.dao.PaisDAO;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.hibernate.Session;

@ManagedBean(name = "paisService", eager = true)
@ApplicationScoped
public class PaisService implements Serializable{

    public List<Pais> getPaises() throws Exception {

        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Pais> paises = new PaisDAO().getPaises();
        return paises;
    }
}
