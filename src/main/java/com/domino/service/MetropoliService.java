package com.domino.service;


import com.domino.bens.Metropoli;
import com.domino.dao.MetropoliDAO;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.hibernate.Session;

@ManagedBean(name = "metropoliService", eager = true)
@ApplicationScoped
public class MetropoliService implements Serializable{
    
       public List<Metropoli> getMetropoli() throws Exception {

        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Metropoli> metropoli = new MetropoliDAO().getMetropoli();
        return metropoli;
    }
       
     public List<Metropoli> getMetropoliId(Integer id) throws Exception {
        System.out.println(id);
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Metropoli> metropoli = new MetropoliDAO().getMetropoliId(id);
        return metropoli;
    }
     
     public List<Metropoli> getMetropoliName(Integer id) throws Exception {
        System.out.println(id);
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Metropoli> metropoli = new MetropoliDAO().getMetropoliName(id);
        return metropoli;
    }
    
}
