package com.domino.service;

import com.domino.bens.Jugador;
import com.domino.bens.JugadorTorneo;
import com.domino.dao.JugadorDAO;
import com.domino.util.HibernateUtil;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import org.hibernate.Session;

import java.io.Serializable;
import java.util.List;

@ManagedBean(name = "jugadorService", eager = true)
@ApplicationScoped
public class JugadorService implements Serializable {

    JugadorDAO jugadorDao = new JugadorDAO();
    Jugador jugador;

    public Integer getLogin(String correo, String pass) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Integer rows = this.jugadorDao.getLogin(correo, pass);
        return rows;
    }
    
    public String getNombre(String correo, String pass) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.jugador = this.jugadorDao.getNombre(correo, pass);
        String name = jugador.getNombre();
        return name;
    }

    public Integer getCorreo(String correo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Integer rows = this.jugadorDao.getCorreo(correo);
        return rows;
    }

    public void insertJugador(Jugador jugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.jugadorDao.insertJugador(jugador);
    }

    public List<Jugador> getIdJug(String correo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Jugador> jugador = this.jugadorDao.getIdJug(correo);
        return jugador;
    }

    public void updateJugador(Jugador jugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        this.jugadorDao.updateJugador(jugador);
    }

    public Integer getIdJugador(String correo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Integer id = this.jugadorDao.getIdJugador(correo);
        return id;

    }

    public void insertJugadorxTorneo(JugadorTorneo jugadortorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
       this.jugadorDao.insertJugadorxTorneo(jugadortorneo);
    }

    public List<Jugador> Pareja(Integer idjugador, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Jugador> jugador = this.jugadorDao.Pareja(idjugador, idtorneo);
        return jugador;
    }

    public List<Jugador> Equipo(Integer idjugador, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<Jugador> jugador = this.jugadorDao.Equipo(idjugador, idtorneo);
        return jugador;
    }
}
