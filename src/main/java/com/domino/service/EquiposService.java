package com.domino.service;

import com.domino.bens.FormacionEquipo;
import com.domino.bens.RegistroTorneoModEquipo;
import com.domino.dao.TorneoDAO;
import com.domino.util.HibernateUtil;
import java.io.Serializable;
import java.util.Iterator;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import org.hibernate.Session;

@ManagedBean(name = "equiposService", eager = true)
@ApplicationScoped
public class EquiposService implements Serializable {

    @ManagedProperty("#{torneoService}")
    private TorneoService torneoService;
    TorneoDAO torneoDAO = new TorneoDAO();

    public Integer equipoRows(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        Integer rows = this.torneoDAO.equipoRows(idTorn, idJug);
        return rows;
    }

    public List<RegistroTorneoModEquipo> getRegistroTorneoModEquipo(Integer id, Integer idtorneo) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        List<RegistroTorneoModEquipo> result = this.torneoDAO.getRegistroTorneoModEquipo(id, idtorneo);
        FormacionEquipo formacionEquipo = this.torneoDAO.getFormacionEquipo(idtorneo, id);
        if (formacionEquipo == null) {
            return result;
        } else {
            Integer id1 = formacionEquipo.getIdjugador1();
            Integer id2 = formacionEquipo.getIdjugador2();
            Integer id3 = formacionEquipo.getIdjugador3();
            Integer id4 = formacionEquipo.getIdjugador4();

            if (id1 == null) {
                id1 = 0;
            }
            if (id2 == null) {
                id2 = 0;
            }
            if (id3 == null) {
                id3 = 0;
            }
            if (id4 == null) {
                id4 = 0;
            }
            for (int i = 0; i < result.size(); i++) {
                if (result.get(i).getIdjugador() == id1.intValue() || result.get(i).getIdjugador() == id2.intValue() || result.get(i).getIdjugador() == id3.intValue() || result.get(i).getIdjugador() == id4.intValue()) {
                    result.remove(i);
                }

            }
            return result;
        }

    }

    public FormacionEquipo formEquipo(Integer idTorn, Integer idJug) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        FormacionEquipo formacionEquipo = this.torneoDAO.formEquipo(idTorn, idJug);
        return formacionEquipo;
    }

    public String getNombreEquipo(Integer idjugador) throws Exception {
        Session session = HibernateUtil.getSession();
        session.isConnected();
        String name = this.torneoDAO.getNombreEquipo(idjugador);
        return name;

    }

    public int guardaEquipo(Integer equ1, Integer id, Integer torneoId) throws Exception {
        boolean valida = true;
        int valor = 0;
        Integer x = this.torneoDAO.equipoRows(torneoId, id);
        Integer y = this.torneoDAO.equipoRows(torneoId, equ1);

        if (y == 1) {
            if (x == 0) {
                FormacionEquipo formacionEquipo = this.torneoDAO.formEquipo(torneoId, equ1);
                Integer idform = formacionEquipo.getIdformacionequipo();
                Integer equipo1 = formacionEquipo.getIdjugador1();
                Integer equipo2 = formacionEquipo.getIdjugador2();
                Integer equipo3 = formacionEquipo.getIdjugador3();
                Integer equipo4 = formacionEquipo.getIdjugador4();
                if (equipo1 == null && valida == true) {
                    formacionEquipo = new FormacionEquipo(idform, torneoId, id, equipo2, equipo3, equipo4);
                    this.torneoDAO.insertEquipo2(formacionEquipo);
                    valida = false;
                    valor = 1;
                }
                if (equipo2 == null && valida == true) {
                    formacionEquipo = new FormacionEquipo(idform, torneoId, equipo1, id, equipo3, equipo4);
                    this.torneoDAO.insertEquipo2(formacionEquipo);
                    valida = false;
                    valor = 1;
                }
                if (equipo3 == null && valida == true) {
                    formacionEquipo = new FormacionEquipo(idform, torneoId, equipo1, equipo2, id, equipo4);
                    this.torneoDAO.insertEquipo2(formacionEquipo);
                    valida = false;
                    valor = 1;
                }
                if (equipo4 == null && valida == true) {
                    formacionEquipo = new FormacionEquipo(idform, torneoId, equipo1, equipo2, equipo3, id);
                    this.torneoDAO.insertEquipo2(formacionEquipo);
                    valida = false;
                    valor = 1;
                }
                formacionEquipo = this.torneoDAO.formEquipo(torneoId, id);
                equipo1 = formacionEquipo.getIdjugador1();
                equipo2 = formacionEquipo.getIdjugador2();
                equipo3 = formacionEquipo.getIdjugador3();
                equipo4 = formacionEquipo.getIdjugador4();
                if (equipo1 != null && equipo2 != null && equipo3 != null && equipo4 != null) {
                    this.torneoDAO.updateRegistroTorneoModEquipo(torneoId, equipo1);
                    this.torneoDAO.updateRegistroTorneoModEquipo(torneoId, equipo2);
                    this.torneoDAO.updateRegistroTorneoModEquipo(torneoId, equipo3);
                    this.torneoDAO.updateRegistroTorneoModEquipo(torneoId, equipo4);
                    valor = 1;
                }
            } else {
                FormacionEquipo formacionEquipo1 = this.torneoDAO.formEquipo(torneoId, id);
                FormacionEquipo formacionEquipo2 = this.torneoDAO.formEquipo(torneoId, equ1);
                Integer numeroequipo = formacionEquipo1.getIdformacionequipo();
                Integer equipo1 = formacionEquipo1.getIdjugador1();
                Integer equipo2 = formacionEquipo1.getIdjugador2();
                Integer equipo3 = formacionEquipo1.getIdjugador3();
                Integer equipo4 = formacionEquipo1.getIdjugador4();
                Integer equi1 = formacionEquipo2.getIdjugador1();
                Integer equi2 = formacionEquipo2.getIdjugador2();
                Integer equi3 = formacionEquipo2.getIdjugador3();
                Integer equi4 = formacionEquipo2.getIdjugador4();
                int num1 = numeroUsuarios(equipo1);
                int num2 = numeroUsuarios(equipo2);
                int num3 = numeroUsuarios(equipo3);
                int num4 = numeroUsuarios(equipo4);
                int n1 = numeroUsuarios(equi1);
                int n2 = numeroUsuarios(equi2);
                int n3 = numeroUsuarios(equi3);
                int n4 = numeroUsuarios(equi4);
                int res1 = num1 + num2 + num3 + num4;
                int res2 = n1 + n2 + n3 + n4;
                int resultado = res1 + res2;
                if (resultado == 4) {
                    this.torneoDAO.delFormacionEquipo2(torneoId, equ1);
                    if (equi1 == null) {
                    } else {
                        ingresaUsuario(torneoId, id, equi1);
                    }
                    if (equi2 == null) {
                    } else {
                        ingresaUsuario(torneoId, id, equi2);
                    }
                    if (equi3 == null) {
                    } else {
                        ingresaUsuario(torneoId, id, equi3);
                    }
                    if (equi4 == null) {
                    } else {
                        ingresaUsuario(torneoId, id, equi4);
                    }
                    FormacionEquipo formacionEquipo = this.torneoDAO.formEquipo(torneoId, id);
                    equipo1 = formacionEquipo.getIdjugador1();
                    equipo2 = formacionEquipo.getIdjugador2();
                    equipo3 = formacionEquipo.getIdjugador3();
                    equipo4 = formacionEquipo.getIdjugador4();
                    if (equipo1 != null && equipo2 != null && equipo3 != null && equipo4 != null) {
                        this.torneoDAO.updateRegistroTorneoModEquipo(torneoId, equipo1);
                        this.torneoDAO.updateRegistroTorneoModEquipo(torneoId, equipo2);
                        this.torneoDAO.updateRegistroTorneoModEquipo(torneoId, equipo3);
                        this.torneoDAO.updateRegistroTorneoModEquipo(torneoId, equipo4);
                        valor = 1;
                    }

                    valor = 1;
                } else {
                    valor = 0;
                }


            }
        } else {

            if (x == 1) {
                FormacionEquipo formacionEquipo = this.torneoDAO.formEquipo(torneoId, id);
                Integer idform = formacionEquipo.getIdformacionequipo();
                Integer equipo1 = formacionEquipo.getIdjugador1();
                Integer equipo2 = formacionEquipo.getIdjugador2();
                Integer equipo3 = formacionEquipo.getIdjugador3();
                Integer equipo4 = formacionEquipo.getIdjugador4();
                if (equipo1 == null && valida == true) {
                    formacionEquipo = new FormacionEquipo(idform, torneoId, equ1, equipo2, equipo3, equipo4);
                    this.torneoDAO.insertEquipo2(formacionEquipo);
                    valida = false;
                    valor = 1;
                } else if (equipo2 == null && valida == true) {
                    formacionEquipo = new FormacionEquipo(idform, torneoId, equipo1, equ1, equipo3, equipo4);
                    this.torneoDAO.insertEquipo2(formacionEquipo);
                    valida = false;
                    valor = 1;
                } else if (equipo3 == null && valida == true) {
                    formacionEquipo = new FormacionEquipo(idform, torneoId, equipo1, equipo2, equ1, equipo4);
                    this.torneoDAO.insertEquipo2(formacionEquipo);
                    valida = false;
                    valor = 1;
                } else if (equipo4 == null && valida == true) {
                    formacionEquipo = new FormacionEquipo(idform, torneoId, equipo1, equipo2, equipo3, equ1);
                    this.torneoDAO.insertEquipo2(formacionEquipo);
                    valida = false;
                    valor = 1;
                }
                formacionEquipo = this.torneoDAO.formEquipo(torneoId, id);
                equipo1 = formacionEquipo.getIdjugador1();
                equipo2 = formacionEquipo.getIdjugador2();
                equipo3 = formacionEquipo.getIdjugador3();
                equipo4 = formacionEquipo.getIdjugador4();
                if (equipo1 != null && equipo2 != null && equipo3 != null && equipo4 != null) {
                    this.torneoDAO.updateRegistroTorneoModEquipo(torneoId, equipo1);
                    this.torneoDAO.updateRegistroTorneoModEquipo(torneoId, equipo2);
                    this.torneoDAO.updateRegistroTorneoModEquipo(torneoId, equipo3);
                    this.torneoDAO.updateRegistroTorneoModEquipo(torneoId, equipo4);
                    valor = 1;
                }
            } else {
                FormacionEquipo formacionEquipo = new FormacionEquipo(null, torneoId, id, equ1, null, null);
                this.torneoDAO.insertEquipo2(formacionEquipo);
                valor = 1;
            }
        }
        return valor;

    }

    public int numeroUsuarios(Integer x) {
        if (x == null) {
            return 0;
        } else {
            return 1;
        }
    }

    public void ingresaUsuario(Integer torneoId, Integer id, Integer equi) throws Exception {
        FormacionEquipo formacionEquipo3 = this.torneoDAO.formEquipo(torneoId, id);
        Integer numeroequipo = formacionEquipo3.getIdformacionequipo();
        Integer equipo1 = formacionEquipo3.getIdjugador1();
        Integer equipo2 = formacionEquipo3.getIdjugador2();
        Integer equipo3 = formacionEquipo3.getIdjugador3();
        Integer equipo4 = formacionEquipo3.getIdjugador4();
        if (equipo1 == null) {
            formacionEquipo3 = new FormacionEquipo(numeroequipo, torneoId, equi, equipo2, equipo3, equipo4);
            this.torneoDAO.insertEquipo2(formacionEquipo3);
        }
        if (equipo2 == null) {
            formacionEquipo3 = new FormacionEquipo(numeroequipo, torneoId, equipo1, equi, equipo3, equipo4);
            this.torneoDAO.insertEquipo2(formacionEquipo3);
        }
        if (equipo3 == null) {
            formacionEquipo3 = new FormacionEquipo(numeroequipo, torneoId, equipo1, equipo2, equi, equipo4);
            this.torneoDAO.insertEquipo2(formacionEquipo3);
        }
        if (equipo4 == null) {
            formacionEquipo3 = new FormacionEquipo(numeroequipo, torneoId, equipo1, equipo2, equipo3, equi);
            this.torneoDAO.insertEquipo2(formacionEquipo3);
        }
    }

    public void eliminar(Integer torneoId, Integer id, String nombre1) throws Exception {
        FormacionEquipo formacionequipo = torneoService.getFormacionEquipo(torneoId, id);
        Integer idequipoinsert = formacionequipo.getIdformacionequipo();
        Integer idtorneoinsert = formacionequipo.getIdtorneo();
        Integer idjugador1 = formacionequipo.getIdjugador1();
        Integer idjugador2 = formacionequipo.getIdjugador2();
        Integer idjugador3 = formacionequipo.getIdjugador3();
        Integer idjugador4 = formacionequipo.getIdjugador4();
        String nom1;
        String nom2;
        String nom3;
        String nom4;
        if (idjugador1 == null) {
            nom1 = " ";
        } else {
            nom1 = torneoService.getNombreEquipo(idjugador1);
            torneoService.updateRegistroTorneoModEquipoIn(idtorneoinsert, idjugador1);

        }
        if (idjugador2 == null) {
            nom2 = " ";
        } else {
            nom2 = torneoService.getNombreEquipo(idjugador2);
            torneoService.updateRegistroTorneoModEquipoIn(idtorneoinsert, idjugador2);

        }
        if (idjugador3 == null) {
            nom3 = " ";
        } else {
            nom3 = torneoService.getNombreEquipo(idjugador3);
            torneoService.updateRegistroTorneoModEquipoIn(idtorneoinsert, idjugador3);

        }
        if (idjugador4 == null) {
            nom4 = " ";
        } else {
            nom4 = torneoService.getNombreEquipo(idjugador4);
            torneoService.updateRegistroTorneoModEquipoIn(idtorneoinsert, idjugador4);

        }

        if (nom1.equals(nombre1)) {
            formacionequipo = new FormacionEquipo(idequipoinsert, idtorneoinsert, null, idjugador2, idjugador3,
                    idjugador4);
            torneoService.updateRegistroTorneoModEquipoIn(idtorneoinsert, idjugador1);
            torneoService.insertEquipo2(formacionequipo);
            

        }
        if (nom2.equals(nombre1)) {
            formacionequipo = new FormacionEquipo(idequipoinsert, idtorneoinsert, idjugador1, null, idjugador3,
                    idjugador4);
            torneoService.updateRegistroTorneoModEquipoIn(idtorneoinsert, idjugador2);
            torneoService.insertEquipo2(formacionequipo);
            

        }
        if (nom3.equals(nombre1)) {
            formacionequipo = new FormacionEquipo(idequipoinsert, idtorneoinsert, idjugador1, idjugador2, null,
                    idjugador4);
            torneoService.updateRegistroTorneoModEquipoIn(idtorneoinsert, idjugador3);
            torneoService.insertEquipo2(formacionequipo);
           



        }
        if (nom4.equals(nombre1)) {
            formacionequipo = new FormacionEquipo(idequipoinsert, idtorneoinsert, idjugador1, idjugador2, idjugador3, null);
            torneoService.updateRegistroTorneoModEquipoIn(idtorneoinsert, idjugador4);
            torneoService.insertEquipo2(formacionequipo);
        }



    }

    public TorneoDAO getTorneoDAO() {
        return torneoDAO;
    }

    public void setTorneoDAO(TorneoDAO torneoDAO) {
        this.torneoDAO = torneoDAO;
    }

    public TorneoService getTorneoService() {
        return torneoService;
    }

    public void setTorneoService(TorneoService torneoService) {
        this.torneoService = torneoService;
    }
}
