package com.domino.managed;

import com.domino.bens.Jugador;
import com.domino.bens.Metropoli;
import com.domino.bens.Pais;
import com.domino.dao.JugadorDAO;
import com.domino.other.EnviarCorreo;
import com.domino.service.JugadorService;
import com.domino.service.MetropoliService;
import com.domino.service.PaisService;
import static com.sun.faces.facelets.util.Path.context;
import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.inject.Inject;
import static org.primefaces.behavior.confirm.ConfirmBehavior.PropertyKeys.message;

@ManagedBean
@ViewScoped
public class IndexManaged implements Serializable {

    @ManagedProperty("#{paisService}")
    private PaisService paisService;
    @ManagedProperty("#{metropoliService}")
    private MetropoliService metropoliService;
    @ManagedProperty("#{jugadorService}")
    private JugadorService jugadorService;
    private List<SelectItem> paises;
    private List<SelectItem> metropolis;
    private List<Jugador> jugadores;
    //Variables de Jugador
    private Integer metropoliID;
    private Integer paisID;
    private String pass;
    private String email;
    private String cel;
    private String tel;
    private String club;
    private String gen;
    private Date fecha;
    private String disc;
    private String iden;
    private String niden;
    private String rfc;
    private String curp;
    private String name;
    private String check;

    public PaisService getPaisService() {
        return paisService;
    }

    @PostConstruct
    public void init() {
        paises = new ArrayList(0);
        metropolis = new ArrayList(0);
        try {

            for (Pais pais : paisService.getPaises()) {
                paises.add(new SelectItem(pais.getId(), pais.getNombre()));
            }

            for (Metropoli metropoli : metropoliService.getMetropoli()) {
                metropolis.add(new SelectItem(metropoli.getIdMetropoli(), metropoli.getMetropoli()));
            }
            gen = "1";
            SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
            String dateInString = "01-01-1960 10:20:56";
            fecha = sdf.parse(dateInString);
            paisID = 1;
            metropoliID = 93;


        } catch (Exception ex) {
            Logger.getLogger(IndexManaged.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    public void cargaCiudades() {
        System.out.println(paisID);
        metropolis = new ArrayList(0);
        try {

            for (Metropoli metropoli : metropoliService.getMetropoliId(paisID)) {
                metropolis.add(new SelectItem(metropoli.getIdPais(), metropoli.getMetropoli()));
            }
        } catch (Exception ex) {
            Logger.getLogger(IndexManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void insertaUser() throws Exception {
        EnviarCorreo correo = new EnviarCorreo();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String fechaCadena = sdf.format(fecha);
        FacesContext context = FacesContext.getCurrentInstance();
        Integer rows = jugadorService.getCorreo(email);
        if (check.equals("false")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Debe aceptar el aviso de privacidad"));
        } else {
            if (rows > 0) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Usuario Registrado"));
            } else {
                Jugador jugador = new Jugador(null, name, email, pass, cel, tel, paisID, metropoliID, club, gen, fechaCadena, disc, iden, niden, rfc, curp);
                jugadorService.insertJugador(jugador);
                int id = jugador.getIdjugador();
                if (id > 1) {
                    correo.Correo(email, email, pass, name);
                    gen = "1";
                    sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");
                    String dateInString = "01-01-1960 10:20:56";
                    fecha = sdf.parse(dateInString);
                    paisID = 1;
                    metropoliID = 93;
                    pass = "";
                    email = "";
                    cel = "";
                    tel = "";
                    club = "";
                    disc = "Ninguna";
                    iden = "INE";
                    niden = "";
                    rfc = "";
                    curp = "";
                    name = "";
                    check = "false";
                    context.addMessage(null, new FacesMessage("Registro exitoso"));
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "No se pudo realizar el registro"));
                }
            }
        }
    }

    public void setPaisService(PaisService paisService) {
        this.paisService = paisService;
    }

    public List<SelectItem> getPaises() {
        return paises;
    }

    public void setPaises(List<SelectItem> paises) {
        this.paises = paises;
    }

    public Integer getPaisID() {
        return paisID;
    }

    public void setPaisID(Integer paisID) {
        this.paisID = paisID;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCel() {
        return cel;
    }

    public void setCel(String cel) {
        this.cel = cel;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public MetropoliService getMetropoliService() {
        return metropoliService;
    }

    public void setMetropoliService(MetropoliService metropoliService) {
        this.metropoliService = metropoliService;
    }

    public List<SelectItem> getMetropolis() {
        return metropolis;
    }

    public void setMetropolis(List<SelectItem> metropolis) {
        this.metropolis = metropolis;
    }

    public Integer getMetropoliID() {
        return metropoliID;
    }

    public void setMetropoliID(Integer metropoliID) {
        this.metropoliID = metropoliID;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getGen() {
        return gen;
    }

    public void setGen(String gen) {
        this.gen = gen;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDisc() {
        return disc;
    }

    public void setDisc(String disc) {
        this.disc = disc;
    }

    public String getIden() {
        return iden;
    }

    public void setIden(String iden) {
        this.iden = iden;
    }

    public String getNiden() {
        return niden;
    }

    public void setNiden(String niden) {
        this.niden = niden;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public JugadorService getJugadorService() {
        return jugadorService;
    }

    public void setJugadorService(JugadorService jugadorService) {
        this.jugadorService = jugadorService;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    public List<Jugador> getJugadores() {
        return jugadores;
    }

    public void setJugadores(List<Jugador> jugadores) {
        this.jugadores = jugadores;
    }
}
