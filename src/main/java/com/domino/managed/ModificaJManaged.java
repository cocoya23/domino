package com.domino.managed;

import com.domino.bens.Jugador;
import com.domino.bens.Metropoli;
import com.domino.bens.Pais;
import com.domino.bens.Torneo;
import com.domino.dao.JugadorDAO;
import com.domino.service.JugadorService;
import com.domino.service.MetropoliService;
import com.domino.service.PaisService;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

@ManagedBean
@ViewScoped
public class ModificaJManaged implements Serializable {

    Jugador jugador;
    @ManagedProperty("#{jugadorService}")
    private JugadorService jugadorService;
    @ManagedProperty("#{paisService}")
    private PaisService paisService;
    @ManagedProperty("#{metropoliService}")
    private MetropoliService metropoliService;
    private List<SelectItem> paises;
    private List<SelectItem> metropolis;
    private List<SelectItem> jugadores;
    private String sesion;
    private Integer id;
    private String nombre;
    private String movil;
    private String telefonoLocal;
    private Integer pais;
    private Integer metropoli;
    private String club;
    private String genero;
    private String fechaNacimiento;
    private String discapacitado;
    private String identificacionFoto;
    private String numeroIdentificacion;
    private String rfc;
    private String curp;
    private Date fecha;
    private Integer idjugador;
    private String email;
    private String psw;
    private Integer variablerfc;
    private Integer variablecurp;

    @PostConstruct
    public void init()  {
        jugadores = new ArrayList(0);
        paises = new ArrayList(0);
        metropolis = new ArrayList(0);

        try {
            sesion = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("key").toString();
            if (sesion.equals("salir")) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
            } else {
                for (Jugador jugador : jugadorService.getIdJug(sesion)) {
                    nombre = jugador.getNombre();
                    movil = jugador.getMovil();
                    telefonoLocal = jugador.getTelefonoLocal();
                    pais = jugador.getPais();
                    metropoli = jugador.getMetropoli();
                    club = jugador.getClub();
                    genero = jugador.getGenero();
                    fechaNacimiento = jugador.getFechaNacimiento();
                    discapacitado = jugador.getDiscapacitado();
                    identificacionFoto = jugador.getIdentificacionFoto();
                    numeroIdentificacion = jugador.getNumeroIdentificacion();
                    rfc = jugador.getRfc();
                    curp = jugador.getCurp();
                    idjugador = jugador.getIdjugador();
                    email = jugador.getEmail();
                    psw = jugador.getPsw();
                }
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                fecha = sdf.parse(fechaNacimiento);

                for (Pais paisid : paisService.getPaises()) {
                    paises.add(new SelectItem(paisid.getId(), paisid.getNombre()));
                }

                for (Metropoli metropoliid : metropoliService.getMetropoliId(pais)) {
                    metropolis.add(new SelectItem(metropoliid.getIdMetropoli(), metropoliid.getMetropoli()));
                }
                jugador = new Jugador(idjugador, nombre, email, psw, movil, telefonoLocal, pais, metropoli, club,
                        genero, fechaNacimiento, discapacitado, identificacionFoto, numeroIdentificacion, rfc, curp);
                int rfclenght = rfc.length();
                int curplenght = curp.length();

                if (rfclenght > 0) {
                    variablerfc = 1;
                } else {
                    variablerfc = 0;
                }
                if (curplenght > 0) {
                    variablecurp = 1;
                } else {
                    variablecurp = 0;
                }



            }
        } catch (Exception ex) {
            Logger.getLogger(IndexManaged.class.getName()).log(Level.SEVERE, null, ex);

        }

    }

    public void cargaCiudades() {
        System.out.println(pais);
        metropolis = new ArrayList(0);
        try {

            for (Metropoli metropoliid : metropoliService.getMetropoliId(pais)) {
                metropolis.add(new SelectItem(metropoliid.getIdPais(), metropoliid.getMetropoli()));
            }
        } catch (Exception ex) {
            Logger.getLogger(IndexManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void updateUser() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        fechaNacimiento = sdf.format(fecha);
        jugador = new Jugador(idjugador, nombre, email, psw, movil, telefonoLocal, pais, metropoli, club, genero,
                fechaNacimiento, discapacitado, identificacionFoto, numeroIdentificacion, rfc, curp);
        jugadorService.updateJugador(jugador);
        context.addMessage(null, new FacesMessage("Actualizacion exitosa"));

    }

    public void updatePass() throws Exception {
        FacesContext context = FacesContext.getCurrentInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        fechaNacimiento = sdf.format(fecha);
        jugador = new Jugador(idjugador, nombre, email, psw, movil, telefonoLocal, pais, metropoli, club, genero,
                fechaNacimiento, discapacitado, identificacionFoto, numeroIdentificacion, rfc, curp);
        jugadorService.updateJugador(jugador);
        psw = "";
        context.addMessage(null, new FacesMessage("Actualizacion exitosa"));

    }

    public Jugador getJugador() {
        return jugador;
    }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public List<SelectItem> getJugadores() {
        return jugadores;
    }

    public void setJugadores(List<SelectItem> jugadores) {
        this.jugadores = jugadores;
    }

    public String getSesion() {
        return sesion;
    }

    public void setSesion(String sesion) {
        this.sesion = sesion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public JugadorService getJugadorService() {
        return jugadorService;
    }

    public void setJugadorService(JugadorService jugadorService) {
        this.jugadorService = jugadorService;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getClub() {
        return club;
    }

    public void setClub(String club) {
        this.club = club;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Integer getMetropoli() {
        return metropoli;
    }

    public void setMetropoli(Integer metropoli) {
        this.metropoli = metropoli;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public Integer getPais() {
        return pais;
    }

    public void setPais(Integer pais) {
        this.pais = pais;
    }

    public String getTelefonoLocal() {
        return telefonoLocal;
    }

    public void setTelefonoLocal(String telefonoLocal) {
        this.telefonoLocal = telefonoLocal;
    }

    public String getCurp() {
        return curp;
    }

    public void setCurp(String curp) {
        this.curp = curp;
    }

    public String getDiscapacitado() {
        return discapacitado;
    }

    public void setDiscapacitado(String discapacitado) {
        this.discapacitado = discapacitado;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getIdentificacionFoto() {
        return identificacionFoto;
    }

    public void setIdentificacionFoto(String identificacionFoto) {
        this.identificacionFoto = identificacionFoto;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public List<SelectItem> getMetropolis() {
        return metropolis;
    }

    public void setMetropolis(List<SelectItem> metropolis) {
        this.metropolis = metropolis;
    }

    public List<SelectItem> getPaises() {
        return paises;
    }

    public void setPaises(List<SelectItem> paises) {
        this.paises = paises;
    }

    public MetropoliService getMetropoliService() {
        return metropoliService;
    }

    public void setMetropoliService(MetropoliService metropoliService) {
        this.metropoliService = metropoliService;
    }

    public PaisService getPaisService() {
        return paisService;
    }

    public void setPaisService(PaisService paisService) {
        this.paisService = paisService;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getIdjugador() {
        return idjugador;
    }

    public void setIdjugador(Integer idjugador) {
        this.idjugador = idjugador;
    }

    public String getPsw() {
        return psw;
    }

    public void setPsw(String psw) {
        this.psw = psw;
    }

    public Integer getVariablecurp() {
        return variablecurp;
    }

    public void setVariablecurp(Integer variablecurp) {
        this.variablecurp = variablecurp;
    }

    public Integer getVariablerfc() {
        return variablerfc;
    }

    public void setVariablerfc(Integer variablerfc) {
        this.variablerfc = variablerfc;
    }
}
