package com.domino.managed;

import java.io.IOException;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class MenuManaged implements Serializable {

    private String sesion;

    @PostConstruct
    public void init() {
        try {
            sesion = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("key").toString();
            if (sesion.equals("salir")) {

                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
            }
        } catch (IOException ex) {
            Logger.getLogger(PrincipalManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void registroTorneo() {
        try {

            FacesContext.getCurrentInstance().getExternalContext().redirect("registrojugadortorneo.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(MenuManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void modificarDatos() {
        try {

            FacesContext.getCurrentInstance().getExternalContext().redirect("modificajugador.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(MenuManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void salir() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("key", "salir");
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");

        } catch (IOException ex) {
            Logger.getLogger(MenuManaged.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public String getSesion() {
        return sesion;
    }

    public void setSesion(String sesion) {
        this.sesion = sesion;
    }
    
}

