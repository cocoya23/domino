package com.domino.managed;

import com.domino.bens.*;
import com.domino.dao.JugadorDAO;
import com.domino.dao.TorneoDAO;
import com.domino.service.*;
import static com.sun.faces.facelets.util.Path.context;
import java.io.IOException;
import java.io.Serializable;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

@ManagedBean
@ViewScoped
public class TorneoManaged implements Serializable {

    @ManagedProperty("#{equiposService}")
    private EquiposService equiposService;
    @ManagedProperty("#{parejaService}")
    private ParejaService parejaService;
    @ManagedProperty("#{cargadatostorneoService}")
    private CargaDatosTorneoService cargadatostorneoService;
    @ManagedProperty("#{jugadorService}")
    private JugadorService jugadorService;
    @ManagedProperty("#{torneoService}")
    private TorneoService torneoService;
    @ManagedProperty("#{metropoliService}")
    private MetropoliService metropoliService;
    private List<SelectItem> jugadores;
    private List<SelectItem> torneos;
    private List<SelectItem> equipos;
    private List<SelectItem> equipos1;
    private List<SelectItem> equipos2;
    private List<SelectItem> equipos3;
    private Integer equ1;
    private Integer equ2;
    private Integer equ3;
    private Integer variabletorneo;
    private String sesion;
    private Integer id;
    private Integer torneoId;
    private int var;
    private String ambito;
    private String nomtor;
    private Integer formato;
    private Integer formatopareja;
    private Integer multibolsa;
    private Integer multibolsapareja;
    private Integer individual;
    private Integer pareja;
    private Integer equipo;
    //variables individual
    private Float modIndB1;
    private Float modIndB2;
    private Float modIndB3;
    private String metaind;
    private Integer tiempoPartidaind;
    private Integer partidaind;
    private Float insind;
    //variables pareja
    private Float modparB1;
    private Float modparB2;
    private Float modparB3;
    private String metapar;
    private Integer tiempoPartidapar;
    private Integer partidapar;
    private Float inspar;
    //variables equipo
    private Float modequB1;
    private Float modequB2;
    private Float modequB3;
    private String metaequ;
    private Integer tiempoPartidaequ;
    private Integer partidaequ;
    private Float insequ;
    //variables check
    private Boolean chkind;
    private Boolean chkpar;
    private Boolean chkequ;
    //variables pareja
    private String pareja2;
    private Integer variable;
    private Integer jugadorId;
    //variable equipo
    private Integer variablequ;
    Torneo torneo;
    JugadorTorneo jugadorTorneo;
    private String amb;
    private String lugar;
    private String sede;
    private String genero;
    private String fechadesde;
    private String fechahasta;
    private String desInter;
    private String hasInter;
    private String desdsede;
    private Integer variableboton;
    private Integer idjugtorneo;
    JugadorTorneo jugadortorneo;
    private Integer variableequipo;
    Boolean checkequipo;
    Boolean checkequipo2;
    Boolean checkequipo3;
    FormacionEquipo formacionequipo;
    private Integer idequipo;
    private Integer idjugador1;
    private Integer idjugador2;
    private Integer idjugador3;
    private Integer idjugador4;
    private String nombre1;
    private String nombre2;
    private String nombre3;
    private Integer variableequipo1;
    private Integer variableequipo2;
    private Integer variableequipo3;
    private Integer idequipoinsert;
    private Integer idtorneoinsert;
    private String formind;
    private String formpar;
    private String formequ;
    private String bo1i;
    private String bo2i;
    private String bo3i;
    private String bo1p;
    private String bo2p;
    private String bo3p;
    private String bo1e;
    private String bo2e;
    private String bo3e;
    private String insi;
    private String insp;
    private String inse;
    Date fechadesde1;
    Date fechahasta1;
    Date desInter1;
    Date hasInter1;
    Date desdsede1;
    Integer variableactualizar;
    RegistroTorneoModPareja registrotorneomodpareja;
    RegistroTorneoModEquipo registrotorneomodequipo;
    private Integer variableindividual;

    @PostConstruct
    public void init() {


        torneos = new ArrayList(0);
        try {

            sesion = FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("key").toString();
            if (sesion.equals("salir")) {
                FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
            } else {
                id = jugadorService.getIdJugador(sesion);
                for (Torneo torneo : torneoService.getTorneo(id)) {
                    torneos.add(new SelectItem(torneo.getIdTorneo(), torneo.getTorneo()));
                    individual = 0;
                    pareja = 0;
                    equipo = 0;
                    var = 0;
                    formato = 0;
                    multibolsa = 0;
                    chkind = false;
                    chkpar = false;
                    chkequ = false;
                    variabletorneo = 0;
                    variableboton = 1;
                    variableequipo = 0;
                    checkequipo2 = true;
                    checkequipo = false;
                    checkequipo3 = true;
                    variableactualizar = 0;
                    variableindividual = 0;
                }

            }
        } catch (Exception ex) {
            Logger.getLogger(IndexManaged.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Metodo para cargar datos del Jugador y Torneo
    public void cargaTorneoRegistro() throws Exception {
        jugadores = new ArrayList(0);
        equipos = new ArrayList(0);
        equipos2 = new ArrayList(0);
        equipos3 = new ArrayList(0);
        int varcontrol = cargadatostorneoService.cargaTorneoRegistro(torneoId.intValue(), id.intValue());
        variableindividual = 1;
        //variables de torneo general 
        nomtor = this.cargadatostorneoService.getNomtor();
        amb = this.cargadatostorneoService.getAmb();
        lugar = this.cargadatostorneoService.getLugar();
        sede = this.cargadatostorneoService.getSede();
        formato = this.cargadatostorneoService.getFormato();
        formind = this.cargadatostorneoService.getFormind();
        formatopareja = this.cargadatostorneoService.getFormatopareja();
        formpar = this.cargadatostorneoService.getFormpar();
        formequ = this.cargadatostorneoService.getFormequ();


        multibolsa = this.cargadatostorneoService.getMultibolsa();
        multibolsapareja = this.cargadatostorneoService.getMultibolsapareja();
        genero = this.cargadatostorneoService.getGenero();

        //Variables individual
        individual = this.cargadatostorneoService.getIndividual();
        modIndB1 = this.cargadatostorneoService.getModIndB1();
        bo1i = this.cargadatostorneoService.getBo1i();
        modIndB2 = this.cargadatostorneoService.getModIndB2();
        bo2i = this.cargadatostorneoService.getBo2i();
        modIndB3 = this.cargadatostorneoService.getModIndB3();
        bo3i = this.cargadatostorneoService.getBo3i();

        tiempoPartidaind = this.cargadatostorneoService.getTiempoPartidaind();
        partidaind = this.cargadatostorneoService.getPartidaind();
        insind = this.cargadatostorneoService.getInsind();
        insi = this.cargadatostorneoService.getInsi();


        //Variable pareja
        pareja = this.cargadatostorneoService.getPareja();
        modparB1 = this.cargadatostorneoService.getModparB1();
        bo1p = this.cargadatostorneoService.getBo1p();
        modparB2 = this.cargadatostorneoService.getModparB2();
        bo2p = this.cargadatostorneoService.getBo2p();
        modparB3 = this.cargadatostorneoService.getModparB3();
        bo3p = this.cargadatostorneoService.getBo3p();

        tiempoPartidapar = this.cargadatostorneoService.getMetapar();
        partidapar = this.cargadatostorneoService.getPartidapar();
        inspar = this.cargadatostorneoService.getInspar();
        insp = this.cargadatostorneoService.getInsp();


        //variable equipo
        equipo = this.cargadatostorneoService.getEquipo();
        modequB1 = this.cargadatostorneoService.getModequB1();
        bo1e = this.cargadatostorneoService.getBo1e();
        modequB2 = this.cargadatostorneoService.getModequB2();
        modequB3 = this.cargadatostorneoService.getModequB3();

        tiempoPartidaequ = this.cargadatostorneoService.getTiempoPartidaequ();
        partidaequ = this.cargadatostorneoService.getPartidaequ();
        insequ = this.cargadatostorneoService.getInsequ();
        inse = this.cargadatostorneoService.getInse();

        //variables 
        fechadesde1 = this.cargadatostorneoService.getFechadesde1();
        fechadesde = this.cargadatostorneoService.getFechadesde();
        fechahasta1 = this.cargadatostorneoService.getFechadesde1();
        fechahasta = this.cargadatostorneoService.getFechadesde();
        desInter1 = this.cargadatostorneoService.getDesInter1();
        desInter = this.cargadatostorneoService.getDesInter();
        hasInter1 = this.cargadatostorneoService.getHasInter1();
        hasInter = this.cargadatostorneoService.getHasInter();
        desdsede1 = this.cargadatostorneoService.getDesdsede1();
        desdsede = this.cargadatostorneoService.getDesdsede();

        boolean in = this.cargadatostorneoService.getIn();
        boolean pa = this.cargadatostorneoService.getPa();
        boolean eq = this.cargadatostorneoService.getEq();

        if (in == true) {
            metaind = "4 ganados o más con dos de ventaja";
        } else {
            metaind = String.valueOf(this.cargadatostorneoService.getMetaind());
        }
        if (pa == true) {
            metapar = "4 ganados o más con dos de ventaja";
        } else {
            metapar = String.valueOf(this.cargadatostorneoService.getMetapar());
        }
        if (eq == true) {
            metaequ = "4 ganados o más con dos de ventaja";
        } else {
            metaequ = String.valueOf(this.cargadatostorneoService.getMetaequ());
        }


        if (varcontrol == 0) {
            variabletorneo = 1;
            variableboton = 0;
            variableactualizar = 0;
            chkind = false;
            chkpar = false;
            chkequ = false;
        }
        if (varcontrol == 1) {
            variableactualizar = 1;
            variabletorneo = 1;
            variableboton = 1;
            for (JugadorTorneo jugadorTorneo : torneoService.getTorneoxJugCheck(torneoId, id)) {
                chkind = modalidadInt2(jugadorTorneo.getIndividual());
                chkpar = modalidadInt2(jugadorTorneo.getPareja());
                chkequ = modalidadInt2(jugadorTorneo.getEquipo());
            }


            if (chkpar == true) {
                Integer rows = parejaService.getrowsParejas(torneoId, id);
                if (rows == 1) {
                    pareja2 = parejaService.namePareja(torneoId, id);
                    variable = 4;
                } else {
                    variable = 2;
                    for (RegistroTorneoModPareja registroTorneoModPareja : parejaService.getRegistroTorneoModPareja(id, torneoId)) {
                        jugadores.add(new SelectItem(registroTorneoModPareja.getIdjugador(), registroTorneoModPareja.getJugador().getNombre()));
                    }
                }
            }
            if (chkequ == true) {
                Integer rowsequipos = equiposService.equipoRows(torneoId, id);
                variableequipo = 2;
                if (rowsequipos == 1) {
                    FormacionEquipo formacionEquipo = equiposService.formEquipo(torneoId, id);
                    Integer id1 = formacionEquipo.getIdjugador1();
                    Integer id2 = formacionEquipo.getIdjugador2();
                    Integer id3 = formacionEquipo.getIdjugador3();
                    Integer id4 = formacionEquipo.getIdjugador4();
                    id1 = idnull(id1);
                    id2 = idnull(id2);
                    id3 = idnull(id3);
                    id4 = idnull(id4);
                    //If cuando el id  se encuentra en la posision 1
                    if (id.intValue() == id1.intValue()) {
                        if (id2 == 0) {
                            checkequipo = false;
                            variableequipo1 = 1;
                            for (RegistroTorneoModEquipo registroTorneoModEquipo : equiposService.getRegistroTorneoModEquipo(id, torneoId)) {
                                equipos.add(new SelectItem(registroTorneoModEquipo.getIdjugador(), registroTorneoModEquipo.getJugadorequipo().getNombre()));
                            }
                        } else {
                            variableequipo1 = 2;
                            nombre1 = equiposService.getNombreEquipo(id2);

                        }
                        if (id3 == 0) {
                            checkequipo2 = false;
                            variableequipo2 = 1;
                            for (RegistroTorneoModEquipo registroTorneoModEquipo : equiposService.getRegistroTorneoModEquipo(id, torneoId)) {
                                equipos2.add(new SelectItem(registroTorneoModEquipo.getIdjugador(), registroTorneoModEquipo.getJugadorequipo().getNombre()));
                            }
                        } else {
                            variableequipo2 = 2;
                            nombre2 = equiposService.getNombreEquipo(id3);
                        }
                        if (id4 == 0) {
                            checkequipo3 = false;
                            variableequipo3 = 1;
                            for (RegistroTorneoModEquipo registroTorneoModEquipo : equiposService.getRegistroTorneoModEquipo(id, torneoId)) {
                                equipos3.add(new SelectItem(registroTorneoModEquipo.getIdjugador(), registroTorneoModEquipo.getJugadorequipo().getNombre()));
                            }
                        } else {
                            variableequipo3 = 2;
                            nombre3 = equiposService.getNombreEquipo(id4);
                        }
                    }
                    //If cuando el id  se encuentra en la posision 2
                    if (id.intValue() == id2.intValue()) {
                        if (id1 == 0) {
                            checkequipo = false;
                            variableequipo1 = 1;
                            for (RegistroTorneoModEquipo registroTorneoModEquipo : equiposService.getRegistroTorneoModEquipo(id, torneoId)) {
                                equipos.add(new SelectItem(registroTorneoModEquipo.getIdjugador(), registroTorneoModEquipo.getJugadorequipo().getNombre()));
                            }
                        } else {
                            variableequipo1 = 2;
                            nombre1 = equiposService.getNombreEquipo(id1);

                        }
                        if (id3 == 0) {
                            checkequipo2 = false;
                            variableequipo2 = 1;
                            for (RegistroTorneoModEquipo registroTorneoModEquipo : equiposService.getRegistroTorneoModEquipo(id, torneoId)) {
                                equipos2.add(new SelectItem(registroTorneoModEquipo.getIdjugador(), registroTorneoModEquipo.getJugadorequipo().getNombre()));
                            }
                        } else {
                            variableequipo2 = 2;
                            nombre2 = equiposService.getNombreEquipo(id3);
                        }


                        if (id4 == 0) {
                            checkequipo3 = false;
                            variableequipo3 = 1;
                            for (RegistroTorneoModEquipo registroTorneoModEquipo : equiposService.getRegistroTorneoModEquipo(id, torneoId)) {
                                equipos3.add(new SelectItem(registroTorneoModEquipo.getIdjugador(), registroTorneoModEquipo.getJugadorequipo().getNombre()));
                            }
                        } else {
                            variableequipo3 = 2;
                            nombre3 = equiposService.getNombreEquipo(id4);
                        }
                    }
                    //If cuando el id  se encuentra en la posision 3
                    if (id.intValue() == id3.intValue()) {
                        if (id1 == 0) {
                            checkequipo = false;
                            variableequipo1 = 1;
                            for (RegistroTorneoModEquipo registroTorneoModEquipo : equiposService.getRegistroTorneoModEquipo(id, torneoId)) {
                                equipos.add(new SelectItem(registroTorneoModEquipo.getIdjugador(), registroTorneoModEquipo.getJugadorequipo().getNombre()));
                            }
                        } else {
                            variableequipo1 = 2;
                            nombre1 = equiposService.getNombreEquipo(id1);

                        }
                        if (id2 == 0) {
                            checkequipo2 = false;
                            variableequipo2 = 1;
                            for (RegistroTorneoModEquipo registroTorneoModEquipo : equiposService.getRegistroTorneoModEquipo(id, torneoId)) {
                                equipos2.add(new SelectItem(registroTorneoModEquipo.getIdjugador(), registroTorneoModEquipo.getJugadorequipo().getNombre()));
                            }
                        } else {
                            variableequipo2 = 2;
                            nombre2 = equiposService.getNombreEquipo(id2);
                        }
                        if (id4 == 0) {
                            checkequipo3 = false;
                            variableequipo3 = 1;
                            for (RegistroTorneoModEquipo registroTorneoModEquipo : equiposService.getRegistroTorneoModEquipo(id, torneoId)) {
                                equipos3.add(new SelectItem(registroTorneoModEquipo.getIdjugador(), registroTorneoModEquipo.getJugadorequipo().getNombre()));
                            }
                        } else {
                            variableequipo3 = 2;
                            nombre3 = equiposService.getNombreEquipo(id4);
                        }
                    }
                    //If cuando el id  se encuentra en la posision 4
                    if (id.intValue() == id4.intValue()) {
                        if (id1 == 0) {
                            checkequipo = false;
                            variableequipo1 = 1;
                            for (RegistroTorneoModEquipo registroTorneoModEquipo : equiposService.getRegistroTorneoModEquipo(id, torneoId)) {
                                equipos.add(new SelectItem(registroTorneoModEquipo.getIdjugador(), registroTorneoModEquipo.getJugadorequipo().getNombre()));
                            }
                        } else {
                            variableequipo1 = 2;
                            nombre1 = equiposService.getNombreEquipo(id1);

                        }
                        if (id2 == 0) {
                            checkequipo2 = false;
                            variableequipo2 = 1;
                            for (RegistroTorneoModEquipo registroTorneoModEquipo : equiposService.getRegistroTorneoModEquipo(id, torneoId)) {
                                equipos2.add(new SelectItem(registroTorneoModEquipo.getIdjugador(), registroTorneoModEquipo.getJugadorequipo().getNombre()));
                            }
                        } else {
                            variableequipo2 = 2;
                            nombre2 = equiposService.getNombreEquipo(id2);
                        }
                        if (id3 == 0) {
                            checkequipo3 = false;
                            variableequipo3 = 1;
                            for (RegistroTorneoModEquipo registroTorneoModEquipo : equiposService.getRegistroTorneoModEquipo(id, torneoId)) {
                                equipos3.add(new SelectItem(registroTorneoModEquipo.getIdjugador(), registroTorneoModEquipo.getJugadorequipo().getNombre()));
                            }
                        } else {
                            variableequipo3 = 2;
                            nombre3 = equiposService.getNombreEquipo(id3);
                        }
                    }

                } else {
                    variableequipo1 = 1;
                    variableequipo2 = 1;
                    variableequipo3 = 1;
                    variableequipo = 2;
                    checkequipo2 = true;
                    checkequipo3 = true;
                    for (RegistroTorneoModEquipo registroTorneoModEquipo : equiposService.getRegistroTorneoModEquipo(id, torneoId)) {
                        equipos.add(new SelectItem(registroTorneoModEquipo.getIdjugador(), registroTorneoModEquipo.getJugadorequipo().getNombre()));
                    }

                }

            }
        }
    }

    public void insertaRegistro() {
        try {
            if (chkind == false && chkpar == false && chkequ == false) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Debe seleccionar una modalidad"));
                variableboton = 0;
            } else {
                int ind = modalidad(chkind);
                int par = modalidad(chkpar);
                int equ = modalidad(chkequ);
                jugadortorneo = new JugadorTorneo(null, torneoId, id, ind, par, equ);
                jugadorService.insertJugadorxTorneo(jugadortorneo);
                variableactualizar = 1;
                if (chkind == true && chkpar == false && chkequ == false) {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro exitoso:", "Felicidades escogiste modalidad individual"));
                } else {
                    FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Registro exitoso:", "Favor de registrar los compañeros de su modalidad"));
                }
                cargaTorneoRegistro();
            }
        } catch (Exception ex) {
        }
    }

    public void actualizaRegistro() {
        torneos = new ArrayList(0);
        jugadores = new ArrayList(0);
        try {
            if (chkind == false && chkpar == false && chkequ == false) {
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Debe seleccionar una modalidad"));
            } else {
                Integer ind = modalidad(chkind);
                Integer par = modalidad(chkpar);
                Integer equ = modalidad(chkequ);
                if (chkind == true) {
                    int i = torneoService.getTorneoxJugInd(torneoId, id);
                    if (i == 0) {
                        torneoService.saveTorneoxJugInd(torneoId, id, 1);
                    }
                } else {
                    int i = torneoService.getTorneoxJugInd(torneoId, id);
                    if (i == 1) {
                        torneoService.saveTorneoxJugInd(torneoId, id, 0);
                    }
                }
                if (chkpar == true) {
                    int i = torneoService.getTorneoxJugPar(torneoId, id);
                    if (i == 0) {
                        torneoService.saveTorneoxJugPar(torneoId, id, 1);
                        int idjxt = torneoService.getIdTorneoxJugPar(torneoId, id);
                        Date date = new Date();
                        DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String convertido = fechaHora.format(date);
                        registrotorneomodpareja = new RegistroTorneoModPareja(null, idjxt, torneoId, id, 0, convertido);
                        torneoService.insertRegistroTorModPareja(registrotorneomodpareja);
                    }
                } else {
                    int i = torneoService.getTorneoxJugPar(torneoId, id);
                    if (i == 1) {

                        Integer ip = parejaService.idPareja(torneoId, id);
                        torneoService.saveTorneoxJugPar(torneoId, id, 0);
                        torneoService.delFormacionPareja(torneoId, id);
                        torneoService.delRegistroTorneoModPareja(torneoId, id);
                        parejaService.updatePareja2(ip, torneoId);
                        variable = 11;

                    }
                }
                if (chkequ == true) {
                    int i = torneoService.getTorneoxJugEqui(torneoId, id);
                    if (i == 0) {
                        torneoService.saveTorneoxJugEqui(torneoId, id, 1);
                        int idjxt = torneoService.getIdTorneoxJugPar(torneoId, id);
                        Date date = new Date();
                        DateFormat fechaHora = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String convertido = fechaHora.format(date);
                        registrotorneomodequipo = new RegistroTorneoModEquipo(null, idjxt, torneoId, id, 0, convertido);
                        torneoService.insertRegistroTorModEquipo(registrotorneomodequipo);
                    }
                } else {
                    int i = torneoService.getTorneoxJugEqui(torneoId, id);
                    if (i == 1) {
                        variableequipo = 11;
                        torneoService.saveTorneoxJugEqui(torneoId, id, 0);
                        torneoService.actualizaEquipo(torneoId, id);
                        torneoService.delFormacionEquipo(torneoId, id);
                        torneoService.delRegistroTorneoModEquipo(torneoId, id);
                    }
                }
                cargaTorneoRegistro();
            }
        } catch (Exception ex) {
        }
    }

    public void insertaPareja() {
        jugadores = new ArrayList(0);
        try {
            FormacionPareja formacionPareja = new FormacionPareja(null, torneoId, id, jugadorId);
            parejaService.insertModPareja(formacionPareja);
            parejaService.updatePareja(id, torneoId);
            parejaService.updatePareja(jugadorId, torneoId);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Registro exitoso"));
            cargaTorneoRegistro();
        } catch (Exception ex) {
            Logger.getLogger(IndexManaged.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Registro fallido"));
        }
    }

    public void eliminaPareja() {
        try {
            parejaService.deleteFormacionPareja(id, torneoId);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Se eliminó con exito."));
            cargaTorneoRegistro();
        } catch (Exception ex) {
            Logger.getLogger(IndexManaged.class.getName()).log(Level.SEVERE, null, ex);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "No se pudo eliminar."));
        }
    }

    public void guardaEquipo() throws Exception {
        Integer n = equiposService.guardaEquipo(equ1, id, torneoId);
        if (n == 1) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Registro exito."));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "No se pudo guardar."));
        }
        cargaTorneoRegistro();
    }

    public void guardaEquipo2() throws Exception {
        Integer n = equiposService.guardaEquipo(equ2, id, torneoId);
        if (n == 1) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Registro exito."));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "No se pudo guardar."));
        }
        cargaTorneoRegistro();
    }

    public void guardaEquipo3() throws Exception {
        Integer n = equiposService.guardaEquipo(equ3, id, torneoId);
        if (n == 1) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "Registro exito."));
        } else {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "", "No se pudo guardar."));
        }
        cargaTorneoRegistro();
    }

    public void eliminar1() throws Exception {
        equiposService.eliminar(torneoId, id, nombre1);
        cargaTorneoRegistro();
    }

    public void eliminar2() throws Exception {
        equiposService.eliminar(torneoId, id, nombre2);
        cargaTorneoRegistro();
    }

    public void eliminar3() throws Exception {
        equiposService.eliminar(torneoId, id, nombre3);
        cargaTorneoRegistro();
    }

    public int modalidad(boolean mod) {
        if (mod == true) {
            return 1;
        } else {
            return 0;
        }
    }

    public boolean modalidadInt2(int mod) {
        if (mod == 1) {
            return true;
        } else {
            return false;
        }
    }

    public int idnull(Integer id) {
        if (id == null) {
            return 0;
        } else {
            return id;
        }
    }

    public String getSesion() {
        return sesion;
    }

    public void setSesion(String sesion) {
        this.sesion = sesion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public JugadorService getJugadorService() {
        return jugadorService;
    }

    public void setJugadorService(JugadorService jugadorService) {
        this.jugadorService = jugadorService;
    }

    public List<SelectItem> getJugadores() {
        return jugadores;
    }

    public void setJugadores(List<SelectItem> jugadores) {
        this.jugadores = jugadores;
    }

    public List<SelectItem> getTorneos() {
        return torneos;
    }

    public void setTorneos(List<SelectItem> torneos) {
        this.torneos = torneos;
    }

    public TorneoService getTorneoService() {
        return torneoService;
    }

    public void setTorneoService(TorneoService torneoService) {
        this.torneoService = torneoService;
    }

    public Integer getTorneoId() {
        return torneoId;
    }

    public void setTorneoId(Integer torneoId) {
        this.torneoId = torneoId;
    }

    public int getVar() {
        return var;
    }

    public void setVar(int var) {
        this.var = var;
    }

    public String getAmbito() {
        return ambito;
    }

    public void setAmbito(String ambito) {
        this.ambito = ambito;
    }

    public String getNomtor() {
        return nomtor;
    }

    public void setNomtor(String nomtor) {
        this.nomtor = nomtor;
    }

    public Integer getFormato() {
        return formato;
    }

    public void setFormato(Integer formato) {
        this.formato = formato;
    }

    public Integer getIndividual() {
        return individual;
    }

    public void setIndividual(Integer individual) {
        this.individual = individual;
    }

    public Integer getPareja() {
        return pareja;
    }

    public void setPareja(Integer pareja) {
        this.pareja = pareja;
    }

    public Integer getEquipo() {
        return equipo;
    }

    public void setEquipo(Integer equipo) {
        this.equipo = equipo;
    }

    public Integer getMultibolsa() {
        return multibolsa;
    }

    public void setMultibolsa(Integer multibolsa) {
        this.multibolsa = multibolsa;
    }

    public Float getModIndB1() {
        return modIndB1;
    }

    public void setModIndB1(Float modIndB1) {
        this.modIndB1 = modIndB1;
    }

    public Float getModIndB2() {
        return modIndB2;
    }

    public void setModIndB2(Float modIndB2) {
        this.modIndB2 = modIndB2;
    }

    public Float getModIndB3() {
        return modIndB3;
    }

    public void setModIndB3(Float modIndB3) {
        this.modIndB3 = modIndB3;
    }

    public Integer getTiempoPartidaind() {
        return tiempoPartidaind;
    }

    public void setTiempoPartidaind(Integer tiempoPartidaind) {
        this.tiempoPartidaind = tiempoPartidaind;
    }

    public Integer getPartidaind() {
        return partidaind;
    }

    public void setPartidaind(Integer partidaind) {
        this.partidaind = partidaind;
    }

    public Float getInsind() {
        return insind;
    }

    public void setInsind(Float insind) {
        this.insind = insind;
    }

    public Float getModparB1() {
        return modparB1;
    }

    public void setModparB1(Float modparB1) {
        this.modparB1 = modparB1;
    }

    public Float getModparB2() {
        return modparB2;
    }

    public void setModparB2(Float modparB2) {
        this.modparB2 = modparB2;
    }

    public Float getModparB3() {
        return modparB3;
    }

    public void setModparB3(Float modparB3) {
        this.modparB3 = modparB3;
    }

    public Integer getTiempoPartidapar() {
        return tiempoPartidapar;
    }

    public void setTiempoPartidapar(Integer tiempoPartidapar) {
        this.tiempoPartidapar = tiempoPartidapar;
    }

    public Integer getPartidapar() {
        return partidapar;
    }

    public void setPartidapar(Integer partidapar) {
        this.partidapar = partidapar;
    }

    public Float getInspar() {
        return inspar;
    }

    public void setInspar(Float inspar) {
        this.inspar = inspar;
    }

    public Float getModequB1() {
        return modequB1;
    }

    public void setModequB1(Float modequB1) {
        this.modequB1 = modequB1;
    }

    public Float getModequB2() {
        return modequB2;
    }

    public void setModequB2(Float modequB2) {
        this.modequB2 = modequB2;
    }

    public Float getModequB3() {
        return modequB3;
    }

    public void setModequB3(Float modequB3) {
        this.modequB3 = modequB3;
    }

    public Integer getTiempoPartidaequ() {
        return tiempoPartidaequ;
    }

    public void setTiempoPartidaequ(Integer tiempoPartidaequ) {
        this.tiempoPartidaequ = tiempoPartidaequ;
    }

    public Integer getPartidaequ() {
        return partidaequ;
    }

    public void setPartidaequ(Integer partidaequ) {
        this.partidaequ = partidaequ;
    }

    public Float getInsequ() {
        return insequ;
    }

    public void setInsequ(Float insequ) {
        this.insequ = insequ;
    }

    public Boolean getChkind() {
        return chkind;
    }

    public void setChkind(Boolean chkind) {
        this.chkind = chkind;
    }

    public Boolean getChkpar() {
        return chkpar;
    }

    public void setChkpar(Boolean chkpar) {
        this.chkpar = chkpar;
    }

    public Boolean getChkequ() {
        return chkequ;
    }

    public void setChkequ(Boolean chkequ) {
        this.chkequ = chkequ;
    }

    public Integer getVariabletorneo() {
        return variabletorneo;
    }

    public void setVariabletorneo(Integer variabletorneo) {
        this.variabletorneo = variabletorneo;
    }

    public String getPareja2() {
        return pareja2;
    }

    public void setPareja2(String pareja2) {
        this.pareja2 = pareja2;
    }

    public Integer getVariable() {
        return variable;
    }

    public void setVariable(Integer variable) {
        this.variable = variable;
    }

    public Integer getJugadorId() {
        return jugadorId;
    }

    public void setJugadorId(Integer jugadorId) {
        this.jugadorId = jugadorId;
    }

    public Integer getVariablequ() {
        return variablequ;
    }

    public void setVariablequ(Integer variablequ) {
        this.variablequ = variablequ;
    }

    public Torneo getTorneo() {
        return torneo;
    }

    public void setTorneo(Torneo torneo) {
        this.torneo = torneo;
    }

    public JugadorTorneo getJugadorTorneo() {
        return jugadorTorneo;
    }

    public void setJugadorTorneo(JugadorTorneo jugadorTorneo) {
        this.jugadorTorneo = jugadorTorneo;
    }

    public String getAmb() {
        return amb;
    }

    public void setAmb(String amb) {
        this.amb = amb;
    }

    public String getDesInter() {
        return desInter;
    }

    public void setDesInter(String desInter) {
        this.desInter = desInter;
    }

    public String getDesdsede() {
        return desdsede;
    }

    public void setDesdsede(String desdsede) {
        this.desdsede = desdsede;
    }

    public String getFechadesde() {
        return fechadesde;
    }

    public void setFechadesde(String fechadesde) {
        this.fechadesde = fechadesde;
    }

    public String getFechahasta() {
        return fechahasta;
    }

    public void setFechahasta(String fechahasta) {
        this.fechahasta = fechahasta;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getHasInter() {
        return hasInter;
    }

    public void setHasInter(String hasInter) {
        this.hasInter = hasInter;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getSede() {
        return sede;
    }

    public void setSede(String sede) {
        this.sede = sede;
    }

    public Integer getVariableboton() {
        return variableboton;
    }

    public void setVariableboton(Integer variableboton) {
        this.variableboton = variableboton;
    }

    public MetropoliService getMetropoliService() {
        return metropoliService;
    }

    public void setMetropoliService(MetropoliService metropoliService) {
        this.metropoliService = metropoliService;
    }

    public Integer getIdjugtorneo() {
        return idjugtorneo;
    }

    public void setIdjugtorneo(Integer idjugtorneo) {
        this.idjugtorneo = idjugtorneo;
    }

    public Integer getFormatopareja() {
        return formatopareja;
    }

    public void setFormatopareja(Integer formatopareja) {
        this.formatopareja = formatopareja;
    }

    public Integer getMultibolsapareja() {
        return multibolsapareja;
    }

    public void setMultibolsapareja(Integer multibolsapareja) {
        this.multibolsapareja = multibolsapareja;
    }

    public Integer getEqu1() {
        return equ1;
    }

    public void setEqu1(Integer equ1) {
        this.equ1 = equ1;
    }

    public Integer getEqu2() {
        return equ2;
    }

    public void setEqu2(Integer equ2) {
        this.equ2 = equ2;
    }

    public Integer getEqu3() {
        return equ3;
    }

    public void setEqu3(Integer equ3) {
        this.equ3 = equ3;
    }

    public List<SelectItem> getEquipos1() {
        return equipos1;
    }

    public void setEquipos1(List<SelectItem> equipos1) {
        this.equipos1 = equipos1;
    }

    public List<SelectItem> getEquipos2() {
        return equipos2;
    }

    public void setEquipos2(List<SelectItem> equipos2) {
        this.equipos2 = equipos2;
    }

    public List<SelectItem> getEquipos3() {
        return equipos3;
    }

    public void setEquipos3(List<SelectItem> equipos3) {
        this.equipos3 = equipos3;
    }

    public JugadorTorneo getJugadortorneo() {
        return jugadortorneo;
    }

    public void setJugadortorneo(JugadorTorneo jugadortorneo) {
        this.jugadortorneo = jugadortorneo;
    }

    public Integer getVariableequipo() {
        return variableequipo;
    }

    public void setVariableequipo(Integer variableequipo) {
        this.variableequipo = variableequipo;
    }

    public List<SelectItem> getEquipos() {
        return equipos;
    }

    public void setEquipos(List<SelectItem> equipos) {
        this.equipos = equipos;
    }

    public Boolean getCheckequipo2() {
        return checkequipo2;
    }

    public void setCheckequipo2(Boolean checkequipo2) {
        this.checkequipo2 = checkequipo2;
    }

    public Boolean getCheckequipo() {
        return checkequipo;
    }

    public void setCheckequipo(Boolean checkequipo) {
        this.checkequipo = checkequipo;
    }

    public Boolean getCheckequipo3() {
        return checkequipo3;
    }

    public void setCheckequipo3(Boolean checkequipo3) {
        this.checkequipo3 = checkequipo3;
    }

    public FormacionEquipo getFormacionequipo() {
        return formacionequipo;
    }

    public void setFormacionequipo(FormacionEquipo formacionequipo) {
        this.formacionequipo = formacionequipo;
    }

    public Integer getIdequipo() {
        return idequipo;
    }

    public void setIdequipo(Integer idequipo) {
        this.idequipo = idequipo;
    }

    public Integer getIdjugador1() {
        return idjugador1;
    }

    public void setIdjugador1(Integer idjugador1) {
        this.idjugador1 = idjugador1;
    }

    public Integer getIdjugador2() {
        return idjugador2;
    }

    public void setIdjugador2(Integer idjugador2) {
        this.idjugador2 = idjugador2;
    }

    public Integer getIdjugador3() {
        return idjugador3;
    }

    public void setIdjugador3(Integer idjugador3) {
        this.idjugador3 = idjugador3;
    }

    public Integer getIdjugador4() {
        return idjugador4;
    }

    public void setIdjugador4(Integer idjugador4) {
        this.idjugador4 = idjugador4;
    }

    public String getNombre1() {
        return nombre1;
    }

    public void setNombre1(String nombre1) {
        this.nombre1 = nombre1;
    }

    public String getNombre2() {
        return nombre2;
    }

    public void setNombre2(String nombre2) {
        this.nombre2 = nombre2;
    }

    public String getNombre3() {
        return nombre3;
    }

    public void setNombre3(String nombre3) {
        this.nombre3 = nombre3;
    }

    public Integer getVariableequipo1() {
        return variableequipo1;
    }

    public void setVariableequipo1(Integer variableequipo1) {
        this.variableequipo1 = variableequipo1;
    }

    public Integer getVariableequipo2() {
        return variableequipo2;
    }

    public void setVariableequipo2(Integer variableequipo2) {
        this.variableequipo2 = variableequipo2;
    }

    public Integer getVariableequipo3() {
        return variableequipo3;
    }

    public void setVariableequipo3(Integer variableequipo3) {
        this.variableequipo3 = variableequipo3;
    }

    public String getBo1e() {
        return bo1e;
    }

    public void setBo1e(String bo1e) {
        this.bo1e = bo1e;
    }

    public String getBo1i() {
        return bo1i;
    }

    public void setBo1i(String bo1i) {
        this.bo1i = bo1i;
    }

    public String getBo1p() {
        return bo1p;
    }

    public void setBo1p(String bo1p) {
        this.bo1p = bo1p;
    }

    public String getBo2e() {
        return bo2e;
    }

    public void setBo2e(String bo2e) {
        this.bo2e = bo2e;
    }

    public String getBo2i() {
        return bo2i;
    }

    public void setBo2i(String bo2i) {
        this.bo2i = bo2i;
    }

    public String getBo2p() {
        return bo2p;
    }

    public void setBo2p(String bo2p) {
        this.bo2p = bo2p;
    }

    public String getBo3e() {
        return bo3e;
    }

    public void setBo3e(String bo3e) {
        this.bo3e = bo3e;
    }

    public String getBo3i() {
        return bo3i;
    }

    public void setBo3i(String bo3i) {
        this.bo3i = bo3i;
    }

    public String getBo3p() {
        return bo3p;
    }

    public void setBo3p(String bo3p) {
        this.bo3p = bo3p;
    }

    public String getFormind() {
        return formind;
    }

    public void setFormind(String formind) {
        this.formind = formind;
    }

    public String getFormpar() {
        return formpar;
    }

    public void setFormpar(String formpar) {
        this.formpar = formpar;
    }

    public Integer getIdequipoinsert() {
        return idequipoinsert;
    }

    public void setIdequipoinsert(Integer idequipoinsert) {
        this.idequipoinsert = idequipoinsert;
    }

    public Integer getIdtorneoinsert() {
        return idtorneoinsert;
    }

    public void setIdtorneoinsert(Integer idtorneoinsert) {
        this.idtorneoinsert = idtorneoinsert;
    }

    public String getInse() {
        return inse;
    }

    public void setInse(String inse) {
        this.inse = inse;
    }

    public String getInsi() {
        return insi;
    }

    public void setInsi(String insi) {
        this.insi = insi;
    }

    public String getInsp() {
        return insp;
    }

    public void setInsp(String insp) {
        this.insp = insp;
    }

    public Date getDesInter1() {
        return desInter1;
    }

    public void setDesInter1(Date desInter1) {
        this.desInter1 = desInter1;
    }

    public Date getDesdsede1() {
        return desdsede1;
    }

    public void setDesdsede1(Date desdsede1) {
        this.desdsede1 = desdsede1;
    }

    public Date getFechadesde1() {
        return fechadesde1;
    }

    public void setFechadesde1(Date fechadesde1) {
        this.fechadesde1 = fechadesde1;
    }

    public Date getFechahasta1() {
        return fechahasta1;
    }

    public void setFechahasta1(Date fechahasta1) {
        this.fechahasta1 = fechahasta1;
    }

    public Date getHasInter1() {
        return hasInter1;
    }

    public void setHasInter1(Date hasInter1) {
        this.hasInter1 = hasInter1;
    }

    public Integer getVariableactualizar() {
        return variableactualizar;
    }

    public void setVariableactualizar(Integer variableactualizar) {
        this.variableactualizar = variableactualizar;
    }

    public RegistroTorneoModEquipo getRegistrotorneomodequipo() {
        return registrotorneomodequipo;
    }

    public void setRegistrotorneomodequipo(RegistroTorneoModEquipo registrotorneomodequipo) {
        this.registrotorneomodequipo = registrotorneomodequipo;
    }

    public RegistroTorneoModPareja getRegistrotorneomodpareja() {
        return registrotorneomodpareja;
    }

    public void setRegistrotorneomodpareja(RegistroTorneoModPareja registrotorneomodpareja) {
        this.registrotorneomodpareja = registrotorneomodpareja;
    }

    public Integer getVariableindividual() {
        return variableindividual;
    }

    public void setVariableindividual(Integer variableindividual) {
        this.variableindividual = variableindividual;
    }

    public CargaDatosTorneoService getCargadatostorneoService() {
        return cargadatostorneoService;
    }

    public void setCargadatostorneoService(CargaDatosTorneoService cargadatostorneoService) {
        this.cargadatostorneoService = cargadatostorneoService;
    }

    public ParejaService getParejaService() {
        return parejaService;
    }

    public void setParejaService(ParejaService parejaService) {
        this.parejaService = parejaService;
    }

    public EquiposService getEquiposService() {
        return equiposService;
    }

    public void setEquiposService(EquiposService equiposService) {
        this.equiposService = equiposService;
    }

    public String getFormequ() {
        return formequ;
    }

    public void setFormequ(String formequ) {
        this.formequ = formequ;
    }

    public String getMetaequ() {
        return metaequ;
    }

    public void setMetaequ(String metaequ) {
        this.metaequ = metaequ;
    }

    public String getMetaind() {
        return metaind;
    }

    public void setMetaind(String metaind) {
        this.metaind = metaind;
    }

    public String getMetapar() {
        return metapar;
    }

    public void setMetapar(String metapar) {
        this.metapar = metapar;
    }
}
