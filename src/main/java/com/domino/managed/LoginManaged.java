package com.domino.managed;

import com.domino.bens.Jugador;
import com.domino.service.JugadorService;
import com.sun.faces.context.SessionMap;
import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.context.RequestContext;

@ManagedBean
@ViewScoped
public class LoginManaged implements Serializable {

    @ManagedProperty("#{jugadorService}")
    private JugadorService jugadorService;
    private String username;
    private String password;
    private String map;
    Jugador jugador;

    public void login() throws Exception {
        RequestContext context = RequestContext.getCurrentInstance();
        FacesMessage message = null;
        boolean loggedIn = false;
        Integer rows = jugadorService.getLogin(username, password);
        if (rows == 1) {
            String name = jugadorService.getNombre(username, password);
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("key", username);
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("name", name);
            FacesContext.getCurrentInstance().getExternalContext().redirect("jugadorprincipal.xhtml");

        } else {
            loggedIn = false;
            message = new FacesMessage(FacesMessage.SEVERITY_WARN, "Error", "Usuario o Contraseña incorrecto");
            FacesContext.getCurrentInstance().addMessage(null, message);
            context.addCallbackParam("loggedIn", loggedIn);
        }



    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public JugadorService getJugadorService() {
        return jugadorService;
    }

    public void setJugadorService(JugadorService jugadorService) {
        this.jugadorService = jugadorService;
    }

    public String getMap() {
        return map;
    }

    public void setMap(String map) {
        this.map = map;
    }
}
