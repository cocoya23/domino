-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: domino
-- ------------------------------------------------------
-- Server version	5.5.46-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `formacionequipo`
--

DROP TABLE IF EXISTS `formacionequipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formacionequipo` (
  `idformacionequipo` int(11) NOT NULL AUTO_INCREMENT,
  `idtorneo` int(11) DEFAULT NULL,
  `idjugador1` int(11) DEFAULT NULL,
  `idjugador2` int(11) DEFAULT NULL,
  `idjugador3` int(11) DEFAULT NULL,
  `idjugador4` int(11) DEFAULT NULL,
  PRIMARY KEY (`idformacionequipo`),
  KEY `formacionequipo_idtoneo` (`idtorneo`),
  KEY `formacionequipo_idjugador3` (`idjugador1`),
  KEY `formacionequipo_idjugador4` (`idjugador2`),
  CONSTRAINT `formacionequipo_idjugador1` FOREIGN KEY (`idjugador1`) REFERENCES `registrotorneomodequipo` (`idjugador`),
  CONSTRAINT `formacionequipo_idjugador2` FOREIGN KEY (`idjugador2`) REFERENCES `registrotorneomodequipo` (`idjugador`),
  CONSTRAINT `formacionequipo_idjugador3` FOREIGN KEY (`idjugador1`) REFERENCES `registrotorneomodequipo` (`idjugador`),
  CONSTRAINT `formacionequipo_idjugador4` FOREIGN KEY (`idjugador2`) REFERENCES `registrotorneomodequipo` (`idjugador`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formacionequipo`
--

LOCK TABLES `formacionequipo` WRITE;
/*!40000 ALTER TABLE `formacionequipo` DISABLE KEYS */;
INSERT INTO `formacionequipo` VALUES (2,3,11,325,52,NULL);
/*!40000 ALTER TABLE `formacionequipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formacionpareja`
--

DROP TABLE IF EXISTS `formacionpareja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formacionpareja` (
  `idformacionpareja` int(11) NOT NULL AUTO_INCREMENT,
  `idtorneo` int(11) DEFAULT NULL,
  `idjugador1` int(11) DEFAULT NULL,
  `idjugador2` int(11) DEFAULT NULL,
  PRIMARY KEY (`idformacionpareja`),
  KEY `formacionpareja_idtoneo` (`idtorneo`),
  KEY `formacionpareja_idjugador1` (`idjugador1`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formacionpareja`
--

LOCK TABLES `formacionpareja` WRITE;
/*!40000 ALTER TABLE `formacionpareja` DISABLE KEYS */;
INSERT INTO `formacionpareja` VALUES (36,3,327,13);
/*!40000 ALTER TABLE `formacionpareja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jugador_x_torneo`
--

DROP TABLE IF EXISTS `jugador_x_torneo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jugador_x_torneo` (
  `idjugador_x_torneo` int(11) NOT NULL AUTO_INCREMENT,
  `idTorneo` int(11) DEFAULT NULL,
  `idjugador` int(11) DEFAULT NULL,
  `individual` int(11) DEFAULT NULL,
  `pareja` int(11) DEFAULT NULL,
  `equipo` int(11) DEFAULT NULL,
  PRIMARY KEY (`idjugador_x_torneo`),
  KEY `idjugador_idx` (`idjugador`),
  KEY `idTorneo_idx` (`idTorneo`)
) ENGINE=InnoDB AUTO_INCREMENT=284 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jugador_x_torneo`
--

LOCK TABLES `jugador_x_torneo` WRITE;
/*!40000 ALTER TABLE `jugador_x_torneo` DISABLE KEYS */;
INSERT INTO `jugador_x_torneo` VALUES (178,3,11,1,0,1),(179,5,11,1,1,1),(180,12,11,1,1,1),(181,3,12,1,0,1),(182,5,12,1,1,1),(183,12,12,1,1,1),(184,3,13,1,1,1),(185,5,13,1,1,1),(186,12,13,1,1,1),(187,3,14,1,1,1),(188,5,14,1,1,1),(189,12,14,1,1,1),(190,3,15,1,1,1),(191,5,15,1,1,1),(192,12,15,1,1,1),(193,3,16,1,1,1),(194,5,16,1,1,1),(195,12,16,1,1,1),(196,3,17,1,1,1),(197,5,17,1,1,1),(198,12,17,1,1,1),(199,3,18,1,1,1),(200,5,18,1,1,1),(201,12,18,1,1,1),(202,3,19,1,1,1),(203,5,19,1,1,1),(204,12,19,1,1,1),(205,3,20,1,1,1),(206,5,20,1,1,1),(207,12,20,1,1,1),(211,3,22,1,1,1),(212,5,22,1,1,1),(213,12,22,1,1,1),(214,3,23,1,1,1),(215,5,23,1,1,1),(216,12,23,1,1,1),(217,3,24,1,1,1),(218,5,24,1,1,1),(219,12,24,1,1,1),(220,3,25,1,1,0),(221,5,25,1,1,1),(222,12,25,1,1,1),(223,3,26,1,1,1),(224,5,26,1,1,1),(225,12,26,1,1,1),(226,3,27,1,1,0),(227,5,27,1,1,1),(228,12,27,1,1,1),(229,3,29,1,1,1),(230,5,29,1,1,1),(231,12,29,1,1,1),(232,3,30,1,1,0),(233,5,30,1,1,1),(234,12,30,1,1,1),(235,3,31,1,1,0),(236,5,31,1,0,1),(237,12,31,1,1,1),(238,3,32,1,1,1),(239,5,32,1,1,1),(240,12,32,1,1,1),(241,3,33,1,1,1),(242,5,33,1,1,1),(243,12,33,1,1,1),(244,3,34,1,1,1),(245,5,34,1,1,1),(246,12,34,1,1,1),(247,3,35,1,1,1),(248,5,35,1,1,1),(249,12,35,1,1,1),(250,3,36,1,1,0),(251,5,36,1,1,1),(252,12,36,1,1,1),(253,3,37,1,1,1),(271,3,52,1,1,1),(272,3,292,1,1,1),(273,3,133,1,1,1),(274,3,135,1,1,1),(275,3,141,1,1,1),(276,3,146,1,1,1),(277,3,87,0,0,1),(278,3,84,1,1,1),(279,3,328,1,1,1),(280,3,322,1,1,1),(281,3,327,1,1,1),(282,3,326,1,1,1),(283,3,325,1,1,1);
/*!40000 ALTER TABLE `jugador_x_torneo` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 trigger cargaModalidadEquipo
after insert on jugador_x_torneo
for each row 
BEGIN
if (new.equipo = 1) then 
insert into registrotorneomodequipo (idjugador_x_torneo, idTorneo, idjugador,fechaRTME)
values (new.idjugador_x_torneo, new.idTorneo, new.idjugador, now()) ;
        end if;
        
if (new.pareja = 1) then 
insert into registrotorneomodpareja (idjugador_x_torneo, idTorneo, idjugador,fechaRTMP)
values (new.idjugador_x_torneo, new.idTorneo, new.idjugador, now()) ;
        end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `jugadores`
--

DROP TABLE IF EXISTS `jugadores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jugadores` (
  `idjugador` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) DEFAULT NULL,
  `email` varchar(500) DEFAULT NULL,
  `psw` varchar(100) DEFAULT NULL,
  `movil` varchar(15) DEFAULT NULL,
  `telefonoLocal` varchar(15) DEFAULT NULL,
  `pais` varchar(100) DEFAULT NULL,
  `metropoli` varchar(100) DEFAULT NULL,
  `Club` varchar(100) DEFAULT NULL,
  `genero` char(1) DEFAULT NULL,
  `fechaNacimiento` datetime DEFAULT NULL,
  `discapacitado` varchar(50) DEFAULT NULL,
  `identificacionFoto` varchar(50) DEFAULT NULL,
  `numeroIdentificacion` varchar(50) DEFAULT NULL,
  `rfc` varchar(13) DEFAULT NULL,
  `curp` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`idjugador`)
) ENGINE=InnoDB AUTO_INCREMENT=334 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jugadores`
--

LOCK TABLES `jugadores` WRITE;
/*!40000 ALTER TABLE `jugadores` DISABLE KEYS */;
INSERT INTO `jugadores` VALUES (3,'javier ramirez','papachu@yahoo.com','zazil369','5540945533','5555382706','1','93','','1','1954-02-23 00:00:00','Ninguna','IFE','','ravj540223va1',''),(6,'zazil ana ramirez espinosa','zazalatas@gmail.com','zazalatas','5555555555','5555382706','1','121','','1','1988-11-22 00:00:00','Ninguna','INE','','',''),(7,'iizax gisela ramirez espinosa','iizals@gmail.com','iizals','5555555555','5555382706','1','93','','1','1985-02-27 00:00:00','Ninguna','INE','','',''),(8,'nishim angelica ramirez espinosa','nishim@hotmail.com','nishim','5678901234','5555382706','1','93','','1','1980-03-19 00:00:00','Ninguna','INE','','',''),(9,'Naayeli','naayeli@gmail.com','naayeli','5513426799','5546903456','1','97','Pinochas','2','1978-01-13 00:00:00','Ninguna','INE','9809888','RAEN980987','Mnulas3456iuuyg'),(11,'abel carranza ','ab@aa.com','abelca','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(12,'ADOLFO GARZA ACEVEDO ','ad@ad.com','adolfo','','','1','106','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(13,'AGUSTIN ROMERO ','ag@ag.com','agusti','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(14,'AGUSTIN VELOZ ','esmachu@gmail.com','zazil369','','','42','154','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(15,'ALAN RODRIGUEZ ','al@al.com','alanro','','','1','121','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(16,'ALBA M. SAYAGO ','alb@alb.com','albams','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(17,'ALBERTO LABASTIDA CASTILLO ','albe@albe.com','albert','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(18,'ALBERTO MARQUINA ','alber@alber.com','alberto','','','44','168','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(19,'ALEJANDRO FLORES SUAREZ ','ale@ale.com','alejan','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(20,'ALEJANDRO GARCIA VILLARREAL ','alej@alej.com','alejand','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(21,'ALEJANDRO GARZA ','alejandr@alejandr.com','alejandr','556','456546','1','111','gfhgf','1','1960-01-01 00:00:00','Ninguna','INE','565','54546ggffg','gfghf65445'),(22,'ALEJANDRO LOPEZ ','alejandro@alejandro.com','alejandro','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(23,'ALEJANDRO PORTNOY LITWINCZAK ','alp@alp.com','alport','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(24,'ALEJANDRO TOVAR SOSA ','alt@alt.com','aletov','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(25,'ALEJANDRO VASQUEZ ','alev@alev.com','alevaz','','','1','121','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(26,'ALEJANDRO VASQUEZ JR. ','alevazj@alevazj.com','alevaz','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(27,'alejandro zuñiga ','alez@alez.com','alezuñ','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(29,'ALEX AYBAR ','alex@alex.com','alexay','','','35','144','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(30,'ALEXANDER CORDERO ','alexa@alexa.com','alexan','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(31,'ALEXANDER MEJIAS ','alemej@alemej.com','alemej','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(32,'ALFONSO ANAYA ','alf@alf.com','alfons','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(33,'ALFONSO FUKUICHI HIRATA ','alff@alff.com','alffuk','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(34,'ALFONSO LANGLE ','alflan@alflan.com','alflan','','','1','89','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(35,'ALFONSO RODRIGUEZ SALGADO ','alfr@alfr.com','alfrod','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(36,'ALFREDO CERVANTES ','alfc@alfc.com','alfcer','','','11','39','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(37,'ALFREDO DE LEON ALVISO ','alfl@alfl.com','alfleo','fgdxfg','dgffd','1','111','fgdfgd','1','1960-01-01 00:00:00','Ninguna','INE','fgdfdg','fgdfdg','fgfdg'),(38,'ALFREDO DIAZ ','alfd@alfd.com','alfdia','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(39,'ALFREDO DIAZ ','alfd@alfd.com','alfdia','','','1','89','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(40,'ALFREDO GUTIERREZ ','alfg@alfg.com','alfgut','dfsse','eser','44','169','etretr','1','1960-01-01 00:00:00','Ninguna','Pasaporte','rteret','etrer','rterte'),(41,'ALFREDO KAUN NADER   ','alfk@alfk.com','alfkau','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(42,'ALIRIO MIJARES ','alfm@alfm.com','alfmij','','','44','170','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(43,'ALONSO RODRIGUEZ ','alo@alo.com','alonso','','','44','171','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(44,'AMERICO ONTIVEROS ','ame@ame.com','americ','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(45,'ANGEL BONILLA ','ang@ang.com','angelb','','','44','171','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(46,'ANGEL DIAZ ','ange@ange.com','angeld','','','42','155','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(47,'ANGEL MORALES GONZALEZ ','angm@angm.com','angelm','','','1','121','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(48,'ANGEL PARRA ','angp@angp.com','angelp','','','44','172','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(49,'ANGEL ROSADO ','angr@angr.com','angelr','','','42','157','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(52,'Sergio','sergiocano25@gmail.com','sergio17','123456789','123456','1','93','ga','1','1991-02-25 00:00:00','Ninguna','Cartilla','1526','cams910225','cams910225'),(54,'Andres','andrez1303@gmail.com','123456','5541256315','553234433','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(55,'Israel Courtois Hernandez','israelcts@gmail.com','123456','','','1','93','','1','1979-12-26 00:00:00','Ninguna','INE','','',''),(56,'Raymundo Ayala','raymundoa@microsys.com.mx','123456','12345678','55667788','1','93','Ninguna','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(57,'ANGEL SANCHEZ ','as@as.com','angel1','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(58,'ÁNGEL SÁNCHEZ WONG ','asw@asw.com','angels','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(59,'ANGELO FERNANDEZ ','ang1@ang1.com','angelo','','','42','157','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(60,'ANTHONY VASQUEZ ','ant@ant.com','anthony','','','34','141','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(61,'ANTONIO AVILA SAUCEDO ','ant2@ant2.com','antonio','','','1','132','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(62,'ANTONIO CALDERON ','ant3@ant3.com','antonio','','','1','110','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(63,'ANTONIO GUERREO GONZALEZ  ','ant4@ant4.com','antonio','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(64,'ANTONIO MASCIA ','ant5@ant5.com','ANTONIO','','','44','173','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(65,'ANTONIO NACOUD ASKAR ','NAC@nac.com','nacoud','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(66,'ANTONIO PANNACCI ','pan@pan.com','pannacci','','','44','109','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(67,'ANTONIO RIBODO ','rib@rib.com','ribodo','','','42','158','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(68,'antonio rios ','rios@rios.com','antonio','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(69,'ANTONIO RIVERO ','riv@riv.com','rivero','','','44','175','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(70,'AQUILES RAMOS ','aqu@aqu.com','aquiles','','','44','176','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(71,'ARMANDO CARRARA ','arm@arm.com','carrara','','','1','119','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(72,'ARMANDO REYES ','rey@rey.com','armand','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(73,'ARNALDO SILVA ','arn@arn.com','arnaldo','','','44','177','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(74,'arturo bolaños vera ','art@art.com','arturo','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(75,'ARTURO ESTRADA ','artu@artu.com','estrada','','','1','106','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(76,'ARTURO MAZATAN ','art1@art1.com','mazatan','','','1','119','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(77,'ASTRO ALVES ','ast@ast.com','astroa','','','7','28','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(78,'AURA FERNANDEZ ','au@au.com','aurafe','','','44','178','','2','1960-01-01 00:00:00','Ninguna','INE','','',''),(79,'AURELIO BASTIDA MALDONADO ','aur@aur.com','aurelio','','','1','127','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(80,'BASALAY IGOR ','ba@ba.com','basalay','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(81,'BAYRO PEARSON ','bay@bay.com','pearson','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(82,'BEATRIZ NOGUEIRA  ','be@be.com','beatriz','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(83,'BELISARIO DOMÍNGUEZ RIVAS ','bel@bel.com','belisario','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(84,'BELITZA RODRIGUEZ ','bel1@bel1.com','belitza','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(85,'BELOMYTTSEV DENIS ','belo@belo.com','belomy','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(86,'BESLAN ABAZA ','bes@bes.com','beslan','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(87,'BESSARABOV MIKHAIL ','bess@bess.com','bessar','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(88,'BGANDA ASLAN ','bg@bg.com','bganda','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(89,'BLOKHIN OLEG ','bl@bl.com','blokhin','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(90,'BORIS BORZOV ','bo@bo.com','borisb','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(91,'BOSCO LUGO ','bos@bos.com','boscol','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(92,'braulia narväez ','br@br.com','braulia','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(93,'CARLOS BUJANDA ','ca@ca.com','bujanda','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(94,'CARLOS CAPILLA MORA ','car@car.com','capilla','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(95,'CARLOS ECHEVARRIA ','carl@carl.com','echeve','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(96,'carlos ernesto ','carlo@carlo.com','ernesto','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(97,'CARLOS FERREIRA ','carlos@carlos.com','ferreira','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(98,'CARLOS JUAN ARROYO GOMEZ ','ju@ju.com','arroyo','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(99,'CARLOS MARQUINA ','cm@cm.com','marquina','','','44','168','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(100,'CARLOS PEDUZZI ','pe@pe.com','peduzzi','','','1','119','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(101,'CARLOS VALDIVIESO ','va@va.com','valdivieso','','','44','169','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(102,'CAROLINA HIDALGO ','caro@caro.com','carolina','','','44','170','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(103,'CAROLINA MENDEZ ','me@me.com','mendez','','','44','171','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(104,'CASILLAS PABLO ','cas1@cas1.com','casillas','','','1','110','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(106,'CESAR ALVAREZ NAVARRO ','ces@ces.com','alvarez','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(107,'CESAR CAMPOS ','cesa@cesa.com','campos','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(108,'CESAR JAUREGUI ROBLES ','cesar@cesar.com','jauregui','','','1','92','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(109,'CESAR ROMAN DIAZ ESPINOSA ','ro@ro.com','romand','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(110,'CHARLES JARJOUR ','ch@ch.com','charles','','','44','172','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(111,'CHARLIE LEYVA ','cha@cha.com','charlie','','','42','154','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(112,'CHRISTIAND GRAIN ','chr@chr.com','christiano','','','2','4','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(113,'CIPRIANO BORGES ','ci@ci.com','cipriano','','','1','92','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(115,'CLYNAY NEIL ','cly@cly.com','clynay','','','12','43','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(116,'DAMASAO CANTU GONZALEZ ','da@da.com','DAMASAO','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(117,'DANIEL RIOS ','dan@dan.com','daniel','','','1','88','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(118,'DANIEL SALOMON PASOL ','dani@dani.com','daniel','','','1','113','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(119,'DANIEL VILLARREAL SALAZAR ','vi@vi.com','villarreal','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(120,'DANIELE FACCIUTO ','danie@danie.com','facciuto','','','42','155','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(121,'DAVID COCCO ','dav@dav.com','cocco1','','','42','157','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(122,'DAVID HERREJON ','he@he.com','herrejon','','','1','89','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(123,'DAVID HINOJOSA SEPULVEDA ','hi@hi.com','hinojosa','','','1','90','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(124,'DAVID LEON ','le@le.com','leon12','','','34','140','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(125,'DAVID MILLAN RAMIREZ ','mi@mi.com','millan','','','1','91','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(126,'DAYURMI GARCES ','day@day.com','dayurmi','','','44','173','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(127,'DOMINGO QUIROZ ','do@do.com','domingo','','','42','158','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(128,'don soriano ','don@don.com','soriano','','','42','159','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(130,'DUDU SILVEIRA ','du@du.com','silveira','','','7','26','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(131,'DULCE RUIZ ','dul@dul.com','dulcer','','','44','109','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(132,'EDA LAREZ ','eda@eda.com','edalarez','','','44','175','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(133,'EDECIO PABUENA ','ede@ede.com','edecio','','','11','38','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(134,'EDGAR RODRIGUEZ ','ed@ed.com','edgarr','','','44','175','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(135,'EDGAR ROJAS ','edg@edg.com','edgarr','','','44','176','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(136,'EDGAR WONG ','edga@edga.com','edgarw','','','3','5','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(137,'EDGARDO CANTILLO DELAHOZ ','edgar@edgar.com','cantillo','','','11','39','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(138,'EDGARDO CANTILLO JR. ','edgarjr@edgarjr.com','junior','','','11','40','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(139,'EDGARDO COLON ','col@col.com','edgardo','','','42','160','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(140,'EDUARDO ARELLANO ','edua@edua.com','arellano','','','1','117','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(141,'EDUARDO CANEDO SANDOVAL ','can@can.com','canedo','','','1','94','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(142,'EDUARDO GARZA  ','ga@ga.com','eduardo','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(143,'EDUARDO MOSCAT ','mos@mos.com','moscat','','','42','161','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(144,'EDUARDO ORTIZ ','ru@ru.com','eduardo','','','4','7','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(145,'EDUARDO TENORIO ','te@te.com','tenorio','','','5','10','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(146,'EDWAER FABREGA ','edw@edw.com','edwaer','','','6','12','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(147,'EDWAR MEJIA ','edwa@edwa.com','edwarm','','','44','177','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(148,'EDWARD BENCOMO ','ben@ben.com','bencomo','','','44','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(149,'ELADIO CRUZ ','el@el.com','eladioc','','','8','29','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(150,'ELIAN RODRIGUEZ ','eli@eli.com','rodriguez','','','42','162','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(151,'ELÃAS SCHÃADOWER ','elias@elias.com','eliass','','','9','33','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(152,'ELIDA FLORES ','elid@elid.com','flores','','','44','168','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(153,'ELIO BELANDRIA ','elio@elio.com','belandria','','','44','169','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(154,'elo bautista ','elo@elo.com','bautista','','','10','34','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(155,'ELOY RAMOS ','eloy@eloy.com','eloyra','','','34','141','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(156,'ELSA PINTO ','elsa@elsa.com','elsapinto','','','44','170','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(157,'ELSY GOMEZ ','elsy@elsy.com','elsygomez','','','44','171','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(158,'EMILIO LOPEZ ','emi@emi.com','emilio','','','13','46','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(159,'EMILIO VAZQUEZ ACOSTA ','emilio@emilio.com','vazquez','','','14','49','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(160,'ENIK YURI ','en@en.com','enikyuri','','','45','179','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(161,'enrique lÃ¶pez ','enr@enr.com','enrique','','','15','51','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(162,'ERICK VILLAREAL ','eri@eri.com','villarreal','','','16','53','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(163,'ERNESTO ANTON ','erne@erne.com','ernesto','','','42','163','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(164,'ERNESTO MEDRANO GARCÍA ','ernes@ernes.com','medrano','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(165,'ESDRAS DAVILA ','esd@esd.com','esdras','','','34','142','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(166,'EUSEBIO GONZÁLEZ GONZÁLEZ ','eus@eus.com','eusebio','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(168,'FEDERICO CAMPS CORDOVA ','fed@fed.com','federico','','','18','58','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(169,'FEDY ALI ','fedy@fedy.com','fedyali','','','44','170','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(170,'FELA RODRIGUEZ ','fela@fela.com','rodriguez','','','42','164','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(171,'FELIPE FRAGOSO ','felipe@felipe.com','felipe','','','19','59','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(172,'feliz 2015 ','feliz@feliz.com','felizf','','','20','64','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(173,'FERNANDO ALLEN ','fern@fern.com','fernando','','','12','44','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(174,'FERNANDO GARCIA ','ferna@ferna.com','fernando','','','42','165','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(175,'FERNANDO ROMO DELGADO ','romo@romo.com','delgado','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(176,'FLOR CARCAMO ','fl@fl.com','carcamo','','','44','171','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(177,'FLOR LOZADA ','flo@flo.com','lozada','','','44','172','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(178,'FRANCISCO ALEJO ','fr@fr.com','francisco','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(179,'FRANCISCO ANAYA BERRONES ','an@an.com','berrones','','','21','65','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(180,'FRANCISCO CASTRO ','cas@cas.com','castro','','','22','66','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(181,'FRANCISCO J SALAZAR ','sal@sal.com','salazar','','','23','67','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(182,'francisco javier  austria ','jav@jav.com','austria','','','24','76','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(183,'FRANCISCO JAVIER MARTINEZ GUTIERREZ ','mart@mart.com','martinez','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(184,'FRANKLIN RUIZ ','fran@fran.com','franklin','','','44','173','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(185,'FREDDY BELLO ','fred@fred.com','freddy','','','44','173','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(186,'FREDDY GOMEZ ','fredd@fredd.com','freddy','','','44','109','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(187,'FREDDY JIMENEZ ','ji@ji.com','freddy','','','44','109','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(188,'FRIEDRICH DE LA ROSA ','frie@frie.com','friedrich','','','25','78','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(189,'G. ALBERTO RODRIGUEZ LOZANO ','gal@gal.com','alberto','','','31','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(190,'GABRIEL COLON ','gab@gab.com','gabriel','','','34','142','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(191,'GABRIEL NAVARRO PETIT ','nav@nav.com','navarro','','','32','137','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(192,'GABRIEL SITTON ','si@si.com','sitton','','','30','135','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(193,'GABUNIA ALMAS ','gabu@gabu.com','gabunia','','','45','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(194,'GABUNIA ARTUR ','gabun@gabun.com','gabunia','','','45','179','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(195,'GAUDENCIO CRESPO ','gau@gau.com','gaudencio','','','44','169','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(196,'GENARO LEOBARDO RAMÃ?Â?REZ MELÃ?Â?NDEZ ','leo@leo.com','leobardo','','','35','143','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(197,'GERARDO ALVAREZ ','ger@ger.com','gerardo','','','36','145','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(198,'GERARDO CARRILLO ','gera@gera.com','carrillo','','','37','146','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(199,'GERARDO GARZA','gerar@gerar.com','gerardo','','','37','146','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(200,'GERARDO GUZMAN ','guz@guz.com','guzman','','','38','147','','1','1960-01-01 00:00:00','Visual','INE','','',''),(201,'GERARDO JOSE DIAZ Ã?VILA ','jos@jos.com','josediaz','','','39','148','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(202,'GILBERTO GONZALEZ ','gi@gi.com','gilberto','','','44','169','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(203,'GILBERTO TORRES ','gilb@gilb.com','torres','','','34','140','','1','1960-01-01 00:00:00','Auditiva','INE','','',''),(204,'GILDA DE ABREU ','gild@gild.com','deabreu','','','44','170','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(205,'GONZALO DIAZ ','go@go.com','gonzalo','','','44','171','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(206,'GONZALO SANCHEZ ','gonz@gonz.com','sanchez','','','40','149','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(207,'GREGORIO ESQUIVEL VALDEZ ','gr@gr.com','gregorio','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(208,'GRISELDA LOPEZ ','gri@gri.com','griselda','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(209,'GUILLERMO CAMARENA ','guill@guill.com','guillermo','','','41','152','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(210,'GUILLERMO GAMPER ','guille@guille.com','guillermo','','','43','167','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(211,'GUILLERMO RODRÃ?GUEZ GUERRERO ','guiller@guiller.com','guillermo','','','46','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(220,'HECTOR FRANCISCO ROLDAN ELIAS','hec@hec.com','hector','','','48','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(221,'HECTOR GONZALEZ','hg@hg.com','gonzalez','','','42','166','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(223,'HECTOR MUJICA','hmu@hmu.com','mujica','','','44','169','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(224,'HÃ?CTOR OSOLLO TORRES','ho@ho.com','osollo','','','30','135','','1','1960-02-06 00:00:00','Ninguna','INE','','',''),(225,'HECTOR OSPINO','hos@hos.com','ospino','','','44','170','','1','1960-03-05 00:00:00','Visual','INE','445566','rade551209','rade551209mfgls'),(226,'HELY ARAGORT','hel@hel.com','aragort','','','1','93','','1','1960-01-01 00:00:00','Auditiva','cedula profesional','1234','1234','1234'),(227,'HENRY VILLALOBOS','hen@hen.com','henryv','7777777','888888','44','171','','1','1960-06-04 00:00:00','Motriz','Cartilla','2345','3456','678'),(228,'HIGINIO HERNÁNDEZ OVALLE','hig@hig.com','higinio','','','1','111','','1','1960-01-01 00:00:00','Motriz','Pasaporte','ty678','gh678u','ftr678'),(229,'HOMERO SANDOVAL ADAME','hom@hom.com','homero','444','5454','1','111','','1','1960-01-01 00:00:00','Ninguna','cedula profesional','4344','re433rt','tr5'),(230,'HORACIO GONZALEZ ELIZONDO','hor@hor.com','horacio','fe23','3213','1','111','','1','1960-01-30 00:00:00','Visual','cedula profesional','65rt','re56','54tr'),(231,'HUGO ESCUDERO','hug@hug.com','escudero','343ee','eer32','30','135','5454g','1','1960-02-27 00:00:00','Visual','cedula profesional','ewrerw','reerwewr43','rewewr32'),(232,'HUMBERTO IRIETA','humb@humb.com','irieta','4554er','rt332','35','144','33erw','1','1964-01-25 00:00:00','Auditiva','Cartilla','32erre','efewewr67','4345rer'),(233,'IGNACIO RAFAEL RIVERA JARAMILLO','ig@ig.com','ignacio','err23','42232w','40','150','rewewew','1','1965-01-23 00:00:00','Auditiva','Cartilla','ewrewr','erwewrew','32r3'),(234,'IRAIDA RAMOS','ir@ir.com','iraida','6643','54454','44','172','rtrtr','2','1956-01-25 00:00:00','Motriz','Cartilla','er556','5454thtr','rert443'),(235,'ISAAC RITZ','is@is.com','isaacr','321213','321213','32','138','23121213','1','1957-01-23 00:00:00','Visual','INE','213231ee','erwewrew','erer343'),(236,'ISIDRO SANTANA','isi@isi.com','isidro','4242','34243','30','93','q','1','1960-01-01 00:00:00','Ninguna','cedula profesional','wq','ewwq','wqwe'),(237,'ISMAEL ALVARADO CEPEDA','ism@ism.com','ismael','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(238,'ISRAEL DAVID','isr@isr.com','israel','','','44','173','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(239,'ITALIA PROFETA','it@it.com','italia','','','42','154','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(240,'ITZIAR UGARTE','itz@itz.com','itziar','','','13','47','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(241,'JACKIE VALENTIN ','ja@ja.com','jackie','645','564','42','155','ghghn','2','1965-01-30 00:00:00','Ninguna','Pasaporte','6565','564564','gfgdfgfd'),(242,'JACOBO VISKIN ','jac@jac.com','jacobo','970790','5555382706','2','4','dsds','1','1960-01-01 00:00:00','Visual','INE','sssda','dfsdfs','fsdsfd'),(243,'JAIME BELTRAN ','jai@jai.com','beltran','ffd','444','1','119','gjy','1','1960-01-29 00:00:00','Ninguna','cedula profesional','555','',''),(244,'JAIME CASSANI ','jaim@jaim.com','cassani','ghfhgf','hgfghf','1','129','gf','1','1958-01-23 00:00:00','Ninguna','Cartilla','gfghf','ghfgfh','gfhhgf'),(245,'JAIME GONZALEZ OVIEDO ','jaime@jaime.com','gonzalez','hfg','hgf','1','96','ghf','1','1960-01-01 00:00:00','Ninguna','Cartilla','grgr','rgrgr','rgrgr'),(246,'JAIME RODRIGUEZ GONZÁLEZ ','jair@jair.com','rodriguez','ooo','ppp','1','96','hjj','1','1957-01-30 00:00:00','Ninguna','Pasaporte','tttyr','hgfgf','ghfgfgf'),(247,'JAIRO CORDERO ','jairo@jairo.com','cordero','sasd','sdasd','44','173','dsdsasad','1','1967-01-17 00:00:00','Auditiva','Pasaporte','wds','sdasa','dssa'),(248,'JAVIER FUENTES ECHE ','javi@javi.com','javier','45543','453453','1','93','345','1','1966-01-26 00:00:00','Motriz','INE','45343','453435','43435'),(249,'JAVIER HERMIDA ','javie@javie.com','hermida','fgf','fdgfd','1','89','df','1','1954-01-21 00:00:00','Visual','INE','675765','65675','657675'),(250,'JAVIER MATAR ASSAD ','matar@matar.com','javier','6667','675675','1','111','','1','1969-01-15 00:00:00','Visual','INE','66767','6776675','6767556'),(251,'JAVIER ORTIZ ','ortiz@ortiz.com','javier','fgdfgd','fgdfgd','34','142','fgfdg','1','1965-01-27 00:00:00','Auditiva','Pasaporte','gfdfg','fgdfdg','fgdfgdfd'),(252,'JAVIER RICARDO RAMIREZ VILLANUEVA ','papachu4@hotmail.com','zazil369','5540945533','5555382706','1','93','amigos','1','1960-01-01 00:00:00','Ninguna','INE','dfsfds','dfsdfs','dsdsfd'),(253,'JESUS COVARRUBIAS CORREA ','jesus@jesus.com','correa','','','1','89','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(254,'JESUS DIEZ ALVAREZ ','diez@diez.com','alvarez','','','1','117','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(255,'JESUS DIEZ JR ','junior@junior.com','junior','','','1','117','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(256,'JESUS GARCIA ','garcia@garci.com','garcia','','','1','112','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(257,'JESÚS GONZÁLEZ GARCÍA ','jes@jes.com','gonzalez','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(258,'JESUS GUIZAR ','guizar@guizar.com','guizar','','','1','89','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(259,'JESUS GUIZAR ','gui@gui.com','guizar','','','1','89','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(260,'JESUS MARTINEZ RODRIGUEZ ','mtz@mtz.com','martinez','','','1','117','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(261,'JESÚS ORTIZ MODESTO ','mod@mod.com','modesto','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(262,'jhon ruiz ','jhon@jhon.com','jhonru','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(263,'JHON URBINA ','ur@ur.com','urbina','','','44','109','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(264,'JIMMY DIAZ ','jim@jim.com','jimmyd','','','34','140','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(265,'JOAN SANTOS ','joa@joa.com','santos','','','34','141','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(266,'JOAO CARLOS ','joao@joao.com','carlos','','','7','16','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(267,'JOAQUÍN MANUEL ESQUIVEL RIVERA ','joaq@joaq.com','joaquin','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(268,'JOAQUIN MARTINEZ ','joaqu@joaqu.com','joaquin','','','35','144','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(269,'JOEL COLON ','joel@joel.com','joelco','','','34','141','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(270,'JOEL COSME ','joelcos@joelcos.com','joelco','','','42','156','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(271,'JOHN ESPINAL ','john@john.com','espinal','','','35','143','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(272,'JONATAN KRONHEIM ','jon@jon.con','jonatan','','','1','113','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(273,'JORGE ABREU ','jor@jor.com','jorgea','','','44','175','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(274,'JORGE ALBERTO SALCIDO SALCIDO ','alberto@alberto.com','alberto','','','1','92','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(275,'JORGE CERON ','jorgec@jorgec.com','jorgec','','','1','92','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(276,'JORGE DACASA ','dacasa@dacasa.com','dacasa','','','1','89','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(277,'JORGE LECUNA ','lecuna@lecuna.com','lecuna','','','1','119','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(278,'JORGE LUIS ARIAS ','luis@luis.com','jorgel','','','44','176','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(279,'JORGE VALDEPEÑA ','val@val.com','valdepeña','','','1','99','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(280,'JORGE VILLABLANCA ','vill@vill.com','villablanca','','','44','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(281,'JOSE A. MACHADO ','mach@mach.com','machado','','','44','178','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(282,'JOSE A. MILLAN ','josem@josem.com','josemi','','','44','168','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(283,'JOSE ANTONIO GONZALEZ ','josean@josean.com','antonio','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(284,'JOSE ANTONIO MORENO OLGUIN ','joseant@joseant.com','moreno','','','1','92','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(285,'JOSE BAEZ ','joseba@joseba.com','joseba','','','42','156','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(286,'JOSE C. MENDEZ ','josec@josec.com','mendez','','','44','169','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(287,'JOSÉ CARLOS MÉNDEZ SUÁREZ ','joseca@joseca.com','mendez','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(288,'JOSE CASTRO ','castro@castro.com','joseca','','','42','157','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(289,'JOSE DE FREITAS ','freitas@freitas.com','freitas','','','44','169','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(290,'JOSE E. GARZA ','josee@josee.com','egarza','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(291,'JOSE FRANCISCO GARCÍA HERNANDEZ ','josef@josef.com','francis','','','1','127','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(292,'JOSÉ GREGORIO MIRANDA JIMENEZ ','gregorio@gregorio.com','gregorio','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(293,'JOSE GUZMAN ','guzman@guzman.com','guzman','','','35','144','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(294,'JOSÉ HOMERO VILLANUEVA GUERRA ','homero@homero.com','homero','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(295,'JOSE L AGUILAR ','aguilar@aguilar.com','aguilar','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(296,'JOSE L. MACHADO ','machado@machado.com','machado','','','44','170','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(297,'JOSE L. SILVA ','josel@josel.com','lsilva','','','44','171','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(298,'JOSE L. SOLORSANO ','solorsano@solorzano.com','solorzano','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(299,'JOSE LUIS GARZA BENAVIDES ','garza@garza.com','benavides','','','1','111','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(300,'JOSE LUIS MONROY JR ','monroy@monroy.com','monroy','','','1','119','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(301,'JOSE LUIS MONROY RIVAS ','mrivas@mrivas.com','monroy','','','1','119','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(302,'JOSE M. CORTEZ ','josecor@josecor.com','cortez','','','1','96','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(303,'JOSE M. MONROY ','josemm@josemm.com','monroy','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(304,'JOSE MARIA ALVAREZ ','josemaa@josemaa.com','josema','','','1','106','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(305,'JOSE MARQUEZ ','marquez@marquez.com','marquez','','','35','143','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(306,'JOSE NICOLAS MAURERA ','nicolas@nicolas.com','nicolas','','','44','171','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(307,'JOSE OLAVO ','joseol@joseol.com','jolavo','','','1','108','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(308,'JOSE PEREZ ','josep@josep.com','josepe','','','42','158','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(309,'JOSE RAFAEL COMPAÑ ','rafael@rafael.com','rafael','','','1','90','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(310,'JOSE RAMIREZ ','ramirez@ramirez.com','ramirez','','','1','119','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(311,'JOSE RODRIGUEZ ','rodriguez@rodriguez.com','rodriguez','','','44','172','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(312,'JOSE TANUZ ','tanuz@tanuz.com','joseta','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(313,'JUAN CARLOS GUZMAN ','carlosg@carlosg.com','guzman','','','35','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(314,'JUAN CARLOS REYNOSO ','reynoso@reynoso.com','reynoso','','','1','101','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(315,'JUAN E. LUIS ','juane@juane.com','juanel','','','44','173','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(316,'JUAN GERARDO VERDAD HERNANDEZ ','gerardo@gerardo.com','gerardo','','','1','106','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(317,'JUAN GONZALEZ TAMAYO ','juan@juan.com','tamayo','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(318,'JUAN GUERRERO GUZMAN ','guerrero@guerrero.com','guerrero','','','1','89','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(319,'JUAN HUMBERTO CRUZ NARVAEZ ','humberto@humberto.com','humberto','','','1','92','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(320,'JUAN MANUEL ZAMORA AMEZCUA ','manuel@manuel.com','manuel','','','1','129','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(321,'JUAN MORALES ','morales@morales.com','morales','','','35','144','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(322,'JUANJO ALVAREZ ','juanjo@juanjo.com','juanjo','','','34','141','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(323,'JUAREZ SILVA ','juarez@juarez.com','juarez','','','7','26','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(324,'JUDAZMIN MATA ','judazmin@judasmin.com','judazmin','','','44','109','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(325,'JULIA PEREZ ','julia@julia.com','juliap','','','44','175','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(326,'JULIANA BUITRIAGO ','juliana@juliana.com','juliana','','','44','176','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(327,'JULIO ACOSTA ','julio@julio.com','acosta','','','11','39','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(328,'JULIO ALBERTO MARTINEZ HUMPHREY ','julioalb@julioalb.com','julioa','','','1','119','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(329,'braulia narvaez','bra@bra.com','braulia','','','1','93','','1','1960-01-01 00:00:00','Ninguna','INE','','',''),(333,'sergio','sergioalcano25@gmail.com','ser','55','55','1','93','55','1','1960-01-01 00:00:00','Ninguna','INE','55','55','55');
/*!40000 ALTER TABLE `jugadores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metropoli`
--

DROP TABLE IF EXISTS `metropoli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metropoli` (
  `idMetropoli` int(11) NOT NULL AUTO_INCREMENT,
  `metropoli` varchar(100) NOT NULL,
  `idPais` int(11) NOT NULL,
  PRIMARY KEY (`idMetropoli`),
  KEY `idPais` (`idPais`),
  CONSTRAINT `idPais` FOREIGN KEY (`idPais`) REFERENCES `paises` (`idPais`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=207 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metropoli`
--

LOCK TABLES `metropoli` WRITE;
/*!40000 ALTER TABLE `metropoli` DISABLE KEYS */;
INSERT INTO `metropoli` VALUES (1,'Buenos Aires',28),(2,'Córdoba',28),(3,'Rosario',28),(4,'Oranjestad',2),(5,'Melbourne',3),(6,'Sydney',3),(7,'Christ Church',4),(8,'Saint John',4),(9,'Saint Michael',4),(10,'Belmopan',5),(11,'Belize',5),(12,'La Paz',6),(13,'Santa Cruz',6),(14,'Belem',7),(15,'Belo Horizonte',7),(16,'Brasilia Distrito Federal',7),(17,'Ceará',7),(18,'Curitiba',7),(19,'Espírito Santo',7),(20,'Fortaleza',7),(21,'Goiania',7),(22,'Goiás',7),(23,'Manaus',7),(24,'Porto Alegre',7),(25,'Recife',7),(26,'Rio de Janeiro',7),(27,'Salvador',7),(28,'Sao Paulo',7),(29,'Montreal',8),(30,'Ottawa',8),(31,'Toronto',8),(32,'Vancouver',8),(33,'Santiago',9),(34,'Beijing',10),(35,'Guangzhou',10),(36,'Shangha',10),(37,'Shenzhen',10),(38,'Barranquilla',11),(39,'Bogotá',11),(40,'Cali',11),(41,'Cartagena',11),(42,'Medellín',11),(43,'Alajuela',12),(44,'Limón',12),(45,'San José',12),(46,'Camaguey',13),(47,'La Habana',13),(48,'Santiago',13),(49,'Barber',14),(50,'Newport',14),(51,'Guayaquil',15),(52,'Quito',15),(53,'San Salvador',16),(54,'Barcelona',17),(55,'Madrid',17),(56,'Málaga',17),(57,'Murcia',17),(58,'Paris',18),(59,'Berlin',19),(60,'Hamburg',19),(61,'Munich',19),(62,'Rhine-Ruhr',19),(63,'Stuttgart',19),(64,'Guatemala',20),(65,'Tegucigalpa',21),(66,'Hong Kong',22),(67,'Ahmedabad',23),(68,'Bangalore',23),(69,'Chennai',23),(70,'Delhi',23),(71,'Hyderabad',23),(72,'Kolkata',23),(73,'Mumbai',23),(74,'Pune',23),(75,'Surat',23),(76,'Jakarta',24),(77,'Surabaya',24),(78,'Milan',25),(79,'Naples',25),(80,'Rome',25),(81,'Hanover',26),(82,'Kingston',26),(83,'Manchester',26),(84,'Westmoreland',26),(85,'Nagoya',27),(86,'Osaka',27),(87,'Tokyo',27),(88,'Acapulco',1),(89,'Aguascalientes',1),(90,'Cancún',1),(91,'Celaya',1),(92,'Chihuahua',1),(93,'Ciudad de México',1),(94,'Ciudad Juárez',1),(95,'Ciudad Obregón',1),(96,'Ciudad Victoria',1),(97,'Cuernavaca',1),(98,'Culiacán',1),(99,'Durango',1),(100,'Ecatepec',1),(101,'Guadalajara',1),(102,'Guadalupe',1),(103,'Hermosillo',1),(104,'Irapuato',1),(105,'La Paz',1),(106,'León',1),(107,'Los Mochis',1),(108,'Mazatlán',1),(109,'Mérida',1),(110,'Mexicali',1),(111,'Monterrey',1),(112,'Morelia',1),(113,'Naucalpan',1),(114,'Nezahualcóyotl',1),(115,'Nuevo Laredo',1),(116,'Pachuca',1),(117,'Puebla',1),(118,'Puerto Vallarta',1),(119,'Querétaro',1),(120,'Saltillo',1),(121,'San Luis Potosí',1),(122,'Tampico',1),(123,'Tepic',1),(124,'Tijuana',1),(125,'Tlalnepantla',1),(126,'Tlaxcala',1),(127,'Toluca',1),(128,'Torreón',1),(129,'Veracruz',1),(130,'Villahermosa',1),(131,'Xalapa',1),(132,'Zacatecas',1),(133,'Managua',29),(134,'Masaya',29),(135,'Panamá',30),(136,'Asunción',31),(137,'Lima',32),(138,'Callao',32),(139,'Manila',33),(140,'San Juan',34),(141,'Trujillo Alto',34),(142,'Vieques',34),(143,'Santiago Rodriguez',35),(144,'Santo Domingo Distrito Nacional',35),(145,'Singapore',36),(146,'Seoul',37),(147,'Taipei',38),(148,'Bangkok',39),(149,'Arima',40),(150,'Port of Spain',40),(151,'Tobago',40),(152,'Ankara',41),(153,'Istanbul',41),(154,'Atlanta',42),(155,'Boston',42),(156,'Chicago',42),(157,'Dallas–Fort Worth',42),(158,'Detroit',42),(159,'Houston',42),(160,'Los Angeles',42),(161,'Miami',42),(162,'New York',42),(163,'Philadelphia',42),(164,'Phoenix',42),(165,'San Francisco',42),(166,'Washington, D.C.',42),(167,'Montevideo',43),(168,'Barquisimeto',44),(169,'Caracas Distrito Capital',44),(170,'Falcón',44),(171,'Guárico',44),(172,'Lara',44),(173,'Maracaibo',44),(174,'Mérida',44),(175,'Sucre',44),(176,'Táchira',44),(177,'Trujillo',44),(178,'Valencia',44),(179,'Sokhumi',45),(180,'Baki Baku',46),(181,'Gence  Ganja',46),(182,'Волгогра Volgograd',47),(183,'Yekaterinburg',47),(184,'Kazan',47),(185,'Moscow',47),(186,'Nizhny Novgorod',47),(187,'Novosibirsk',47),(188,'Omsk',47),(189,'Perm',47),(190,'Rostov-on-Don',47),(191,'Samara',47),(192,'Leningrado- San Petersburgo',47),(193,'Ufa',47),(194,'Chelyabinsk',47),(195,'Kiev',48),(196,'Bayamón',34),(197,'Duarte',35),(198,'La Altagracia',35),(199,'Connecticut',42),(200,'Illinois',42),(201,'Florida',42),(202,'Amazonas',44),(203,'Maracay',44),(204,'Araure',44),(205,'Adiguesia ',47),(206,'Aga Buriati',47);
/*!40000 ALTER TABLE `metropoli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago_individual`
--

DROP TABLE IF EXISTS `pago_individual`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pago_individual` (
  `idinscripcion` int(11) NOT NULL AUTO_INCREMENT,
  `idjugador` int(11) DEFAULT NULL,
  `idtorneo` int(11) DEFAULT NULL,
  `pais` int(11) DEFAULT NULL,
  `metropoli` int(11) DEFAULT NULL,
  `club` varchar(100) DEFAULT NULL,
  `activo` bit(1) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `tipo_pago` varchar(100) DEFAULT NULL,
  `id_capturista` int(11) DEFAULT NULL,
  PRIMARY KEY (`idinscripcion`)
) ENGINE=InnoDB AUTO_INCREMENT=491 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago_individual`
--

LOCK TABLES `pago_individual` WRITE;
/*!40000 ALTER TABLE `pago_individual` DISABLE KEYS */;
INSERT INTO `pago_individual` VALUES (1,95,3,45,179,'','','1954-05-29','1',1),(2,115,3,45,179,'','','1954-05-29','1',1),(3,135,3,45,179,'','','1954-05-30','1',1),(4,157,3,45,179,'','','1954-05-31','1',1),(5,187,3,45,179,'','','1954-06-01','1',1),(6,220,3,45,179,'','','1954-06-02','1',1),(7,258,3,45,179,'','','1954-06-03','1',1),(8,306,3,45,179,'','','1954-06-04','1',1),(9,365,3,45,179,'','','1954-06-05','1',1),(10,434,3,45,179,'','','1954-06-06','1',1),(11,114,3,7,16,'','','1954-06-07','1',1),(12,134,3,7,16,'','','1954-06-08','1',1),(13,156,3,7,16,'','','1954-06-09','1',1),(14,186,3,7,16,'','','1954-06-10','1',1),(15,219,3,7,16,'','','1954-06-11','1',1),(16,257,3,7,16,'','','1954-06-12','1',1),(17,305,3,7,16,'','','1954-06-13','1',1),(18,363,3,7,28,'','','1954-06-14','1',1),(19,364,3,7,16,'','','1954-06-15','1',1),(20,432,3,7,28,'','','1954-06-16','1',1),(21,433,3,7,16,'','','1954-06-17','1',1),(22,304,3,11,39,'','','1954-06-18','1',1),(23,362,3,11,39,'','','1954-06-19','1',1),(24,431,3,11,39,'','','1954-06-20','1',1),(25,113,3,12,43,'','','1954-06-21','1',1),(26,133,3,12,43,'','','1954-06-22','1',1),(27,154,3,12,45,'','','1954-06-23','1',1),(28,155,3,12,43,'','','1954-06-24','1',1),(29,184,3,12,45,'','','1954-06-25','1',1),(30,185,3,12,43,'','','1954-06-26','1',1),(31,217,3,12,45,'','','1954-06-27','1',1),(32,218,3,12,43,'','','1954-06-28','1',1),(33,255,3,12,45,'','','1954-06-29','1',1),(34,256,3,12,43,'','','1954-06-30','1',1),(35,302,3,12,45,'','','1954-07-01','1',1),(36,303,3,12,43,'','','1954-07-02','1',1),(37,360,3,12,45,'','','1954-07-03','1',1),(38,361,3,12,43,'','','1954-07-04','1',1),(39,429,3,12,45,'','','1954-07-05','1',1),(40,430,3,12,43,'','','1954-07-06','1',1),(41,17,3,1,111,'cabrito','','1954-07-07','1',1),(42,19,3,1,111,'cabrito','','1954-07-08','1',1),(43,21,3,1,111,'cabrito','','1954-07-09','1',1),(44,23,3,1,111,'cabrito','','1954-07-10','1',1),(45,25,3,1,111,'cabrito','','1954-07-11','1',1),(46,27,3,1,111,'','','1954-07-12','1',1),(47,28,3,1,111,'cabrito','','1954-07-13','1',1),(48,29,3,1,93,'','','1954-07-14','1',1),(49,31,3,1,111,'','','1954-07-15','1',1),(50,32,3,1,111,'cabrito','','1954-07-16','1',1),(51,33,3,1,93,'','','1954-07-17','1',1),(52,38,3,1,111,'','','1954-07-18','1',1),(53,39,3,1,111,'cabrito','','1954-07-19','1',1),(54,40,3,1,93,'','','1954-07-20','1',1),(55,45,3,1,111,'','','1954-07-21','1',1),(56,46,3,1,111,'cabrito','','1954-07-22','1',1),(57,47,3,1,93,'','','1954-07-23','1',1),(58,52,3,1,111,'','','1954-07-24','1',1),(59,53,3,1,111,'cabrito','','1954-07-25','1',1),(60,54,3,1,93,'','','1954-07-26','1',1),(61,61,3,1,111,'','','1954-07-27','1',1),(62,62,3,1,111,'regio','','1954-07-28','1',1),(63,63,3,1,111,'cabrito','','1954-07-29','1',1),(64,64,3,1,93,'','','1954-07-30','1',1),(65,65,3,1,93,'Amigos por el Domino','','1954-07-31','1',1),(66,72,3,1,111,'','','1954-08-01','1',1),(67,73,3,1,111,'regio','','1954-08-02','1',1),(68,74,3,1,111,'cabrito','','1954-08-03','1',1),(69,75,3,1,93,'','','1954-08-04','1',1),(70,76,3,1,93,'Amigos por el Domino','','1954-08-05','1',1),(71,84,3,1,111,'','','1954-08-06','1',1),(72,85,3,1,111,'regio','','1954-08-07','1',1),(73,86,3,1,111,'la silla','','1954-08-08','1',1),(74,87,3,1,111,'cabrito','','1954-08-09','1',1),(75,88,3,1,93,'','','1954-08-10','1',1),(76,89,3,1,93,'m3','','1954-08-11','1',1),(77,90,3,1,93,'Amigos por el Domino','','1954-08-12','1',1),(78,101,3,1,119,'qw','','1954-08-13','1',1),(79,102,3,1,111,'','','1954-08-14','1',1),(80,103,3,1,111,'regio','','1954-08-15','1',1),(81,104,3,1,111,'la silla','','1954-08-16','1',1),(82,105,3,1,111,'cabrito','','1954-08-17','1',1),(83,106,3,1,93,'','','1954-08-18','1',1),(84,107,3,1,93,'m3','','1954-08-19','1',1),(85,108,3,1,93,'Amigos por el Domino','','1954-08-20','1',1),(86,121,3,1,119,'qw','','1954-08-21','1',1),(87,122,3,1,111,'','','1954-08-22','1',1),(88,123,3,1,111,'regio','','1954-08-23','1',1),(89,124,3,1,111,'la silla','','1954-08-24','1',1),(90,125,3,1,111,'cabrito','','1954-08-25','1',1),(91,126,3,1,93,'','','1954-08-26','1',1),(92,127,3,1,93,'m3','','1954-08-27','1',1),(93,128,3,1,93,'Amigos por el Domino','','1954-08-28','1',1),(94,141,3,1,119,'qw','','1954-08-29','1',1),(95,142,3,1,111,'','','1954-08-30','1',1),(96,143,3,1,111,'regio','','1954-08-31','1',1),(97,144,3,1,111,'la silla','','1954-09-01','1',1),(98,145,3,1,111,'cabrito','','1954-09-02','1',1),(99,146,3,1,93,'','','1954-09-03','1',1),(100,147,3,1,93,'m3','','1954-09-04','1',1),(101,148,3,1,93,'m2','','1954-09-05','1',1),(102,149,3,1,93,'Amigos por el Domino','','1954-09-06','1',1),(103,164,3,1,119,'qw','','1954-09-07','1',1),(104,165,3,1,111,'','','1954-09-08','1',1),(105,166,3,1,111,'regio','','1954-09-09','1',1),(106,167,3,1,111,'rayados','','1954-09-10','1',1),(107,168,3,1,111,'la silla','','1954-09-11','1',1),(108,169,3,1,111,'cabrito','','1954-09-12','1',1),(109,170,3,1,106,'','','1954-09-13','1',1),(110,171,3,1,96,'','','1954-09-14','1',1),(111,172,3,1,93,'','','1954-09-15','1',1),(112,173,3,1,93,'m3','','1954-09-16','1',1),(113,174,3,1,93,'m2','','1954-09-17','1',1),(114,175,3,1,93,'Amigos por el Domino','','1954-09-18','1',1),(115,176,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92','','1954-09-19','1',1),(116,195,3,1,119,'qw','','1954-09-20','1',1),(117,196,3,1,112,'','','1954-09-21','1',1),(118,197,3,1,111,'','','1954-09-22','1',1),(119,198,3,1,111,'regio','','1954-09-23','1',1),(120,199,3,1,111,'rayados','','1954-09-24','1',1),(121,200,3,1,111,'la silla','','1954-09-25','1',1),(122,201,3,1,111,'cabrito','','1954-09-26','1',1),(123,202,3,1,106,'','','1954-09-27','1',1),(124,203,3,1,96,'','','1954-09-28','1',1),(125,204,3,1,93,'','','1954-09-29','1',1),(126,205,3,1,93,'m3','','1954-09-30','1',1),(127,206,3,1,93,'m2','','1954-10-01','1',1),(128,207,3,1,93,'Amigos por el Domino','','1954-10-02','1',1),(129,208,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92','','1954-10-03','1',1),(130,228,3,1,119,'qw','','1954-10-04','1',1),(131,229,3,1,119,'CLUB ESTRELLA','','1954-10-05','1',1),(132,230,3,1,113,'nau','','1954-10-06','1',1),(133,231,3,1,112,'','','1954-10-07','1',1),(134,232,3,1,111,'','','1954-10-08','1',1),(135,233,3,1,111,'regio','','1954-10-09','1',1),(136,234,3,1,111,'rayados','','1954-10-10','1',1),(137,235,3,1,111,'la silla','','1954-10-11','1',1),(138,236,3,1,111,'cabrito','','1954-10-12','1',1),(139,237,3,1,106,'','','1954-10-13','1',1),(140,238,3,1,96,'','','1954-10-14','1',1),(141,239,3,1,96,'cv1','','1954-10-15','1',1),(142,240,3,1,93,'','','1954-10-16','1',1),(143,241,3,1,93,'m3','','1954-10-17','1',1),(144,242,3,1,93,'m2','','1954-10-18','1',1),(145,243,3,1,93,'fdrm','','1954-10-19','1',1),(146,244,3,1,93,'Amigos por el Domino','','1954-10-20','1',1),(147,245,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92','','1954-10-21','1',1),(148,268,3,1,132,'asociacion estatal de 132','','1954-10-22','1',1),(149,269,3,1,127,'','','1954-10-23','1',1),(150,270,3,1,119,'','','1954-10-24','1',1),(151,271,3,1,119,'qw','','1954-10-25','1',1),(152,272,3,1,119,'CLUB ESTRELLA','','1954-10-26','1',1),(153,273,3,1,117,'','','1954-10-27','1',1),(154,274,3,1,113,'','','1954-10-28','1',1),(155,275,3,1,113,'nau','','1954-10-29','1',1),(156,276,3,1,112,'','','1954-10-30','1',1),(157,277,3,1,111,'','','1954-10-31','1',1),(158,278,3,1,111,'regio','','1954-11-01','1',1),(159,279,3,1,111,'rayados','','1954-11-02','1',1),(160,280,3,1,111,'la silla','','1954-11-03','1',1),(161,281,3,1,111,'cabrito','','1954-11-04','1',1),(162,282,3,1,110,'Club de Domino de 110 AC','','1954-11-05','1',1),(163,283,3,1,106,'','','1954-11-06','1',1),(164,284,3,1,96,'','','1954-11-07','1',1),(165,285,3,1,96,'cv1','','1954-11-08','1',1),(166,286,3,1,93,'','','1954-11-09','1',1),(167,287,3,1,93,'m3','','1954-11-10','1',1),(168,288,3,1,93,'m2','','1954-11-11','1',1),(169,289,3,1,93,'fdrm','','1954-11-12','1',1),(170,290,3,1,93,'Amigos por el Domino','','1954-11-13','1',1),(171,291,3,1,92,'','','1954-11-14','1',1),(172,292,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92','','1954-11-15','1',1),(173,317,3,1,132,'asociacion estatal de 132','','1954-11-16','1',1),(174,318,3,1,129,'','','1954-11-17','1',1),(175,319,3,1,127,'','','1954-11-18','1',1),(176,320,3,1,121,'','','1954-11-19','1',1),(177,321,3,1,119,'','','1954-11-20','1',1),(178,322,3,1,119,'qw','','1954-11-21','1',1),(179,323,3,1,119,'CLUB ESTRELLA','','1954-11-22','1',1),(180,324,3,1,117,'','','1954-11-23','1',1),(181,325,3,1,113,'','','1954-11-24','1',1),(182,326,3,1,113,'nau','','1954-11-25','1',1),(183,327,3,1,112,'','','1954-11-26','1',1),(184,328,3,1,111,'','','1954-11-27','1',1),(185,329,3,1,111,'regio','','1954-11-28','1',1),(186,330,3,1,111,'rayados','','1954-11-29','1',1),(187,331,3,1,111,'la silla','','1954-11-30','1',1),(188,332,3,1,111,'Derededeimex AC','','1954-12-01','1',1),(189,333,3,1,111,'cabrito','','1954-12-02','1',1),(190,334,3,1,110,'Club de Domino de 110 AC','','1954-12-03','1',1),(191,335,3,1,106,'','','1954-12-04','1',1),(192,336,3,1,99,'ASOCIACION DE DOMINO DEL ESTADO DE 99','','1954-12-05','1',1),(193,337,3,1,96,'','','1954-12-06','1',1),(194,338,3,1,96,'cv1','','1954-12-07','1',1),(195,339,3,1,93,'','','1954-12-08','1',1),(196,340,3,1,93,'m3','','1954-12-09','1',1),(197,341,3,1,93,'m2','','1954-12-10','1',1),(198,342,3,1,93,'fdrm','','1954-12-11','1',1),(199,343,3,1,93,'Amigos por el Domino','','1954-12-12','1',1),(200,344,3,1,92,'','','1954-12-13','1',1),(201,345,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92','','1954-12-14','1',1),(202,346,3,1,89,'academia de domino','','1954-12-15','1',1),(203,380,3,1,132,'asociacion estatal de 132','','1954-12-16','1',1),(204,381,3,1,129,'','','1954-12-17','1',1),(205,382,3,1,127,'','','1954-12-18','1',1),(206,383,3,1,124,'','','1954-12-19','1',1),(207,384,3,1,121,'','','1954-12-20','1',1),(208,385,3,1,119,'','','1954-12-21','1',1),(209,386,3,1,119,'qw','','1954-12-22','1',1),(210,387,3,1,119,'CLUB ESTRELLA','','1954-12-23','1',1),(211,388,3,1,117,'','','1954-12-24','1',1),(212,389,3,1,113,'','','1954-12-25','1',1),(213,390,3,1,113,'nau','','1954-12-26','1',1),(214,391,3,1,112,'','','1954-12-27','1',1),(215,392,3,1,111,'','','1954-12-28','1',1),(216,393,3,1,111,'regio','','1954-12-29','1',1),(217,394,3,1,111,'rayados','','1954-12-30','1',1),(218,395,3,1,111,'la silla','','1954-12-31','1',1),(219,396,3,1,111,'Derededeimex AC','','1955-01-01','1',1),(220,397,3,1,111,'cabrito','','1955-01-02','1',1),(221,398,3,1,110,'Club de Domino de 110 AC','','1955-01-03','1',1),(222,399,3,1,107,'','','1955-01-04','1',1),(223,400,3,1,106,'','','1955-01-05','1',1),(224,401,3,1,99,'','','1955-01-06','1',1),(225,402,3,1,99,'ASOCIACIoN DE DOMINO DEL ESTADO DE 99','','1955-01-07','1',1),(226,403,3,1,96,'','','1955-01-08','1',1),(227,404,3,1,96,'cv1','','1955-01-09','1',1),(228,405,3,1,93,'','','1955-01-10','1',1),(229,406,3,1,93,'m3','','1955-01-11','1',1),(230,407,3,1,93,'m2','','1955-01-12','1',1),(231,408,3,1,93,'fdrm','','1955-01-13','1',1),(232,409,3,1,93,'Amigos por el Domino','','1955-01-14','1',1),(233,410,3,1,92,'','','1955-01-15','1',1),(234,411,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92','','1955-01-16','1',1),(235,412,3,1,89,'academia de domino','','1955-01-17','1',1),(236,454,3,1,132,'asociacion estatal de 132','','1955-01-18','1',1),(237,455,3,1,129,'','','1955-01-19','1',1),(238,456,3,1,127,'','','1955-01-20','1',1),(239,457,3,1,126,'','','1955-01-21','1',1),(240,458,3,1,124,'','','1955-01-22','1',1),(241,459,3,1,121,'','','1955-01-23','1',1),(242,460,3,1,119,'','','1955-01-24','1',1),(243,461,3,1,119,'qw','','1955-01-25','1',1),(244,462,3,1,119,'CLUB ESTRELLA','','1955-01-26','1',1),(245,463,3,1,117,'','','1955-01-27','1',1),(246,464,3,1,113,'','','1955-01-28','1',1),(247,465,3,1,113,'nau','','1955-01-29','1',1),(248,466,3,1,112,'','','1955-01-30','1',1),(249,467,3,1,111,'','','1955-01-31','1',1),(250,468,3,1,111,'regio','','1955-02-01','1',1),(251,469,3,1,111,'rayados','','1955-02-02','1',1),(252,470,3,1,111,'la silla','','1955-02-03','1',1),(253,471,3,1,111,'Derededeimex AC','','1955-02-04','1',1),(254,472,3,1,111,'cabrito','','1955-02-05','1',1),(255,473,3,1,110,'Club de Domino de 110 A.C.','','1955-02-06','1',1),(256,474,3,1,107,'','','1955-02-07','1',1),(257,475,3,1,106,'','','1955-02-08','1',1),(258,476,3,1,101,'','','1955-02-09','1',1),(259,477,3,1,99,'','','1955-02-10','1',1),(260,478,3,1,99,'ASOCIACION DE DOMINO DEL ESTADO DE 99','','1955-02-11','1',1),(261,479,3,1,96,'','','1955-02-12','1',1),(262,480,3,1,96,'cv1','','1955-02-13','1',1),(263,481,3,1,93,'','','1955-02-14','1',1),(264,482,3,1,93,'m3','','1955-02-15','1',1),(265,483,3,1,93,'m2','','1955-02-16','1',1),(266,484,3,1,93,'m1','','1955-02-17','1',1),(267,485,3,1,93,'fdrm','','1955-02-18','1',1),(268,486,3,1,93,'Amigos por el Domino','','1955-02-19','1',1),(269,487,3,1,92,'','','1955-02-20','1',1),(270,488,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92','','1955-02-21','1',1),(271,489,3,1,89,'','','1955-02-22','1',1),(272,490,3,1,89,'academia de domino','','1955-02-23','1',1),(273,35,3,30,135,'','','1955-02-24','1',1),(274,42,3,30,135,'','','1955-02-25','1',1),(275,49,3,30,135,'','','1955-02-26','1',1),(276,56,3,30,135,'','','1955-02-27','1',1),(277,67,3,30,135,'','','1955-02-28','1',1),(278,79,3,30,135,'','','1955-03-01','1',1),(279,94,3,30,135,'','','1955-03-02','1',1),(280,112,3,30,135,'','','1955-03-03','1',1),(281,132,3,30,135,'','','1955-03-04','1',1),(282,153,3,30,135,'','','1955-03-05','1',1),(283,183,3,30,135,'','','1955-03-06','1',1),(284,216,3,30,135,'','','1955-03-07','1',1),(285,254,3,30,135,'','','1955-03-08','1',1),(286,301,3,30,135,'','','1955-03-09','1',1),(287,359,3,30,135,'','','1955-03-10','1',1),(288,428,3,30,135,'','','1955-03-11','1',1),(289,182,3,34,196,'','','1955-03-12','1',1),(290,215,3,34,196,'','','1955-03-13','1',1),(291,253,3,34,196,'','','1955-03-14','1',1),(292,300,3,34,196,'','','1955-03-15','1',1),(293,356,3,34,141,'','','1955-03-16','1',1),(294,357,3,34,140,'aguas buenas','','1955-03-17','1',1),(295,358,3,34,196,'','','1955-03-18','1',1),(296,424,3,34,141,'','','1955-03-19','1',1),(297,425,3,34,140,'140','','1955-03-20','1',1),(298,426,3,34,140,'aguas buenas','','1955-03-21','1',1),(299,427,3,34,196,'','','1955-03-22','1',1),(300,214,3,35,197,'','','1955-03-23','1',1),(301,252,3,35,197,'','','1955-03-24','1',1),(302,299,3,35,197,'','','1955-03-25','1',1),(303,355,3,35,197,'','','1955-03-26','1',1),(304,422,3,35,198,'','','1955-03-27','1',1),(305,423,3,35,197,'','','1955-03-28','1',1),(306,180,3,47,205,'','','1955-03-29','1',1),(307,181,3,47,205,'mosk','','1955-03-30','1',1),(308,212,3,47,205,'','','1955-03-31','1',1),(309,213,3,47,205,'mosk','','1955-04-01','1',1),(310,249,3,47,206,'','','1955-04-02','1',1),(311,250,3,47,205,'','','1955-04-03','1',1),(312,251,3,47,205,'mosk','','1955-04-04','1',1),(313,296,3,47,206,'','','1955-04-05','1',1),(314,297,3,47,205,'','','1955-04-06','1',1),(315,298,3,47,205,'mosk','','1955-04-07','1',1),(316,351,3,47,192,'','','1955-04-08','1',1),(317,352,3,47,206,'','','1955-04-09','1',1),(318,353,3,47,205,'','','1955-04-10','1',1),(319,354,3,47,205,'mosk','','1955-04-11','1',1),(320,417,3,47,192,'','','1955-04-12','1',1),(321,418,3,47,192,'lenin','','1955-04-13','1',1),(322,419,3,47,206,'','','1955-04-14','1',1),(323,420,3,47,205,'','','1955-04-15','1',1),(324,421,3,47,205,'mosk','','1955-04-16','1',1),(325,1,3,42,201,'','','1955-04-17','1',1),(326,2,3,42,201,'','','1955-04-18','1',1),(327,3,3,42,201,'','','1955-04-19','1',1),(328,4,3,42,201,'','','1955-04-20','1',1),(329,5,3,42,201,'','','1955-04-21','1',1),(330,6,3,42,201,'','','1955-04-22','1',1),(331,7,3,42,201,'','','1955-04-23','1',1),(332,8,3,42,201,'','','1955-04-24','1',1),(333,9,3,42,201,'','','1955-04-25','1',1),(334,10,3,42,201,'','','1955-04-26','1',1),(335,11,3,42,201,'','','1955-04-27','1',1),(336,12,3,42,201,'','','1955-04-28','1',1),(337,13,3,42,201,'','','1955-04-29','1',1),(338,14,3,42,201,'','','1955-04-30','1',1),(339,15,3,42,201,'','','1955-05-01','1',1),(340,16,3,42,201,'','','1955-05-02','1',1),(341,18,3,42,201,'','','1955-05-03','1',1),(342,20,3,42,201,'','','1955-05-04','1',1),(343,22,3,42,201,'','','1955-05-05','1',1),(344,24,3,42,201,'','','1955-05-06','1',1),(345,26,3,42,201,'','','1955-05-07','1',1),(346,30,3,42,201,'','','1955-05-08','1',1),(347,34,3,42,201,'','','1955-05-09','1',1),(348,41,3,42,201,'','','1955-05-10','1',1),(349,48,3,42,201,'','','1955-05-11','1',1),(350,55,3,42,201,'','','1955-05-12','1',1),(351,66,3,42,201,'','','1955-05-13','1',1),(352,77,3,42,201,'','','1955-05-14','1',1),(353,78,3,42,199,'','','1955-05-15','1',1),(354,91,3,42,201,'','','1955-05-16','1',1),(355,92,3,42,201,'usdomino','','1955-05-17','1',1),(356,93,3,42,199,'','','1955-05-18','1',1),(357,109,3,42,201,'','','1955-05-19','1',1),(358,110,3,42,201,'usdomino','','1955-05-20','1',1),(359,111,3,42,199,'','','1955-05-21','1',1),(360,129,3,42,201,'','','1955-05-22','1',1),(361,130,3,42,201,'usdomino','','1955-05-23','1',1),(362,131,3,42,199,'','','1955-05-24','1',1),(363,150,3,42,201,'','','1955-05-25','1',1),(364,151,3,42,201,'usdomino','','1955-05-26','1',1),(365,152,3,42,199,'','','1955-05-27','1',1),(366,177,3,42,201,'','','1955-05-28','1',1),(367,178,3,42,201,'usdomino','','1955-05-29','1',1),(368,179,3,42,199,'','','1955-05-30','1',1),(369,209,3,42,201,'','','1955-05-31','1',1),(370,210,3,42,201,'usdomino','','1955-06-01','1',1),(371,211,3,42,199,'','','1955-06-02','1',1),(372,246,3,42,201,'','','1955-06-03','1',1),(373,247,3,42,201,'usdomino','','1955-06-04','1',1),(374,248,3,42,199,'','','1955-06-05','1',1),(375,293,3,42,201,'','','1955-06-06','1',1),(376,294,3,42,201,'usdomino','','1955-06-07','1',1),(377,295,3,42,199,'','','1955-06-08','1',1),(378,347,3,42,200,'','','1955-06-09','1',1),(379,348,3,42,201,'','','1955-06-10','1',1),(380,349,3,42,201,'usdomino','','1955-06-11','1',1),(381,350,3,42,199,'','','1955-06-12','1',1),(382,413,3,42,200,'','','1955-06-13','1',1),(383,414,3,42,201,'','','1955-06-14','1',1),(384,415,3,42,201,'usdomino','','1955-06-15','1',1),(385,416,3,42,199,'','','1955-06-16','1',1),(386,36,3,44,178,'','','1955-06-17','1',1),(387,37,3,44,169,'Capital','','1955-06-18','1',1),(388,43,3,44,178,'','','1955-06-19','1',1),(389,44,3,44,169,'Capital','','1955-06-20','1',1),(390,50,3,44,178,'','','1955-06-21','1',1),(391,51,3,44,169,'Capital','','1955-06-22','1',1),(392,57,3,44,178,'','','1955-06-23','1',1),(393,58,3,44,169,'','','1955-06-24','1',1),(394,59,3,44,169,'Capital','','1955-06-25','1',1),(395,60,3,44,169,'amigos del domino','','1955-06-26','1',1),(396,68,3,44,178,'','','1955-06-27','1',1),(397,69,3,44,169,'','','1955-06-28','1',1),(398,70,3,44,169,'Capital','','1955-06-29','1',1),(399,71,3,44,169,'amigos del domino','','1955-06-30','1',1),(400,80,3,44,178,'','','1955-07-01','1',1),(401,81,3,44,169,'','','1955-07-02','1',1),(402,82,3,44,169,'Capital','','1955-07-03','1',1),(403,83,3,44,169,'amigos del domino','','1955-07-04','1',1),(404,96,3,44,178,'','','1955-07-05','1',1),(405,97,3,44,169,'','','1955-07-06','1',1),(406,98,3,44,169,'Capital','','1955-07-07','1',1),(407,99,3,44,169,'amigos del domino','','1955-07-08','1',1),(408,100,3,44,202,'','','1955-07-09','1',1),(409,116,3,44,178,'','','1955-07-10','1',1),(410,117,3,44,169,'','','1955-07-11','1',1),(411,118,3,44,169,'Capital','','1955-07-12','1',1),(412,119,3,44,169,'amigos del domino','','1955-07-13','1',1),(413,120,3,44,202,'','','1955-07-14','1',1),(414,136,3,44,178,'','','1955-07-15','1',1),(415,137,3,44,169,'','','1955-07-16','1',1),(416,138,3,44,169,'Capital','','1955-07-17','1',1),(417,139,3,44,169,'amigos del domino','','1955-07-18','1',1),(418,140,3,44,202,'','','1955-07-19','1',1),(419,158,3,44,178,'','','1955-07-20','1',1),(420,159,3,44,172,'','','1955-07-21','1',1),(421,160,3,44,169,'','','1955-07-22','1',1),(422,161,3,44,169,'Capital','','1955-07-23','1',1),(423,162,3,44,169,'amigos del domino','','1955-07-24','1',1),(424,163,3,44,202,'','','1955-07-25','1',1),(425,188,3,44,178,'','','1955-07-26','1',1),(426,189,3,44,172,'','','1955-07-27','1',1),(427,190,3,44,169,'','','1955-07-28','1',1),(428,191,3,44,169,'Capital','','1955-07-29','1',1),(429,192,3,44,169,'amigos del domino','','1955-07-30','1',1),(430,193,3,44,202,'','','1955-07-31','1',1),(431,194,3,44,202,'amigos del domino','','1955-08-01','1',1),(432,221,3,44,178,'','','1955-08-02','1',1),(433,222,3,44,172,'','','1955-08-03','1',1),(434,223,3,44,169,'','','1955-08-04','1',1),(435,224,3,44,169,'Capital','','1955-08-05','1',1),(436,225,3,44,169,'amigos del domino','','1955-08-06','1',1),(437,226,3,44,202,'','','1955-08-07','1',1),(438,227,3,44,202,'amigos del domino','','1955-08-08','1',1),(439,259,3,44,178,'','','1955-08-09','1',1),(440,260,3,44,174,'federacion de 44s','','1955-08-10','1',1),(441,261,3,44,172,'','','1955-08-11','1',1),(442,262,3,44,169,'','','1955-08-12','1',1),(443,263,3,44,169,'Capital','','1955-08-13','1',1),(444,264,3,44,169,'amigos del domino','','1955-08-14','1',1),(445,265,3,44,204,'ara','','1955-08-15','1',1),(446,266,3,44,202,'','','1955-08-16','1',1),(447,267,3,44,202,'amigos del domino','','1955-08-17','1',1),(448,307,3,44,178,'','','1955-08-18','1',1),(449,308,3,44,175,'','','1955-08-19','1',1),(450,309,3,44,174,'federacion de 44s','','1955-08-20','1',1),(451,310,3,44,172,'','','1955-08-21','1',1),(452,311,3,44,169,'','','1955-08-22','1',1),(453,312,3,44,169,'Capital','','1955-08-23','1',1),(454,313,3,44,169,'amigos del domino','','1955-08-24','1',1),(455,314,3,44,204,'ara','','1955-08-25','1',1),(456,315,3,44,202,'','','1955-08-26','1',1),(457,316,3,44,202,'amigos del domino','','1955-08-27','1',1),(458,366,3,44,178,'','','1955-08-28','1',1),(459,367,3,44,178,'naguanagua','','1955-08-29','1',1),(460,368,3,44,175,'','','1955-08-30','1',1),(461,369,3,44,174,'','','1955-08-31','1',1),(462,370,3,44,174,'federacion de 44s','','1955-09-01','1',1),(463,371,3,44,172,'','','1955-09-02','1',1),(464,372,3,44,171,'','','1955-09-03','1',1),(465,373,3,44,169,'','','1955-09-04','1',1),(466,374,3,44,169,'Capital','','1955-09-05','1',1),(467,375,3,44,169,'amigos del domino','','1955-09-06','1',1),(468,376,3,44,204,'ara','','1955-09-07','1',1),(469,377,3,44,202,'','','1955-09-08','1',1),(470,378,3,44,202,'amigos del domino','','1955-09-09','1',1),(471,379,3,44,202,'202','','1955-09-10','1',1),(472,435,3,44,178,'','','1955-09-11','1',1),(473,436,3,44,178,'naguanagua','','1955-09-12','1',1),(474,437,3,44,176,'Plaza Delta','','1955-09-13','1',1),(475,438,3,44,175,'','','1955-09-14','1',1),(476,439,3,44,175,'italo venezolano','','1955-09-15','1',1),(477,440,3,44,203,'','','1955-09-16','1',1),(478,441,3,44,203,'mara','','1955-09-17','1',1),(479,442,3,44,174,'','','1955-09-18','1',1),(480,443,3,44,174,'federacion de 44s','','1955-09-19','1',1),(481,444,3,44,172,'','','1955-09-20','1',1),(482,445,3,44,171,'','','1955-09-21','1',1),(483,446,3,44,169,'','','1955-09-22','1',1),(484,447,3,44,169,'Capital','','1955-09-23','1',1),(485,448,3,44,169,'amigos del domino','','1955-09-24','1',1),(486,449,3,44,204,'','','1955-09-25','1',1),(487,450,3,44,204,'ara','','1955-09-26','1',1),(488,451,3,44,202,'','','1955-09-27','1',1),(489,452,3,44,202,'amigos del domino','','1955-09-28','1',1),(490,453,3,44,202,'202','','1955-09-29','1',1);
/*!40000 ALTER TABLE `pago_individual` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pago_individual_control`
--

DROP TABLE IF EXISTS `pago_individual_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pago_individual_control` (
  `idinscripcion` int(11) DEFAULT NULL,
  `idjugador` int(11) DEFAULT NULL,
  `idtorneo` int(11) DEFAULT NULL,
  `pais` int(11) DEFAULT NULL,
  `metropoli` int(11) DEFAULT NULL,
  `club` varchar(100) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=491 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pago_individual_control`
--

LOCK TABLES `pago_individual_control` WRITE;
/*!40000 ALTER TABLE `pago_individual_control` DISABLE KEYS */;
INSERT INTO `pago_individual_control` VALUES (52,38,3,1,111,'',1),(345,26,3,42,201,'',2),(461,369,3,44,174,'',3),(6,220,3,45,179,'',4),(362,131,3,42,199,'',5),(279,94,3,30,135,'',6),(97,144,3,1,111,'la silla',7),(318,353,3,47,205,'',8),(425,188,3,44,178,'',9),(25,113,3,12,43,'',10),(15,219,3,7,16,'',11),(122,201,3,1,111,'cabrito',12),(433,222,3,44,172,'',13),(258,476,3,1,101,'',14),(302,299,3,35,197,'',15),(287,359,3,30,135,'',16),(80,103,3,1,111,'regio',17),(21,433,3,7,16,'',18),(410,117,3,44,169,'',19),(349,48,3,42,201,'',20),(50,32,3,1,111,'cabrito',21),(280,112,3,30,135,'',22),(379,348,3,42,201,'',23),(405,97,3,44,169,'',24),(213,390,3,1,113,'nau',25),(311,250,3,47,205,'',26),(11,114,3,7,16,'',27),(458,366,3,44,178,'',28),(36,303,3,12,43,'',29),(301,252,3,35,197,'',30),(265,483,3,1,93,'m2',31),(296,424,3,34,141,'',32),(220,397,3,1,111,'cabrito',33),(366,177,3,42,201,'',34),(416,138,3,44,169,'Capital',35),(14,186,3,7,16,'',36),(180,324,3,1,117,'',37),(10,434,3,45,179,'',38),(443,263,3,44,169,'Capital',39),(351,66,3,42,201,'',40),(255,473,3,1,110,'Club de Domino de 110 A.C.',41),(350,55,3,42,201,'',42),(411,118,3,44,169,'Capital',43),(313,296,3,47,206,'',44),(275,49,3,30,135,'',45),(330,6,3,42,201,'',46),(185,329,3,1,111,'regio',47),(406,98,3,44,169,'Capital',48),(474,437,3,44,176,'Plaza Delta',49),(62,62,3,1,111,'regio',50),(1,95,3,45,179,'',51),(323,420,3,47,205,'',52),(99,146,3,1,93,'',53),(478,441,3,44,203,'mara',54),(359,111,3,42,199,'',55),(283,183,3,30,135,'',56),(240,458,3,1,124,'',57),(473,436,3,44,178,'naguanagua',58),(35,302,3,12,45,'',59),(355,92,3,42,201,'usdomino',60),(383,414,3,42,201,'',61),(134,232,3,1,111,'',62),(314,297,3,47,205,'',63),(428,191,3,44,169,'Capital',64),(59,53,3,1,111,'cabrito',65),(400,80,3,44,178,'',66),(370,210,3,42,201,'usdomino',67),(295,358,3,34,196,'',68),(242,460,3,1,119,'',69),(335,11,3,42,201,'',70),(460,368,3,44,175,'',71),(29,184,3,12,45,'',72),(241,459,3,1,121,'',73),(435,224,3,44,169,'Capital',74),(328,4,3,42,201,'',75),(294,357,3,34,140,'aguas buenas',76),(381,350,3,42,199,'',77),(320,417,3,47,192,'',78),(34,256,3,12,43,'',79),(41,17,3,1,111,'cabrito',80),(436,225,3,44,169,'amigos del domino',81),(233,410,3,1,92,'',82),(312,251,3,47,205,'mosk',83),(341,18,3,42,201,'',84),(114,175,3,1,93,'Amigos por el Domino',85),(470,378,3,44,202,'amigos del domino',86),(338,14,3,42,201,'',87),(13,156,3,7,16,'',88),(108,169,3,1,111,'cabrito',89),(352,77,3,42,201,'',90),(304,422,3,35,198,'',91),(487,450,3,44,204,'ara',92),(391,51,3,44,169,'Capital',93),(376,294,3,42,201,'usdomino',94),(7,258,3,45,179,'',95),(33,255,3,12,45,'',96),(291,253,3,34,196,'',97),(227,404,3,1,96,'cv1',98),(431,194,3,44,202,'amigos del domino',99),(23,362,3,11,39,'',100),(182,326,3,1,113,'nau',101),(393,58,3,44,169,'',102),(290,215,3,34,196,'',103),(368,179,3,42,199,'',104),(22,304,3,11,39,'',105),(216,393,3,1,111,'regio',106),(285,254,3,30,135,'',107),(336,12,3,42,201,'',108),(276,56,3,30,135,'',109),(74,87,3,1,111,'cabrito',110),(367,178,3,42,201,'usdomino',111),(471,379,3,44,202,'202',112),(481,444,3,44,172,'',113),(54,40,3,1,93,'',114),(361,130,3,42,201,'usdomino',115),(316,351,3,47,192,'',116),(440,260,3,44,174,'federacion de 44s',117),(49,31,3,1,111,'',118),(340,16,3,42,201,'',119),(305,423,3,35,197,'',120),(257,475,3,1,106,'',121),(4,157,3,45,179,'',122),(310,249,3,47,206,'',123),(408,100,3,44,202,'',124),(424,163,3,44,202,'',125),(380,349,3,42,201,'usdomino',126),(119,198,3,1,111,'regio',127),(38,361,3,12,43,'',128),(389,44,3,44,169,'Capital',129),(67,73,3,1,111,'regio',130),(365,152,3,42,199,'',131),(19,364,3,7,16,'',132),(353,78,3,42,199,'',133),(212,389,3,1,113,'',134),(387,37,3,44,169,'Capital',135),(317,352,3,47,206,'',136),(445,265,3,44,204,'ara',137),(186,330,3,1,111,'rayados',138),(344,24,3,42,201,'',139),(28,155,3,12,43,'',140),(16,257,3,7,16,'',141),(270,488,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92',142),(392,57,3,44,178,'',143),(358,110,3,42,201,'usdomino',144),(321,418,3,47,192,'lenin',145),(459,367,3,44,178,'naguanagua',146),(100,147,3,1,93,'m3',147),(27,154,3,12,45,'',148),(455,314,3,44,204,'ara',149),(179,323,3,1,119,'CLUB ESTRELLA',150),(373,247,3,42,201,'usdomino',151),(306,180,3,47,205,'',152),(463,371,3,44,172,'',153),(89,124,3,1,111,'la silla',154),(369,209,3,42,201,'',155),(5,187,3,45,179,'',156),(469,377,3,44,202,'',157),(77,90,3,1,93,'Amigos por el Domino',158),(322,419,3,47,206,'',159),(24,431,3,11,39,'',160),(307,181,3,47,205,'mosk',161),(115,176,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92',162),(486,449,3,44,204,'',163),(12,134,3,7,16,'',164),(419,158,3,44,178,'',165),(181,325,3,1,113,'',166),(278,79,3,30,135,'',167),(315,298,3,47,205,'mosk',168),(53,39,3,1,111,'cabrito',169),(464,372,3,44,171,'',170),(319,354,3,47,205,'mosk',171),(333,9,3,42,201,'',172),(483,446,3,44,169,'',173),(214,391,3,1,112,'',174),(26,133,3,12,43,'',175),(308,212,3,47,205,'',176),(163,283,3,1,106,'',177),(432,221,3,44,178,'',178),(39,429,3,12,45,'',179),(293,356,3,34,141,'',180),(334,10,3,42,201,'',181),(60,54,3,1,93,'',182),(412,119,3,44,169,'amigos del domino',183),(298,426,3,34,140,'aguas buenas',184),(372,246,3,42,201,'',185),(112,173,3,1,93,'m3',186),(414,136,3,44,178,'',187),(8,306,3,45,179,'',188),(446,266,3,44,202,'',189),(378,347,3,42,200,'',190),(244,462,3,1,119,'CLUB ESTRELLA',191),(309,213,3,47,205,'mosk',192),(159,279,3,1,111,'rayados',193),(18,363,3,7,28,'',194),(398,70,3,44,169,'Capital',195),(2,115,3,45,179,'',196),(224,401,3,1,99,'',197),(457,316,3,44,202,'amigos del domino',198),(385,416,3,42,199,'',199),(273,35,3,30,135,'',200),(267,485,3,1,93,'fdrm',201),(364,151,3,42,201,'usdomino',202),(462,370,3,44,174,'federacion de 44s',203),(20,432,3,7,28,'',204),(427,190,3,44,169,'',205),(237,455,3,1,129,'',206),(347,34,3,42,201,'',207),(289,182,3,34,196,'',208),(261,479,3,1,96,'',209),(327,3,3,42,201,'',210),(477,440,3,44,203,'',211),(284,216,3,30,135,'',212),(69,75,3,1,93,'',213),(476,439,3,44,175,'italo venezolano',214),(288,428,3,30,135,'',215),(3,135,3,45,179,'',216),(448,307,3,44,178,'',217),(271,489,3,1,89,'',218),(356,93,3,42,199,'',219),(299,427,3,34,196,'',220),(466,374,3,44,169,'Capital',221),(132,230,3,1,113,'nau',222),(384,415,3,42,201,'usdomino',223),(303,355,3,35,197,'',224),(175,319,3,1,127,'',225),(401,81,3,44,169,'',226),(332,8,3,42,201,'',227),(32,218,3,12,43,'',228),(215,392,3,1,111,'',229),(325,1,3,42,201,'',230),(403,83,3,44,169,'amigos del domino',231),(40,430,3,12,43,'',232),(441,261,3,44,172,'',233),(30,185,3,12,43,'',234),(331,7,3,42,201,'',235),(93,128,3,1,93,'Amigos por el Domino',236),(409,116,3,44,178,'',237),(205,382,3,1,127,'',238),(329,5,3,42,201,'',239),(274,42,3,30,135,'',240),(422,161,3,44,169,'Capital',241),(203,380,3,1,132,'asociacion estatal de 132',242),(342,20,3,42,201,'',243),(300,214,3,35,197,'',244),(167,287,3,1,93,'m3',245),(442,262,3,44,169,'',246),(348,41,3,42,201,'',247),(324,421,3,47,205,'mosk',248),(423,162,3,44,169,'amigos del domino',249),(94,141,3,1,119,'qw',250),(292,300,3,34,196,'',251),(375,293,3,42,201,'',252),(166,286,3,1,93,'',253),(479,442,3,44,174,'',254),(31,217,3,12,45,'',255),(357,109,3,42,201,'',256),(160,280,3,1,111,'la silla',257),(420,159,3,44,172,'',258),(339,15,3,42,201,'',259),(37,360,3,12,45,'',260),(193,337,3,1,96,'',261),(377,295,3,42,199,'',262),(429,192,3,44,169,'amigos del domino',263),(286,301,3,30,135,'',264),(169,289,3,1,93,'fdrm',265),(17,305,3,7,16,'',266),(467,375,3,44,169,'amigos del domino',267),(360,129,3,42,201,'',268),(46,27,3,1,111,'',269),(449,308,3,44,175,'',270),(297,425,3,34,140,'140',271),(354,91,3,42,201,'',272),(75,88,3,1,93,'',273),(382,413,3,42,200,'',274),(434,223,3,44,169,'',275),(9,365,3,45,179,'',276),(277,67,3,30,135,'',277),(484,447,3,44,169,'Capital',278),(146,244,3,1,93,'Amigos por el Domino',279),(346,30,3,42,201,'',280),(109,170,3,1,106,'',281),(421,160,3,44,169,'',282),(363,150,3,42,201,'',283),(282,153,3,30,135,'',284),(374,248,3,42,199,'',285),(157,277,3,1,111,'',286),(453,312,3,44,169,'Capital',287),(281,132,3,30,135,'',288),(248,466,3,1,112,'',289),(337,13,3,42,201,'',290),(397,69,3,44,169,'',291),(230,407,3,1,93,'m2',292),(465,373,3,44,169,'',293),(95,142,3,1,111,'',294),(468,376,3,44,204,'ara',295),(260,478,3,1,99,'ASOCIACION DE DOMINO DEL ESTADO DE 99',296),(125,204,3,1,93,'',297),(116,195,3,1,119,'qw',298),(234,411,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92',299),(239,457,3,1,126,'',300),(91,126,3,1,93,'',301),(404,96,3,44,178,'',302),(202,346,3,1,89,'academia de domino',303),(438,227,3,44,202,'amigos del domino',304),(194,338,3,1,96,'cv1',305),(245,463,3,1,117,'',306),(151,271,3,1,119,'qw',307),(154,274,3,1,113,'',308),(105,166,3,1,111,'regio',309),(259,477,3,1,99,'',310),(70,76,3,1,93,'Amigos por el Domino',311),(133,231,3,1,112,'',312),(172,292,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92',313),(264,482,3,1,93,'m3',314),(223,400,3,1,106,'',315),(130,228,3,1,119,'qw',316),(178,322,3,1,119,'qw',317),(162,282,3,1,110,'Club de Domino de 110 AC',318),(226,403,3,1,96,'',319),(147,245,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92',320),(485,448,3,44,169,'amigos del domino',321),(131,229,3,1,119,'CLUB ESTRELLA',322),(390,50,3,44,178,'',323),(161,281,3,1,111,'cabrito',324),(136,234,3,1,111,'rayados',325),(221,398,3,1,110,'Club de Domino de 110 AC',326),(225,402,3,1,99,'ASOCIACIoN DE DOMINO DEL ESTADO DE 99',327),(111,172,3,1,93,'',328),(78,101,3,1,119,'qw',329),(156,276,3,1,112,'',330),(211,388,3,1,117,'',331),(222,399,3,1,107,'',332),(174,318,3,1,129,'',333),(183,327,3,1,112,'',334),(152,272,3,1,119,'CLUB ESTRELLA',335),(204,381,3,1,129,'',336),(45,25,3,1,111,'cabrito',337),(141,239,3,1,96,'cv1',338),(266,484,3,1,93,'m1',339),(155,275,3,1,113,'nau',340),(188,332,3,1,111,'Derededeimex AC',341),(148,268,3,1,132,'asociacion estatal de 132',342),(231,408,3,1,93,'fdrm',343),(177,321,3,1,119,'',344),(247,465,3,1,113,'nau',345),(201,345,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92',346),(208,385,3,1,119,'',347),(256,474,3,1,107,'',348),(206,383,3,1,124,'',349),(123,202,3,1,106,'',350),(171,291,3,1,92,'',351),(145,243,3,1,93,'fdrm',352),(76,89,3,1,93,'m3',353),(243,461,3,1,119,'qw',354),(269,487,3,1,92,'',355),(58,52,3,1,111,'',356),(418,140,3,44,202,'',357),(96,143,3,1,111,'regio',358),(439,259,3,44,178,'',359),(246,464,3,1,113,'',360),(48,29,3,1,93,'',361),(129,208,3,1,92,'CLUB DE DOMINO DEL EDO. DE 92',362),(238,456,3,1,127,'',363),(140,238,3,1,96,'',364),(101,148,3,1,93,'m2',365),(272,490,3,1,89,'academia de domino',366),(200,344,3,1,92,'',367),(106,167,3,1,111,'rayados',368),(430,193,3,44,202,'',369),(480,443,3,44,174,'federacion de 44s',370),(451,310,3,44,172,'',371),(475,438,3,44,175,'',372),(84,107,3,1,93,'m3',373),(482,445,3,44,171,'',374),(210,387,3,1,119,'CLUB ESTRELLA',375),(444,264,3,44,169,'amigos del domino',376),(79,102,3,1,111,'',377),(126,205,3,1,93,'m3',378),(110,171,3,1,96,'',379),(150,270,3,1,119,'',380),(415,137,3,44,169,'',381),(143,241,3,1,93,'m3',382),(447,267,3,44,202,'amigos del domino',383),(190,334,3,1,110,'Club de Domino de 110 AC',384),(113,174,3,1,93,'m2',385),(118,197,3,1,111,'',386),(117,196,3,1,112,'',387),(176,320,3,1,121,'',388),(47,28,3,1,111,'cabrito',389),(456,315,3,44,202,'',390),(168,288,3,1,93,'m2',391),(396,68,3,44,178,'',392),(85,108,3,1,93,'Amigos por el Domino',393),(138,236,3,1,111,'cabrito',394),(262,480,3,1,96,'cv1',395),(139,237,3,1,106,'',396),(72,85,3,1,111,'regio',397),(57,47,3,1,93,'',398),(86,121,3,1,119,'qw',399),(149,269,3,1,127,'',400),(197,341,3,1,93,'m2',401),(426,189,3,44,172,'',402),(55,45,3,1,111,'',403),(394,59,3,44,169,'Capital',404),(195,339,3,1,93,'',405),(71,84,3,1,111,'',406),(207,384,3,1,121,'',407),(191,335,3,1,106,'',408),(44,23,3,1,111,'cabrito',409),(326,2,3,42,201,'',410),(103,164,3,1,119,'qw',411),(343,22,3,42,201,'',412),(144,242,3,1,93,'m2',413),(254,472,3,1,111,'cabrito',414),(153,273,3,1,117,'',415),(164,284,3,1,96,'',416),(187,331,3,1,111,'la silla',417),(142,240,3,1,93,'',418),(236,454,3,1,132,'asociacion estatal de 132',419),(124,203,3,1,96,'',420),(395,60,3,44,169,'amigos del domino',421),(165,285,3,1,96,'cv1',422),(417,139,3,44,169,'amigos del domino',423),(235,412,3,1,89,'academia de domino',424),(170,290,3,1,93,'Amigos por el Domino',425),(388,43,3,44,178,'',426),(121,200,3,1,111,'la silla',427),(454,313,3,44,169,'amigos del domino',428),(450,309,3,44,174,'federacion de 44s',429),(251,469,3,1,111,'rayados',430),(402,82,3,44,169,'Capital',431),(64,64,3,1,93,'',432),(268,486,3,1,93,'Amigos por el Domino',433),(90,125,3,1,111,'cabrito',434),(209,386,3,1,119,'qw',435),(173,317,3,1,132,'asociacion estatal de 132',436),(252,470,3,1,111,'la silla',437),(437,226,3,44,202,'',438),(192,336,3,1,99,'ASOCIACION DE DOMINO DEL ESTADO DE 99',439),(488,451,3,44,202,'',440),(413,120,3,44,202,'',441),(120,199,3,1,111,'rayados',442),(407,99,3,44,169,'amigos del domino',443),(232,409,3,1,93,'Amigos por el Domino',444),(250,468,3,1,111,'regio',445),(65,65,3,1,93,'Amigos por el Domino',446),(43,21,3,1,111,'cabrito',447),(184,328,3,1,111,'',448),(371,211,3,42,199,'',449),(158,278,3,1,111,'regio',450),(489,452,3,44,202,'amigos del domino',451),(81,104,3,1,111,'la silla',452),(107,168,3,1,111,'la silla',453),(189,333,3,1,111,'cabrito',454),(66,72,3,1,111,'',455),(127,206,3,1,93,'m2',456),(42,19,3,1,111,'cabrito',457),(490,453,3,44,202,'202',458),(399,71,3,44,169,'amigos del domino',459),(249,467,3,1,111,'',460),(102,149,3,1,93,'Amigos por el Domino',461),(104,165,3,1,111,'',462),(92,127,3,1,93,'m3',463),(253,471,3,1,111,'Derededeimex AC',464),(87,122,3,1,111,'',465),(98,145,3,1,111,'cabrito',466),(229,406,3,1,93,'m3',467),(199,343,3,1,93,'Amigos por el Domino',468),(137,235,3,1,111,'la silla',469),(198,342,3,1,93,'fdrm',470),(82,105,3,1,111,'cabrito',471),(472,435,3,44,178,'',472),(73,86,3,1,111,'la silla',473),(263,481,3,1,93,'',474),(88,123,3,1,111,'regio',475),(68,74,3,1,111,'cabrito',476),(218,395,3,1,111,'la silla',477),(63,63,3,1,111,'cabrito',478),(217,394,3,1,111,'rayados',479),(386,36,3,44,178,'',480),(135,233,3,1,111,'regio',481),(196,340,3,1,93,'m3',482),(61,61,3,1,111,'',483),(128,207,3,1,93,'Amigos por el Domino',484),(83,106,3,1,93,'',485),(219,396,3,1,111,'Derededeimex AC',486),(56,46,3,1,111,'cabrito',487),(452,311,3,44,169,'',488),(228,405,3,1,93,'',489),(51,33,3,1,93,'',490);
/*!40000 ALTER TABLE `pago_individual_control` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paises`
--

DROP TABLE IF EXISTS `paises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paises` (
  `idPais` int(11) NOT NULL AUTO_INCREMENT,
  `pais` varchar(100) NOT NULL,
  PRIMARY KEY (`idPais`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paises`
--

LOCK TABLES `paises` WRITE;
/*!40000 ALTER TABLE `paises` DISABLE KEYS */;
INSERT INTO `paises` VALUES (1,'México'),(2,'Aruba'),(3,'Australia'),(4,'Barbados'),(5,'Belize'),(6,'Bolivia'),(7,'Brazil'),(8,'Canada'),(9,'Chile'),(10,'China'),(11,'Colombia'),(12,'Costa Rica'),(13,'Cuba'),(14,'Curacao'),(15,'Ecuador'),(16,'El Salvador'),(17,'España'),(18,'France'),(19,'Germany'),(20,'Guatemala'),(21,'Honduras'),(22,'Hong Kong'),(23,'India'),(24,'Indonesia'),(25,'Italy'),(26,'Jamaica'),(27,'Japan'),(28,'Argentina'),(29,'Nicaragua'),(30,'Panama'),(31,'Paraguay'),(32,'Peru'),(33,'Philippines'),(34,'Puerto Rico'),(35,'Republica Dominicana'),(36,'Singapore'),(37,'South Korea'),(38,'Taiwan'),(39,'Thailand'),(40,'Trinidad and Tobago'),(41,'Turkey'),(42,'United States'),(43,'Uruguay'),(44,'Venezuela'),(45,'Abhkazia'),(46,'Azerbaijan'),(47,'Rusia'),(48,'Ucrania'),(49,'pais'),(50,'Pakistan');
/*!40000 ALTER TABLE `paises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registrotorneomodequipo`
--

DROP TABLE IF EXISTS `registrotorneomodequipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registrotorneomodequipo` (
  `idRTME` int(11) NOT NULL AUTO_INCREMENT,
  `idjugador_x_torneo` int(11) DEFAULT NULL,
  `idTorneo` int(11) DEFAULT NULL,
  `idjugador` int(11) DEFAULT NULL,
  `activo` bit(1) DEFAULT b'0',
  `fechaRTME` datetime DEFAULT NULL,
  PRIMARY KEY (`idRTME`),
  KEY `rtme_idjuagador_x_torneo` (`idjugador_x_torneo`),
  KEY `rtme_idTorneo` (`idTorneo`),
  KEY `rtme_idJugador` (`idjugador`),
  CONSTRAINT `rtme_idjuagador_x_torneo` FOREIGN KEY (`idjugador_x_torneo`) REFERENCES `jugador_x_torneo` (`idjugador_x_torneo`),
  CONSTRAINT `rtme_idJugador` FOREIGN KEY (`idjugador`) REFERENCES `jugadores` (`idjugador`),
  CONSTRAINT `rtme_idTorneo` FOREIGN KEY (`idTorneo`) REFERENCES `torneos` (`idTorneo`)
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registrotorneomodequipo`
--

LOCK TABLES `registrotorneomodequipo` WRITE;
/*!40000 ALTER TABLE `registrotorneomodequipo` DISABLE KEYS */;
INSERT INTO `registrotorneomodequipo` VALUES (43,178,3,11,'\0','2016-04-12 12:55:18'),(44,179,5,11,'\0','2016-04-12 12:56:25'),(45,180,12,11,'\0','2016-04-12 12:56:44'),(46,181,3,12,'\0','2016-04-12 12:57:16'),(47,182,5,12,'\0','2016-04-12 12:57:47'),(48,183,12,12,'\0','2016-04-12 12:57:58'),(49,184,3,13,'\0','2016-04-12 12:58:56'),(50,185,5,13,'\0','2016-04-12 12:59:17'),(51,186,12,13,'\0','2016-04-12 12:59:28'),(52,187,3,14,'\0','2016-04-12 13:01:18'),(53,188,5,14,'\0','2016-04-12 13:01:33'),(54,189,12,14,'\0','2016-04-12 13:01:48'),(55,190,3,15,'\0','2016-04-12 13:02:34'),(56,191,5,15,'\0','2016-04-12 13:02:43'),(57,192,12,15,'\0','2016-04-12 13:03:00'),(58,193,3,16,'\0','2016-04-12 13:04:11'),(59,194,5,16,'\0','2016-04-12 13:04:23'),(60,195,12,16,'\0','2016-04-12 13:04:31'),(61,196,3,17,'\0','2016-04-12 13:06:32'),(62,197,5,17,'\0','2016-04-12 13:06:40'),(63,198,12,17,'\0','2016-04-12 13:06:47'),(64,199,3,18,'\0','2016-04-12 13:07:15'),(65,200,5,18,'\0','2016-04-12 13:07:22'),(66,201,12,18,'\0','2016-04-12 13:07:32'),(67,202,3,19,'\0','2016-04-12 13:08:39'),(68,203,5,19,'\0','2016-04-12 13:08:47'),(69,204,12,19,'\0','2016-04-12 13:09:10'),(70,205,3,20,'\0','2016-04-12 13:12:01'),(71,206,5,20,'\0','2016-04-12 13:12:11'),(72,207,12,20,'\0','2016-04-12 13:12:20'),(76,211,3,22,'\0','2016-04-12 13:31:57'),(77,212,5,22,'\0','2016-04-12 13:32:07'),(78,213,12,22,'\0','2016-04-12 13:32:24'),(79,214,3,23,'\0','2016-04-12 13:32:52'),(80,215,5,23,'\0','2016-04-12 13:33:07'),(81,216,12,23,'\0','2016-04-12 13:33:30'),(82,217,3,24,'\0','2016-04-12 13:34:09'),(83,218,5,24,'\0','2016-04-12 13:34:16'),(84,219,12,24,'\0','2016-04-12 13:34:24'),(86,221,5,25,'\0','2016-04-12 13:35:14'),(87,222,12,25,'\0','2016-04-12 13:35:26'),(88,223,3,26,'\0','2016-04-12 13:36:15'),(89,224,5,26,'\0','2016-04-12 13:36:23'),(90,225,12,26,'\0','2016-04-12 13:36:29'),(92,227,5,27,'\0','2016-04-12 13:39:56'),(93,228,12,27,'\0','2016-04-12 13:40:09'),(94,229,3,29,'\0','2016-04-12 13:40:51'),(95,230,5,29,'\0','2016-04-12 13:40:58'),(96,231,12,29,'\0','2016-04-12 13:41:05'),(98,233,5,30,'\0','2016-04-12 13:46:22'),(99,234,12,30,'\0','2016-04-12 13:46:32'),(101,236,5,31,'\0','2016-04-12 13:47:09'),(102,237,12,31,'\0','2016-04-12 13:47:16'),(103,238,3,32,'\0','2016-04-12 13:47:46'),(104,239,5,32,'\0','2016-04-12 13:47:53'),(105,240,12,32,'\0','2016-04-12 13:48:00'),(106,241,3,33,'\0','2016-04-12 13:48:26'),(107,242,5,33,'\0','2016-04-12 13:48:34'),(108,243,12,33,'\0','2016-04-12 13:48:41'),(109,244,3,34,'\0','2016-04-12 13:49:03'),(110,245,5,34,'\0','2016-04-12 13:49:10'),(111,246,12,34,'\0','2016-04-12 13:49:18'),(112,247,3,35,'\0','2016-04-12 13:49:42'),(113,248,5,35,'\0','2016-04-12 13:49:52'),(114,249,12,35,'\0','2016-04-12 13:50:01'),(116,251,5,36,'\0','2016-04-12 13:50:32'),(117,252,12,36,'\0','2016-04-12 13:50:39'),(118,253,3,37,'\0','2016-04-12 16:39:51'),(146,271,3,52,'\0','2016-04-21 14:55:35'),(147,272,3,292,'\0','2016-04-29 15:04:18'),(148,273,3,133,'\0','2016-04-29 15:05:39'),(149,274,3,135,'\0','2016-04-29 15:08:46'),(150,275,3,141,'\0','2016-04-29 15:11:53'),(151,276,3,146,'\0','2016-04-29 15:16:26'),(152,277,3,87,'\0','2016-04-29 15:17:17'),(153,278,3,84,'\0','2016-04-29 15:19:25'),(154,279,3,328,'\0','2016-04-29 15:20:20'),(155,280,3,322,'\0','2016-04-29 15:26:52'),(156,281,3,327,'\0','2016-05-10 12:58:38'),(157,282,3,326,'\0','2016-05-10 13:40:48'),(158,283,3,325,'\0','2016-05-10 13:41:09');
/*!40000 ALTER TABLE `registrotorneomodequipo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `registrotorneomodpareja`
--

DROP TABLE IF EXISTS `registrotorneomodpareja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registrotorneomodpareja` (
  `idRTMP` int(11) NOT NULL AUTO_INCREMENT,
  `idjugador_x_torneo` int(11) DEFAULT NULL,
  `idTorneo` int(11) DEFAULT NULL,
  `idjugador` int(11) DEFAULT NULL,
  `activo` bit(1) DEFAULT b'0',
  `fechaRTMP` datetime DEFAULT NULL,
  PRIMARY KEY (`idRTMP`),
  KEY `rtmp_idjuagador_x_torneo` (`idjugador_x_torneo`),
  KEY `rtmp_idTorneo` (`idTorneo`),
  KEY `rtmp_idJugador` (`idjugador`),
  CONSTRAINT `rtmp_idjuagador_x_torneo` FOREIGN KEY (`idjugador_x_torneo`) REFERENCES `jugador_x_torneo` (`idjugador_x_torneo`),
  CONSTRAINT `rtmp_idJugador` FOREIGN KEY (`idjugador`) REFERENCES `jugadores` (`idjugador`),
  CONSTRAINT `rtmp_idTorneo` FOREIGN KEY (`idTorneo`) REFERENCES `torneos` (`idTorneo`)
) ENGINE=InnoDB AUTO_INCREMENT=265 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `registrotorneomodpareja`
--

LOCK TABLES `registrotorneomodpareja` WRITE;
/*!40000 ALTER TABLE `registrotorneomodpareja` DISABLE KEYS */;
INSERT INTO `registrotorneomodpareja` VALUES (153,179,5,11,'\0','2016-04-12 12:56:25'),(156,182,5,12,'\0','2016-04-12 12:57:47'),(157,183,12,12,'\0','2016-04-12 12:57:58'),(158,184,3,13,'','2016-04-12 12:58:56'),(159,185,5,13,'\0','2016-04-12 12:59:17'),(160,186,12,13,'\0','2016-04-12 12:59:28'),(161,187,3,14,'\0','2016-04-12 13:01:18'),(162,188,5,14,'\0','2016-04-12 13:01:33'),(163,189,12,14,'\0','2016-04-12 13:01:48'),(164,190,3,15,'\0','2016-04-12 13:02:34'),(165,191,5,15,'\0','2016-04-12 13:02:43'),(166,192,12,15,'\0','2016-04-12 13:03:00'),(167,193,3,16,'\0','2016-04-12 13:04:11'),(168,194,5,16,'\0','2016-04-12 13:04:23'),(169,195,12,16,'\0','2016-04-12 13:04:31'),(170,196,3,17,'\0','2016-04-12 13:06:32'),(171,197,5,17,'\0','2016-04-12 13:06:40'),(172,198,12,17,'\0','2016-04-12 13:06:47'),(173,199,3,18,'\0','2016-04-12 13:07:15'),(174,200,5,18,'\0','2016-04-12 13:07:22'),(175,201,12,18,'\0','2016-04-12 13:07:32'),(176,202,3,19,'\0','2016-04-12 13:08:39'),(177,203,5,19,'\0','2016-04-12 13:08:47'),(178,204,12,19,'\0','2016-04-12 13:09:10'),(179,205,3,20,'\0','2016-04-12 13:12:01'),(180,206,5,20,'\0','2016-04-12 13:12:11'),(181,207,12,20,'\0','2016-04-12 13:12:20'),(185,211,3,22,'\0','2016-04-12 13:31:57'),(186,212,5,22,'\0','2016-04-12 13:32:07'),(187,213,12,22,'\0','2016-04-12 13:32:24'),(188,214,3,23,'\0','2016-04-12 13:32:52'),(189,215,5,23,'\0','2016-04-12 13:33:07'),(190,216,12,23,'\0','2016-04-12 13:33:30'),(191,217,3,24,'\0','2016-04-12 13:34:09'),(192,218,5,24,'\0','2016-04-12 13:34:16'),(193,219,12,24,'\0','2016-04-12 13:34:24'),(194,220,3,25,'\0','2016-04-12 13:34:59'),(195,221,5,25,'\0','2016-04-12 13:35:14'),(196,222,12,25,'\0','2016-04-12 13:35:26'),(197,223,3,26,'\0','2016-04-12 13:36:15'),(198,224,5,26,'\0','2016-04-12 13:36:23'),(199,225,12,26,'\0','2016-04-12 13:36:29'),(200,226,3,27,'\0','2016-04-12 13:39:46'),(201,227,5,27,'\0','2016-04-12 13:39:56'),(202,228,12,27,'\0','2016-04-12 13:40:09'),(203,229,3,29,'\0','2016-04-12 13:40:51'),(204,230,5,29,'\0','2016-04-12 13:40:58'),(205,231,12,29,'\0','2016-04-12 13:41:05'),(206,232,3,30,'\0','2016-04-12 13:46:04'),(207,233,5,30,'\0','2016-04-12 13:46:22'),(208,234,12,30,'\0','2016-04-12 13:46:32'),(209,235,3,31,'\0','2016-04-12 13:47:02'),(210,237,12,31,'\0','2016-04-12 13:47:16'),(211,238,3,32,'\0','2016-04-12 13:47:46'),(212,239,5,32,'\0','2016-04-12 13:47:53'),(213,240,12,32,'\0','2016-04-12 13:48:00'),(214,241,3,33,'\0','2016-04-12 13:48:26'),(215,242,5,33,'\0','2016-04-12 13:48:34'),(216,243,12,33,'\0','2016-04-12 13:48:41'),(217,244,3,34,'\0','2016-04-12 13:49:03'),(218,245,5,34,'\0','2016-04-12 13:49:10'),(219,246,12,34,'\0','2016-04-12 13:49:18'),(220,247,3,35,'\0','2016-04-12 13:49:42'),(221,248,5,35,'\0','2016-04-12 13:49:52'),(222,249,12,35,'\0','2016-04-12 13:50:01'),(223,250,3,36,'\0','2016-04-12 13:50:26'),(224,251,5,36,'\0','2016-04-12 13:50:32'),(225,252,12,36,'\0','2016-04-12 13:50:39'),(226,253,3,37,'\0','2016-04-12 16:39:51'),(251,272,3,292,'\0','2016-04-29 15:04:18'),(252,273,3,133,'\0','2016-04-29 15:05:39'),(253,274,3,135,'\0','2016-04-29 15:08:46'),(254,275,3,141,'\0','2016-04-29 15:11:53'),(255,276,3,146,'\0','2016-04-29 15:16:26'),(256,278,3,84,'\0','2016-04-29 15:19:25'),(257,279,3,328,'\0','2016-04-29 15:20:20'),(258,280,3,322,'\0','2016-04-29 15:26:52'),(261,271,3,52,'\0','2016-05-06 14:58:35'),(262,281,3,327,'','2016-05-10 12:58:38'),(263,282,3,326,'\0','2016-05-10 13:40:48'),(264,283,3,325,'\0','2016-05-10 13:41:09');
/*!40000 ALTER TABLE `registrotorneomodpareja` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `torneos`
--

DROP TABLE IF EXISTS `torneos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `torneos` (
  `idTorneo` int(11) NOT NULL AUTO_INCREMENT,
  `torneo` varchar(200) DEFAULT NULL,
  `ambito` varchar(200) DEFAULT NULL,
  `idPais` int(11) DEFAULT NULL,
  `idMetropoli` int(11) DEFAULT NULL,
  `sede` varchar(200) DEFAULT NULL,
  `genero` char(1) DEFAULT NULL,
  `fechaHoraio` datetime DEFAULT NULL,
  `formato` varchar(50) DEFAULT NULL,
  `numeroMult` int(11) DEFAULT NULL,
  `modInd` bit(1) DEFAULT NULL,
  `modIndB1` float(9,2) DEFAULT NULL,
  `modIndB2` float(9,2) DEFAULT NULL,
  `modIndB3` float(9,2) DEFAULT NULL,
  `modPareja` bit(1) DEFAULT NULL,
  `modParejaB1` float(9,2) DEFAULT NULL,
  `modParejaB2` float(9,2) DEFAULT NULL,
  `modParejaB3` float(9,2) DEFAULT NULL,
  `modEquipo` bit(1) DEFAULT NULL,
  `modEquipoB1` float(9,2) DEFAULT NULL,
  `modEquipoB2` float(9,2) DEFAULT NULL,
  `modEquipoB3` float(9,2) DEFAULT NULL,
  `metaind` int(11) DEFAULT NULL,
  `tiempoPartidaind` int(11) DEFAULT NULL,
  `horarioIntDesde` datetime DEFAULT NULL,
  `horarioIntHasta` datetime DEFAULT NULL,
  `horarioSede` datetime DEFAULT NULL,
  `activoInternet` bit(1) DEFAULT NULL,
  `activoTorneo` bit(1) DEFAULT NULL,
  `partidaind` int(11) DEFAULT NULL,
  `insind` float(9,2) DEFAULT NULL,
  `partidapar` int(11) DEFAULT NULL,
  `inspar` float(9,2) DEFAULT NULL,
  `partidaequ` int(11) DEFAULT NULL,
  `insequ` float(9,2) DEFAULT NULL,
  `horarioSedeHasta` datetime DEFAULT NULL,
  `metapar` int(11) DEFAULT NULL,
  `tiempoPartidapar` int(11) DEFAULT NULL,
  `metaequ` int(11) DEFAULT NULL,
  `tiempoPartidaequ` int(11) DEFAULT NULL,
  `fechahorariofin` datetime DEFAULT NULL,
  `formatopar` int(11) DEFAULT NULL,
  `numeroMultpar` int(11) DEFAULT NULL,
  `fotmatoequ` int(11) DEFAULT NULL,
  PRIMARY KEY (`idTorneo`),
  KEY `torneos_idPais` (`idPais`),
  KEY `torneos_idMetropoli` (`idMetropoli`),
  CONSTRAINT `torneos_idMetropoli` FOREIGN KEY (`idMetropoli`) REFERENCES `metropoli` (`idMetropoli`),
  CONSTRAINT `torneos_idPais` FOREIGN KEY (`idPais`) REFERENCES `paises` (`idPais`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `torneos`
--

LOCK TABLES `torneos` WRITE;
/*!40000 ALTER TABLE `torneos` DISABLE KEYS */;
INSERT INTO `torneos` VALUES (3,'capital','1',1,88,'capital','3','2016-02-29 00:00:00','4',0,'',9000.00,5000.00,0.00,'',9000.00,7000.00,80000.00,'',800000.00,3000.00,5000.00,NULL,25,'2016-02-29 23:01:00','2016-02-23 23:59:00','2016-02-29 23:00:00','','',10,500.00,10,500.00,3,3000.00,'2016-02-23 14:00:00',NULL,25,NULL,25,'2016-04-18 14:00:00',4,0,4),(5,'vallejo','1',1,93,'vallejo','2','2016-03-01 23:00:00','2',2,'',100.00,5000.00,7000.00,'',200.00,800.00,500.00,'',0.00,0.00,0.00,100,25,'2016-03-01 00:01:00','2016-03-01 23:59:00','2016-03-01 00:00:00','','',5,500.50,10,250.00,0,0.00,'2016-03-01 00:00:00',100,25,0,0,'2016-02-29 22:00:00',2,2,4),(12,'Orlando','2',6,12,'orlando2','3','2016-09-07 10:00:00','2',2,'',500.00,9000.00,100.00,'',555.00,9000.00,800.00,'\0',0.00,0.00,0.00,100,25,'2016-04-11 23:59:59','2016-04-11 00:00:00','2016-04-11 23:59:59','','',5,800.00,5,0.00,0,0.00,'2016-04-11 00:00:00',0,0,0,0,'2016-09-10 13:00:00',2,2,4);
/*!40000 ALTER TABLE `torneos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `idUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `idJugador` int(11) DEFAULT NULL,
  `correo` varchar(500) DEFAULT NULL,
  `psw` varchar(10) DEFAULT NULL,
  `roll` bit(1) DEFAULT NULL,
  `fechaAlta` date DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  KEY `usuarios_idJugador` (`idJugador`),
  CONSTRAINT `usuarios_idJugador` FOREIGN KEY (`idJugador`) REFERENCES `jugadores` (`idjugador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'domino'
--

--
-- Dumping routines for database 'domino'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_deleteequipo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deleteequipo`(torneo int)
BEGIN
   
  
DELETE FROM formacionequipo where idtorneo=torneo;
DELETE FROM registrotorneomodequipo where idTorneo=torneo;
UPDATE jugador_x_torneo set equipo = 0 where idTorneo=torneo;
update torneos set modEquipoB1=0,modEquipoB2=0,modEquipoB3=0,partidaequ=0,insequ=0,metaequ=0,tiempoPartidaequ=0 where idTorneo=torneo;
  
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_deletepais` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deletepais`(id int )
BEGIN
DELETE FROM metropoli where idPais=id;
DELETE FROM paises where idPais=id; 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_deletepareja` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deletepareja`(torneo int)
BEGIN
   
  
DELETE FROM formacionpareja where idtorneo=torneo;
DELETE FROM registrotorneomodpareja where idTorneo=torneo;
UPDATE jugador_x_torneo set pareja = 0 where idTorneo=torneo;
update torneos set modParejaB1=0,modParejaB2=0,modParejaB3=0,partidapar=0,inspar=0,metapar=0,tiempoPartidapar=0,formatopar=1,numeroMultpar=0 where idTorneo=torneo;
  
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_deletetorneo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_deletetorneo`(torneo int)
BEGIN
  DELETE FROM formacionequipo where idtorneo=torneo;
  DELETE FROM formacionpareja where idtorneo=torneo;
  DELETE FROM registrotorneomodequipo where idTorneo=torneo;
  DELETE FROM registrotorneomodpareja where idTorneo=torneo;
  DELETE FROM jugador_x_torneo where idTorneo=torneo;
  DELETE FROM torneos where idTorneo=torneo;
  END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-15  9:54:02
